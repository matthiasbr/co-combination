/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.importer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;

import vre.analysis.AbstractAnalyzer;
import vre.analysis.ExtendedDatabaseConnection;
import vre.analysis.util.DBTextpassage;

/**
 * Import the results of an external analysis by the CrowdFlower users.
 *
 */
public class ImportAnalysis extends AbstractAnalyzer {

	/**
	 * constructor
	 */
	public ImportAnalysis() {

	}

	/**
	 * Starts the import routine.
	 * 
	 * @param dbc database connection
	 * @param path file path of the CrowdFlower results
	 */
	public void importText(ExtendedDatabaseConnection dbc, String path) {

		// Stream to read file
		FileInputStream fin;
		BufferedReader din;
		String title = "";
		String date = "";
		String text = "";
		String words = "";
		int pos = 0;
		// ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();
		String tpA = "";
		int tps = 0;

		HashSet<Integer> currentLemmaIDs = new HashSet<Integer>();
		ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();

		// Open an input stream
		try {
			fin = new FileInputStream(path);

			din = new BufferedReader(new InputStreamReader(fin));

			String line;

			// Read a line of text
			while ((line = din.readLine()) != null) {

				String linec[] = line.split("\t");
				if (linec[0].equals("#")) {
					title = linec[1];
					date = linec[2];
				} else {

					if (!linec[0].equals("")) {
						Integer i = Integer.parseInt(linec[0]);
						String word = linec[1];
						text += (word);
						text += " ";
						int wlength = word.length();

						String tag[] = linec[2].split("-");
						if (!tag[0].equals("O")) {
							if (tag[0].equals("I")) {
								words += word + " ";
							} else {
								// B
								tps = pos;
								tpA = tag[1];
								words = word;
							}
						} else {
							int tpe = pos - 1;

							if (tpA != "" && (tpA.equals("PER") || tpA.equals("PERderiv") || tpA.equals("PERpart"))) {

								int start[] = new int[2];
								int end[] = new int[2];
								start[0] = 1;
								end[0] = 1;
								start[1] = tps;
								end[1] = tpe;
								DBTextpassage tp = new DBTextpassage(words + " # " + tpA, start, end);
								TextPassages.add(tp);

								HashSet<String> RegisterTopic = RegisterTopics.get(tpA);
								if (RegisterTopic == null)
									RegisterTopic = new HashSet<String>();
								RegisterTopic.add(words);

								RegisterTopics.put(tpA, RegisterTopic);

								if (LemmaIDs.get(tpA) == null)
									LemmaIDs.put(tpA, lemmaIdCounter++);

								String lemmaString = words + " # " + tpA;
								int currentLemmaID;
								if (LemmaIDs.get(lemmaString) == null) {
									currentLemmaID = lemmaIdCounter++;
									LemmaIDs.put(lemmaString, currentLemmaID);
								}
								currentLemmaID = LemmaIDs.get(lemmaString);
								currentLemmaIDs.add(currentLemmaID);

								tpA = "";

							}

							tpA = "";

						}

						pos += wlength;
						pos++;

					} else {
						// End of Sentence
						// = new Document;
						Integer docID = dbc.createNewDocument(text);

						DocumentLemma.put(docID, currentLemmaIDs);
						DocumentTextPassages.put(docID, TextPassages);
						currentLemmaIDs = new HashSet<Integer>();
						TextPassages = new ArrayList<DBTextpassage>();
						text = "";
						pos = 0;

					}
				}

			}

			// Close our input stream
			fin.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
	}

}
