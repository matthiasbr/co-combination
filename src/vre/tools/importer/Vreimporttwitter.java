/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.importer;

import vre.analysis.ExtendedDatabaseConnection;

/**
 * Class for an import of the Lowland results to the vre database.
 *
 */
public class Vreimporttwitter {

	/**
	 * Method to start the import.
	 * @param args
	 * 
	 */
	public static void main(String[] args) {

		ExtendedDatabaseConnection dbc = new ExtendedDatabaseConnection("vre_lowlands_4", "vre", "change");

		ImportAnalysisTwitter ia = new ImportAnalysisTwitter();
		ia.importText(dbc, false, "lowlands.test", "");

		String returnList = ia.createSQL(dbc, "import");

		// System.out.println(returnList);
		dbc.executeQuery(returnList);

	}

}
