/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.importer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import vre.analysis.AbstractAnalyzer;
import vre.analysis.DatabaseConnection;
import vre.analysis.util.DBTextpassage;

/**
 * Import the results of an external analysis by the CrowdFlower users.
 *
 */
public class ImportAnalysisTwitter extends AbstractAnalyzer {

	/**
	 * constructor
	 */
	public ImportAnalysisTwitter() {

	}

	/**
	 * Starts the import routine. The option crowd decides, which data to import.
	 * 
	 * @param dbc database connection
	 * @param crowd import only gold data: false, import both: true
	 * @param pathgold file path to gold data
	 * @param pathcrowd file path to crowd data
	 */
	public void importText(DatabaseConnection dbc, Boolean crowd, String pathgold, String pathcrowd) {

		int addi = 1000;
		// int addi = 0;

		// Stream to read file
		FileInputStream fingold;
		BufferedReader dingold;
		FileInputStream fincrowd;
		BufferedReader dincrowd;
		int pos = 0;
		// ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();
		String tpA = "";
		int tps = 0;

		HashSet<Integer> currentLemmaIDs = new HashSet<Integer>();
		ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();

		// Open an input stream

		try {
			fingold = new FileInputStream(pathgold);
			dingold = new BufferedReader(new InputStreamReader(fingold, "UTF8"));
			fincrowd = fingold;
			dincrowd = dingold;
			if (!pathcrowd.isEmpty()) {
				fincrowd = new FileInputStream(pathcrowd);
				dincrowd = new BufferedReader(new InputStreamReader(fincrowd, "UTF8"));
			}

			int tweetc = 0;

			String line = "";
			String linecrowd = "";
			String tweet = "";

			// Read a line of text
			while ((line = dingold.readLine()) != null) {
				if (!pathcrowd.isEmpty()) {
					linecrowd = dincrowd.readLine();
				}

				String linec[] = line.split("\t");
				String linecrowdc[] = linecrowd.split("\t");

				if (linec.length > 1) {
					String crowdcratings[] = new String[0];
					if (!pathcrowd.isEmpty()) {
						crowdcratings = linecrowdc[2].split(",");
					}
					// && linec[1] != "."
					if (tweet.length() > 0) {
						tweet += " ";
					}
					String word = linec[0];
					tweet += word;

					int wlength = word.length();

					tps = pos;
					String words = word;
					int tpe = tps + wlength;

					HashSet<String> Tags = new HashSet<String>();
					// gold
					Tags.add(linec[1]);
					// crowd
					if (crowd) {
						for (int j = 0; j < crowdcratings.length; j++) {
							if (!Tags.contains(crowdcratings[j])) {
								if (crowdcratings[j].length() > 0) {
									Tags.add(crowdcratings[j]);
								}
							}
						}
					}

					for (String tag : Tags) {
						tpA = tag;
						int start[] = new int[2];
						int end[] = new int[2];
						start[0] = 1;
						end[0] = 1;
						start[1] = tps;
						end[1] = tpe;
						DBTextpassage tp = new DBTextpassage(words + " # " + tpA, start, end);

						HashMap<Long, Double> cfweights = new HashMap<Long, Double>();
						// 100000 = generate user ID
						// gold
						cfweights.put((long) 100000, 1.0);

						// crowd
						if (crowd) {
							for (int j = 0; j < crowdcratings.length; j++) {
								if (crowdcratings[j].equals(tag)) {
									cfweights.put((long) 100000 + j, 1.0);
								}
							}
						}

						for (long cfuserid : cfweights.keySet()) {
							tp.otherweights.put(cfuserid, cfweights.get(cfuserid).toString());
						}

						TextPassages.add(tp);

						HashSet<String> RegisterTopic = RegisterTopics.get(tpA);
						if (RegisterTopic == null)
							RegisterTopic = new HashSet<String>();
						RegisterTopic.add(words);

						RegisterTopics.put(tpA, RegisterTopic);

						if (LemmaIDs.get(tpA) == null)
							LemmaIDs.put(tpA, lemmaIdCounter++);

						String lemmaString = words + " # " + tpA;
						int currentLemmaID;
						if (LemmaIDs.get(lemmaString) == null) {
							currentLemmaID = lemmaIdCounter++;
							LemmaIDs.put(lemmaString, currentLemmaID);
						}
						currentLemmaID = LemmaIDs.get(lemmaString);
						currentLemmaIDs.add(currentLemmaID);
					}

					pos += wlength;
					pos++;

				} else {
					// End of Tweet
					// = new Document;
					tweetc++;

					// documents.put(tweetc, tweet);
					tweet = "";

					// Integer docID = dbc.createNewDocument(text);
					Integer docID = tweetc + addi;

					DocumentLemma.put(docID, currentLemmaIDs);
					DocumentTextPassages.put(docID, TextPassages);
					currentLemmaIDs = new HashSet<Integer>();
					TextPassages = new ArrayList<DBTextpassage>();
					pos = 0;

					// ping
					// dbc.getDocumentTextbydocID(docID);
					dbc.getNextID("extend");
				}

			}

			// Close our input stream
			fingold.close();
			if (!pathcrowd.isEmpty()) {
				fincrowd.close();
			}
			System.out.println("End of File");

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
