/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.exporter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Class for an export of the Lowland results prepared for the vre database.
 *
 */
public class Twitterextporter {

	/**
	 * Method to start the export.
	 * @param args
	 * 
	 */
	public static void main(String[] args) {

		// Stream to read file
		FileInputStream fin;
		BufferedReader din;

		String path = "lowlands.test";

		HashMap<Integer, String> documents = new HashMap<Integer, String>();
		int tweetc = 0;
		try {
			fin = new FileInputStream(path);

			din = new BufferedReader(new InputStreamReader(fin));

			String line = "";
			String tweet = "";

			// Read a line of text
			while ((line = din.readLine()) != null) {
				String linec[] = line.split("\t");
				if (linec.length > 1) {
					// && linec[1] != "."
					if (tweet.length() > 0) {
						tweet += " ";
					}
					tweet += linec[0];
				} else {
					tweetc++;
					documents.put(tweetc, tweet);
					tweet = "";
				}

			}

		} catch (FileNotFoundException e1) {

			e1.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		try {

			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("importtwitter.txt")));

			bw.write("\"50011\"\t\"50051\"\n");

			// for (String document : documents.keySet()) {
			for (int i = 1; i <= tweetc; i++) {
				int document = i;
				bw.write("\"" + document + "\"\t\"" + documents.get(document) + "\"\n");
			}

			bw.flush();
			bw.close();

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
