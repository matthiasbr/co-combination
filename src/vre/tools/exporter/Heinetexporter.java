/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.exporter;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import vre.tools.util.DatabaseConnection;

/**
 * Class for an export of the Lowland results prepared for the vre database.
 *
 */
public class Heinetexporter {

	/**
	 * Method to start the export.
	 * @param args
	 * 
	 */
	public static void main(String[] args) {

		DatabaseConnection dbcon = new DatabaseConnection();

		try {
			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("importheine.txt"), "UTF-8"));

			bw.write("\"50011\"\t\"50051\"\n");

			HashMap<String, String> documents = dbcon.getDocuments();

			for (String document : documents.keySet()) {
				bw.write("\"" + document + "\"\t\"" + documents.get(document) + "\"\n");
			}

		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
