/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.StringEscapeUtils;

import vre.tools.crowd.TextIDContainer;

/**
 * Database Connection. This program is designed to run with two databases: 
 * 
 * Database of the virtual research environment: Research Network and
 * Database system for the Collaborative Research Center CRC 600 Strangers and
 * Poor People by the Trier Center for Digital Humanities, Research Center
 * Europe, Service Center eSciences at Trier University.
 * 
 * Database of the Heinrich Heine portal by the Trier Center for Digital Humanities.
 *
 */
public class DatabaseConnection {

	/**
	 * name of the vre database
	 */
	String db1 = "vre_heine_4";
	
	/**
	 * name of the hhp database
	 */
	String db2 = "HHPdatabase";

	/**
	 * database connection
	 */
	protected Connection con;

	/**
	 * constructor
	 */
	public DatabaseConnection() {

		// Register the JDBC driver for MariaDB.
		try {
			// Class.forName("com.mariadb.jdbc.Driver");

			String url = "jdbc:mariadb://localhost:3306/"+db2+"?useUnicode=yes&characterEncoding=UTF-8";

			System.out.println("DATABASE:" + url);

			String user = "vre";
			String pwd = "change";

			con = DriverManager.getConnection(url, user, pwd);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Gets documents to upload in Crowd Flower.
	 * 
	 * @param ccmax maximum words per document
	 * @return document list
	 */
	public ArrayList<String[]> getDocuments(int ccmax) {

		ArrayList<String[]> Documents = new ArrayList<String[]>();

		String Document[] = new String[2];

		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT objid, pageid, lineid, pos, type, style, sentenceid, word " + "FROM text "
					+ "WHERE left(objid,1) = 'W'" + "ORDER BY objid, pos, pageid, lineid " + "" + ";";

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			String lobjid = "";
			String lpageid = "";
			String llineid = "";
			int lsentenceid = -1;

			String text = "";
			int cc = 0;
			int cp = 0;

			while (rs.next()) {
				String objid = rs.getString(1);
				String pageid = rs.getString(2);
				String lineid = rs.getString(3);
				int pos = rs.getInt(4);
				String type = rs.getString(5);
				int style = rs.getInt(6);
				int sentenceid = rs.getInt(7);
				String word = rs.getString(8);
				word = getTextfromDBstring(word);

				// if (lsentenceid != -1 && lsentenceid != sentenceid) {
				// text += " .";
				// }

				if ((!lobjid.equals("") && !lobjid.equals(objid)) || (cc == ccmax)) {
					String add = "";
					if (cp > 0) {
						add = "_" + cp;
					}
					Document[0] = lobjid + add;
					text = text.replaceAll("\"", "''");
					Document[1] = text;
					Documents.add(Document);
					Document = new String[2];
					text = "";

					if (cc == ccmax) {
						cp++;
					} else {
						cp = 0;
					}
					cc = 0;
				} else if (!lpageid.equals("") && !lpageid.equals(pageid)) {
					text += "\t";
				} else if (!llineid.equals("") && !llineid.equals(lineid)) {
					text += "\t";
				} else {
					text += "\t";
				}

				cc++;

				text += word;

				lobjid = objid;
				lpageid = pageid;
				llineid = lineid;
				lsentenceid = sentenceid;

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return Documents;

	}

	/**
	 * Gets positions in HHP database.
	 * @param objid object id of HHP database
	 * @return positions
	 */
	public ArrayList<Integer> getPositions(String objid) {

		ArrayList<Integer> positionsL = new ArrayList<Integer>();

		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT objid, pageid, lineid, pos, type, style, sentenceid, word, textid " + "FROM text "
					+ "WHERE objid = '" + objid + "'" + "ORDER BY objid, pos, pageid, lineid  " +

					"" + ";";

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			while (rs.next()) {
				positionsL.add(rs.getInt(4));
				System.out.println(rs.getInt(4));

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return positionsL;

	}

	/**
	 * Gets text IDs in HHP database.
	 * @param objid object id of HHP database
	 * @return textid
	 */
	public ArrayList<Long> getTextids(String objid) {

		ArrayList<Long> positionsL = new ArrayList<Long>();

		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT objid, pageid, lineid, pos, type, style, sentenceid, word, textid " + "FROM text "
					+ "WHERE objid = '" + objid + "'" + "ORDER BY objid, pos, pageid, lineid  " +

					"" + ";";

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			while (rs.next()) {
				positionsL.add(rs.getLong(9));
				System.out.println(rs.getLong(9));

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return positionsL;

	}

	/**
	 * Writes an CrowdFlower result in the vre database.
	 * 
	 * @param docID document ID of the vre
	 * @param part word number
	 * @param tid text ID 
	 * @param cfid CrowdFlower ID
	 * @param cfuserid CrowdFlower user ID
	 * @param tag parser tagging
	 * @param trust CrowdFlower trust
	 * @param plb position line begin
	 * @param pcb position char begin
	 * @param ple position line end
	 * @param pce position char end
	 * @param text actual text string
	 */
	public void write_cfresult(int docID, int part, long tid, long cfid, long cfuserid, String tag, double trust,
			int plb, int pcb, int ple, int pce, String text) {
		executeQuery("INSERT INTO " + db1
				+ ".cf_results (docID,part,tid,cfid,cfuserid,tag,trust,plb,pcb,ple,pce,text) VALUES (" + docID + ","
				+ part + "," + tid + "," + cfid + "," + cfuserid + ",'" + tag + "'," + trust + "," + plb + "," + pcb
				+ "," + ple + "," + pce + ",'" + text + "')");
	}

	/**
	 * Deletes CrowdFlower result table.
	 */
	public void truncate_cfresult() {
		executeQuery("DELETE FROM " + db1 + ".cf_results WHERE 1;");
	}

	/**
	 * Gets Documents of HHP database as text.
	 * @return hash map with objid and text
	 */
	public HashMap<String, String> getDocuments() {

		HashMap<String, String> Documents = new HashMap<String, String>();
		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT objid, pageid, lineid, pos, type, style, sentenceid, word " + "FROM text "
					+ "WHERE left(objid,1) = 'W' " + "ORDER BY objid, pos, pageid, lineid " + "" + ";";

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			String lobjid = "";
			String lpageid = "";
			String llineid = "";
			int lsentenceid = -1;

			String text = "";

			while (rs.next()) {
				String objid = rs.getString(1);
				String pageid = rs.getString(2);
				String lineid = rs.getString(3);
				int pos = rs.getInt(4);
				String type = rs.getString(5);
				int style = rs.getInt(6);
				int sentenceid = rs.getInt(7);
				String word = rs.getString(8);
				word = getTextfromDBstring(word);

				// if (lsentenceid != -1 && lsentenceid != sentenceid) {
				// text += " .";
				// }

				if ((!lobjid.equals("") && !lobjid.equals(objid))) {
					String add = "";
					text = text.replaceAll("\"", "''");
					Documents.put(lobjid, text);
					text = "";

				} else if (!lpageid.equals("") && !lpageid.equals(pageid)) {
					text += " ";
				} else if (!llineid.equals("") && !llineid.equals(lineid)) {
					text += " ";
				} else {
					text += " ";
				}

				text += word;

				lobjid = objid;
				lpageid = pageid;
				llineid = lineid;
				lsentenceid = sentenceid;

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return Documents;

	}


	/**
	 * Gets documents of HHPdatabase in database structure .
	 * @return hash map of textid and objects representing the contents of a database entry 
	 */
	public HashMap<String, HashMap<Long, TextIDContainer>> getDocumentContainers() {

		HashMap<String, HashMap<Long, TextIDContainer>> DocumentContainers = new HashMap<String, HashMap<Long, TextIDContainer>>();
		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT objid, pageid, lineid, pos, type, style, sentenceid, word, textid " + "FROM text "
					+ "WHERE left(objid,1) = 'W' " + "ORDER BY objid, pos, pageid, lineid " + "" + ";";

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			String lobjid = "";
			String lpageid = "";
			String llineid = "";
			int lsentenceid = -1;

			HashMap<Long, TextIDContainer> textIDContainers = new HashMap<Long, TextIDContainer>();

			while (rs.next()) {
				String objid = rs.getString(1);
				String pageid = rs.getString(2);
				String lineid = rs.getString(3);
				int pos = rs.getInt(4);
				String type = rs.getString(5);
				int style = rs.getInt(6);
				int sentenceid = rs.getInt(7);
				String word = rs.getString(8);
				word = getTextfromDBstring(word);
				long textid = rs.getLong(9);

				TextIDContainer cTextIDContainer = new TextIDContainer(textid, objid, pageid, lineid, pos, type, style,
						sentenceid, word);

				textIDContainers.put(textid, cTextIDContainer);

				if ((!lobjid.equals("") && !lobjid.equals(objid))) {
					String add = "";
					DocumentContainers.put(lobjid, textIDContainers);
					textIDContainers = new HashMap<Long, TextIDContainer>();

				}

				lobjid = objid;
				lpageid = pageid;
				llineid = lineid;
				lsentenceid = sentenceid;

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return DocumentContainers;

	}

	/**
	 * Gets the according docID in the vre database for a objid of the hhp database.
	 * @param objid
	 * @return docID
	 */
	public int getDocID(String objid) {

		int docID = -1;
		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT docID from " + db1 + ".extend WHERE val = '" + objid + "'";
			;

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			while (rs.next()) {
				docID = rs.getInt(1);
				break;

			}

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return docID;

	}

	/**
	 * Executes a SQL Query.
	 * To be implemented in derived class. 
	 * 
	 * @param sql SQL Query
	 */
	public void executeQuery(String sql) {
		String lastssql = "";
		try {
			Statement s = con.createStatement();

			for (String ssql : sql.split(";;;")) {
				lastssql = ssql;
				s.executeUpdate(ssql);
			}

		} catch (SQLException e) {

			System.out.println("QUERY: " + sql + "\n" + "LAST" + lastssql);
			e.printStackTrace();
			System.exit(1);
		}

		return;

	}

	/**
	 * Converts XML text to plain text.
	 * 
	 * @param text XML containing the text
	 * @return plain text
	 */
	private String getTextfromDBstring(String text) {

		text = text.replaceAll("</hi>", "");
		text = text.replaceAll("<hi[^<>]*>", "");
		text = text.replaceAll("<lb/>", "\n");

		text = StringEscapeUtils.unescapeHtml4(text);

		return text;
	}

}
