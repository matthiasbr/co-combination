/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.crowd;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.commons.lang3.ArrayUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import vre.tools.util.DatabaseConnection;

/**
 * Outputs the data in the database in several formats.
 *
 */
public class ResolveJsonToTable {

	/**
	 * maximum number of entries per CrowdFlower line.
	 */
	static final int maxword = 400;
	/**
	 * set of entity types
	 */
	static final String[] netypes = { "PER", "LOC", "ORG" };
	/**
	 * according background colors
	 */
	static final String[] backgrounds = { "0ff", "f0f", "ff0" };
	/**
	 * different styles
	 */
	static final String[] csstypes = { "d", "s" };

	/**
	 * Format a double value.
	 * 
	 * @param pattern string to describe to pattern
	 * @param value double value
	 * @return string
	 */
	static public String customFormat(String pattern, double value) {
		DecimalFormat myFormatter = new DecimalFormat(pattern);
		String output = myFormatter.format(value);
		return output;
	}

	/**
	 * Generates output as html, tex, tsv
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		DatabaseConnection dbcon = new DatabaseConnection();
		dbcon.truncate_cfresult();

		HashMap<String, HashMap<Long, TextIDContainer>> DocumentContainers = dbcon.getDocumentContainers();

		try {

			BufferedWriter bw = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("samplecomnew.html"), "UTF-8"));
			// only new for test remove later

			BufferedWriter bwl = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("samplecom.tex"), "UTF-8"));

			BufferedWriter bwt = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream("samplecom.txt"), "UTF-8"));

			bw.write("<html><meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\"><head>" + "\n"
					+ "<style type=\"text/css\">" + "\n" + "<!-" + "\n");
			bw.write(".f { background: #f00;} \n");
			for (int a = 0; a < netypes.length; a++) {
				for (String s : csstypes) {
					bw.write("." + s + netypes[a] + " {\n");
					bw.write("  background: #" + backgrounds[a] + ";\n");
					bw.write("}\n");
				}
			}

			bw.write("" + "\n" + "-->" + "\n" + "</style>" + "\n" + "</head>\n<body>" + "\n");

			bwl.write("%start" + "\n");

			int briefe = 0;
			String sCurrentLine;
			String oldBriefId = null;
			String oldgolden = null;
			HashMap<Integer, String[]> ttext = new HashMap<Integer, String[]>();
			HashMap<Integer, Boolean[][]> tneagg = new HashMap<Integer, Boolean[][]>();
			HashMap<Integer, Boolean[][][]> tne = new HashMap<Integer, Boolean[][][]>();
			HashMap<Integer, Double[]> ttrusts = new HashMap<Integer, Double[]>();
			HashMap<Integer, Long[]> tjudgmentIds = new HashMap<Integer, Long[]>();
			HashMap<Integer, Long[]> tworkerIds = new HashMap<Integer, Long[]>();

			HashMap<Long, Long> workers = new HashMap<Long, Long>();
			HashMap<Long, Double> workerstrust = new HashMap<Long, Double>();
			HashMap<Long, Long> workersrev = new HashMap<Long, Long>();
			HashMap<Long, Integer> workersCnt = new HashMap<Long, Integer>();
			long wc = 0;
			HashMap<Long, Long> jugnewIDs = new HashMap<Long, Long>();
			long jnewc = 0;

			HashMap<String, Integer> letterusers = new HashMap<String, Integer>();
			JsonParser parser = new JsonParser();

			ArrayList<String> filelocs = new ArrayList<String>();

			filelocs.add("job.json");

			for (String fileloc : filelocs) {

				BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(fileloc), "UTF-8"));

				sCurrentLine = br.readLine();

				do {

					JsonObject jsonO = parser.parse(sCurrentLine).getAsJsonObject();
					Long dataId = jsonO.get("id").getAsLong();
					JsonObject dataO = jsonO.getAsJsonObject("data");

					String id = dataO.get("id").getAsString();
					String golden = null;
					// _golden
					try {
						if (!dataO.get("answer_gold").isJsonNull()) {
							System.out.print("golden");
							golden = "1";
						}
					} catch (Exception e) {
						System.out.println("EXECPTION golden");

					}
					String[] splitid = id.split("_");
					String briefid = splitid[0];

					int teilid = 0;
					if (splitid.length > 1) {
						teilid = Integer.parseInt(splitid[1]);
					}

					System.out.println(id);

					String text[] = new String[maxword];
					for (int i = 0; i < text.length; i++) {
						try {
							text[i] = dataO.get("t" + i).getAsString();
						} catch (UnsupportedOperationException e) {

							text[i] = "";
						}
					}
					JsonObject results = jsonO.getAsJsonObject("results");
					JsonArray judgments = results.getAsJsonObject().get("judgments").getAsJsonArray();
					JsonObject answeraggO = results.getAsJsonObject().get("answer").getAsJsonObject();
					String answeragg = "";
					Boolean[][] neagg = null;
					try {
						answeragg = answeraggO.get("agg").getAsString();

						neagg = new Boolean[netypes.length][maxword];
						for (int ib = 0; ib < netypes.length; ib++)
							for (int ic = 0; ic < maxword; ic++)
								neagg[ib][ic] = false;
						for (String answerS : answeragg.split(" ")) {
							String[] answerSplit = answerS.split("_");
							String token = answerSplit[0];
							int tokennr = Integer.parseInt(token.replaceAll("t", ""));
							int nenr = ArrayUtils.indexOf(netypes, answerSplit[1]);
							// System.out.println(answerSplit[1] + " " + nenr);
							neagg[nenr][tokennr] = true;
						}
					} catch (UnsupportedOperationException e) {

					} catch (NumberFormatException e) {

					}
					// Count
					int maxletterusers = 0;
					if (letterusers.containsKey(briefid)) {
						maxletterusers = letterusers.get(briefid);
					}
					if (judgments.size() > maxletterusers) {
						maxletterusers = judgments.size();
					}
					letterusers.put(briefid, maxletterusers);

					//
					Boolean[][][] ne = new Boolean[judgments.size()][netypes.length][maxword];
					Double[] trusts = new Double[judgments.size()];
					Long[] judgmentIds = new Long[judgments.size()];
					for (int ia = 0; ia < judgments.size(); ia++)
						for (int ib = 0; ib < netypes.length; ib++)
							for (int ic = 0; ic < maxword; ic++)
								ne[ia][ib][ic] = false;
					Long workerIds[] = new Long[judgments.size()];
					int jc = -1;
					for (JsonElement judgmentE : judgments) {
						jc++;

						JsonObject judgment = judgmentE.getAsJsonObject();

						Long judgmentId = judgment.get("id").getAsLong();
						Long judgnewId = jugnewIDs.get(judgmentId);
						if (judgnewId == null) {
							jnewc++;
							jugnewIDs.put(judgmentId, jnewc);
						}
						
						
						Double trust = judgment.get("trust").getAsDouble();
						Double worker_trust = null;
						try {
							worker_trust = judgment.get("worker_trust").getAsDouble();
						} catch (UnsupportedOperationException e) {

						}
						Long workerId = judgment.get("worker_id").getAsLong();
						Long worker = workers.get(workerId);
						if (worker == null) {
							wc++;
							workers.put(workerId, wc);
							workersrev.put(wc, workerId);
							worker = wc;
							workerstrust.put(wc, worker_trust);
						}

						workerIds[jc] = worker;
						trusts[jc] = trust;
						judgmentIds[jc] = judgmentId;

						// System.out.print(workerId+".");
						try {
							JsonArray answer = judgment.getAsJsonObject("data").get("answer").getAsJsonArray();

							for (JsonElement answerE : answer) {
								String answerS = answerE.getAsString();
								String[] answerSplit = answerS.split("_");
								String token = answerSplit[0];
								int tokennr = Integer.parseInt(token.replaceAll("t", ""));
								int nenr = ArrayUtils.indexOf(netypes, answerSplit[1]);
								// System.out.println(answerSplit[1] + " " + nenr);
								ne[jc][nenr][tokennr] = true;
							}
						} catch (java.lang.NullPointerException e) {
							System.out.println("No answer");
							// bw.write("<b>No answer</b>");
						}

					}

					sCurrentLine = br.readLine();

					if (sCurrentLine == null) {
						ttext.put(teilid, text);
						tneagg.put(teilid, neagg);
						tne.put(teilid, ne);
						tworkerIds.put(teilid, workerIds);
						ttrusts.put(teilid, trusts);
						tjudgmentIds.put(teilid, judgmentIds);

					}
					if (((oldBriefId != null) && (!oldBriefId.equals(briefid)) || sCurrentLine == null)) {
						HashMap<Long, TextIDContainer> cDocument = DocumentContainers.get(oldBriefId);
						bw.write("<br/><h2>ID: " + oldBriefId + "</h2>");
						briefe++;
						bwl.write("\n\\clBrief{");
						bwl.write("\n\\clBriefStart{}\\clBriefID{" + oldBriefId + "}\n");
						if (oldgolden != null) {
							bw.write("(Testquestion)");
							bwl.write("\\clWasTestquestion{}\n");
						}
						ArrayList<Integer> positions = dbcon.getPositions(oldBriefId);
						ArrayList<Long> textids = dbcon.getTextids(oldBriefId);
						SortedMap<Integer, Integer> positionsMap = new TreeMap<Integer, Integer>();
						// long[] ltextids = new long [positions.size()];

						for (int m = 0; m < positions.size(); m++)
							positionsMap.put(positions.get(m), m);
						// ltextids[m] = textids.get

						int oldkey = -1;
						HashMap<Long, Integer> cworkerCnt = new HashMap<Long, Integer>();
						bw.write("<br />\n");
						bwl.write("\n");
						// recalc tk text
						int slpos = 1;
						int elpos = 1;
						int spos = 0;
						int epos = 0;
						String lorgline = "";
						String lorgpage = "";
						String orgline = "";
						String orgpage = "";
						int orgpos = -1;
						for (Integer position : positionsMap.keySet()) {
							lorgline = orgline;
							lorgpage = orgpage;
							int pos = positionsMap.get(position);
							int k = pos % 400;
							int key = pos / 400;
							// System.out.println(position +":" +pos +" is "+key +":"+ k);
							long textid = textids.get(pos);
							TextIDContainer cTextIDContainer = null;
							if (cDocument.containsKey(textid)) {
							     cTextIDContainer = cDocument.get(textid);
							}
							if (cTextIDContainer!= null) {
								orgline = cTextIDContainer.getLineid();
								orgpage = cTextIDContainer.getPageid();
								orgpos = cTextIDContainer.getPos();
								if (lorgpage != orgpage) {
									bwl.write("\n\\clPage{" + lorgpage + "}\n");
									bw.write("\n<hr>Page" + lorgpage + "<hr>\n");
								}
								if (lorgline != orgline) {
									bwl.write("\n\\clLine{" + lorgline + "}\n");
									bw.write("\n<sup>" + lorgline + "</sup>\n");
								}
							}
							else {
								System.out.println("Textid NOT FOUND");
							}

							String[] ctext = ttext.get(key);
							if (ctext == null) {
								bw.write("."); // key + Abschnitt nicht gefunden <br />\n
								bwl.write(".");
								continue;
							}
							Boolean[][][] cne = tne.get(key);
							Boolean[][] cneagg = tneagg.get(key);
							Long[] cworkerIds = tworkerIds.get(key);
							Double[] ctrusts = ttrusts.get(key);
							Long[] cjudgmentIds = tjudgmentIds.get(key);

							int lentext = ctext[k].length();
							epos += lentext;

							if (oldkey != key) {
								if (oldkey != -1) {
									bwl.write("%Ende Briefpassage\n}");
								}

								// bw.write("<br />\n");
								bw.write("<hr /><b>Trust: " + cworkerIds.length + " user</b>: ");
								bwl.write("\n\\clSeparator{}\n\\clTrustInfo{\\clTrust{" + cworkerIds.length + "}");
								for (int t = 0; t < cworkerIds.length; t++) {
									try {
										bw.write("Nr. " + cworkerIds[t] + " : " + customFormat("0.000", ctrusts[t])
												+ " (" + customFormat("0.00", workerstrust.get(cworkerIds[t])) + "). ");
										bwl.write("\\clUserNr{" + cworkerIds[t] + "}{"
												+ customFormat("0.000", ctrusts[t]) + "}{"
												+ customFormat("0.00", workerstrust.get(cworkerIds[t])) + "}");
									} catch (NullPointerException e) {
										bw.write("Sata: " + t + ".");
										bwl.write("\\clSata{" + t + "}");
										System.out.println(e);
									}
								}
								bw.write("<hr />");
								bwl.write("}\n\\clSeparator{}\n\\clPassage{%Anfang Briefpassage\n");
								// bw.write("<br />");
								oldkey = key;

							}

							String htmlclass = "";
							String htmlsup = "";
							String latexsup = "";
							int[] classcnt = new int[netypes.length];
							for (int kn = 0; kn < netypes.length; kn++) {
								classcnt[kn] = 0;
							}

							// try {
							for (int kn = 0; kn < netypes.length; kn++) {

								if (cneagg[kn][k]) {
									htmlclass += "d" + netypes[kn] + " ";
									htmlsup += ("<sup style=\"border:solid;border-width:1px\" class=\"s" + netypes[kn]
											+ "\">AGG</sup>");
									latexsup += "\\clsup{" + netypes[kn] + "}{AGG}";
								}

							}

							for (int kj = 0; kj < cne.length; kj++) {
								for (int kn = 0; kn < netypes.length; kn++) {

									if (cne[kj][kn][k]) {
										htmlclass = "d" + netypes[kn] + " "; // ` '
										htmlsup += ("<sup style=\"border:solid;border-width:1px\" class=\"s"
												+ netypes[kn] + "\">" + cworkerIds[kj] + "</sup>");
										classcnt[kn]++;
										latexsup += "\\clsup{" + netypes[kn] + "}{" + cworkerIds[kj] + "}";
										// System.out.print(cworkerIds[kj]+",");
										if (cworkerCnt.containsKey(cworkerIds[kj])) {
											int cnt = cworkerCnt.get(cworkerIds[kj]);
											cnt++;
											cworkerCnt.put(cworkerIds[kj], cnt);
										} else {
											cworkerCnt.put(cworkerIds[kj], 1);
										}

										int docID = dbcon.getDocID(oldBriefId);
										try {
											dbcon.write_cfresult(docID, key, k, cjudgmentIds[kj],
													workersrev.get(cworkerIds[kj]), netypes[kn], ctrusts[kj], slpos,
													spos, elpos, epos, ctext[k].replaceAll("'", "\\\\'"));
										} catch (Exception e) {

										}
										bwt.write(oldBriefId);
										bwt.write('\t');
										bwt.write(orgpage);
										bwt.write('\t');
										bwt.write(orgline);
										bwt.write('\t');
										bwt.write(orgpos + "");
										bwt.write('\t');
										bwt.write(jugnewIDs.get(cjudgmentIds[kj]).toString());
										bwt.write('\t');
										bwt.write(workers.get(workersrev.get(cworkerIds[kj])).toString());
										bwt.write('\t');
										bwt.write(netypes[kn]);
										bwt.write('\t');
										bwt.write(ctext[k]);
										bwt.write('\t');
										bwt.write(ctrusts[kj].toString());
										bwt.write('\t');
										bwt.write(workerstrust.get(cworkerIds[kj]).toString());
										bwt.write('\n');
										bwt.flush();

									}

								}
							}

							bw.write("<span>");

							if (htmlclass != "") {
								String ctextv = ctext[k];
								String ctexta = ctextv;
								String ctextal = "";
								if (ctextv.length() > 2) {
									String ctextvl = ctextv.substring(ctextv.length() - 1, ctextv.length());
								}
								bw.write("<span class=\" " + htmlclass + "\" >" + ctexta + "</span>");
								// bwl.write("\\clMain{"+latexmain+"}{"+ctext[k]+"}");

								bwl.write("\\clEntry{" + ctexta + "}");
								for (int kn = 0; kn < netypes.length; kn++) {
									bwl.write("{" + classcnt[kn] + "}");
								}
								bwl.write("{");
								bwl.write(latexsup);
								bwl.write("}");
								bw.write(ctextal);
								bwl.write(ctextal);
							} else {
								bw.write(ctext[k]);
								bwl.write(ctext[k]);
							}

							bw.write(htmlsup);
							bwl.write(" ");
							bw.write(" </span>");
							spos = epos + 1;
							epos = spos;

						}
						ttext.clear();
						tne.clear();
						tneagg.clear();
						ttrusts.clear();

						// bw.write("<br />\n");
						bw.write("<hr />");
						bwl.write("%Ende Briefpassage\n}\n\\clSeparator{}\n");
						int cntthreshold = 0;
						int cntuser = 0;
						for (Entry<Long, Integer> cw : cworkerCnt.entrySet()) {
							if (cw.getValue() > 0) {
								cntuser++;
								cntthreshold += cw.getValue();
							}
						}
						if (cntuser > 0) {
							cntthreshold = cntthreshold / cntuser / 4;
							bw.write("T: " + cntthreshold + ". ");
							bwl.write("\\clcntthreshold{" + cntthreshold + "}");
							for (Entry<Long, Integer> cw : cworkerCnt.entrySet()) {
								if (cw.getValue() > 0) {
									bw.write("Nr. " + cw.getKey() + " : " + cw.getValue());
									bwl.write("\\clcwNr{" + cw.getKey() + "}{" + cw.getValue() + "}");
									if (cw.getValue() < cntthreshold) {
										if (workersCnt.containsKey(cw.getKey())) {
											int cnt = cworkerCnt.get(cw.getKey());
											cnt++;
											workersCnt.put(cw.getKey(), cnt);
										} else {
											workersCnt.put(cw.getKey(), 1);
										}
									} else {
										bw.write(". ");
									}
								}
							}
						}
						bw.write("<hr />");
						bwl.write("}\\clBriefEnd{}\n");
						bwl.write("\n\\clSeparator{}\n");
						// bw.write("<br />");
						cworkerCnt.clear();

					}

					// bw.write(" <b>"+worker+"("+workerId+") :"+trust+"</b>");

					if (sCurrentLine != null) {
						ttext.put(teilid, text);
						tne.put(teilid, ne);
						tneagg.put(teilid, neagg);
						tworkerIds.put(teilid, workerIds);
						ttrusts.put(teilid, trusts);
						tjudgmentIds.put(teilid, judgmentIds);
					}

					oldBriefId = briefid;
					oldgolden = golden;

					// }

				} while (sCurrentLine != null);

			}

			bw.write("\n<h3>Worker Trust</h3>");
			bwl.write("\\clWorkerTrust{}");
			for (int i = 1; i < workersrev.size(); i++) {
				Long wid = workersrev.get((long) i);
				Double val = workerstrust.get((long) i);
				if (val == null)
					val = 1.0;
				bw.write("<p>" + wid + " : " + workers.get(wid) + " : " + val + "</p>");
				bw.write("\\clWid{" + wid + "}{" + workers.get(wid) + "}{" + val + "}");
			}

			for (Entry<Long, Integer> e : workersCnt.entrySet()) {
				bw.write("<p>" + e.getKey() + " : " + e.getValue() + "</p>");
				bw.write("\\clWidE{" + e.getKey() + "}{" + e.getValue() + "}");
			}

			bw.write("\n</body></html>");
			bw.flush();
			bw.close();
			bwl.flush();
			bwl.close();
			bwt.flush();
			bwt.close();

			System.out.println("Briefe: " + briefe);

		} catch (JsonIOException e) {

			e.printStackTrace();
		} catch (JsonSyntaxException e) {

			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

	}

}
