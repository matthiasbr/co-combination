/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.crowd;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;

import com.opencsv.CSVReader;

import vre.tools.util.DatabaseConnection;

/**
 * Generates the templates for the crowd-sourcing platform CrowdFlower.
 *
 */
public class CrowdTemplateGenerator {

	/**
	 * maximum number of entries per CrowdFlower line.
	 */
	public static int ccmax = 400;

	public static void cml() {
		String caption = "caption";
		String noEntitiesCaption = "noEntitiesCaption";

		StringWriter cml = new StringWriter();
		cml.append("<div class=\"gate-snippet\">\n" + "  <cml:checkboxes validates=\"required\" label=\"");
		// StringEscapeUtils.escapeXml(cml, caption);
		cml.append("\" name=\"answer\">\n" + "    {% for tok in tokens %}\n"
				+ "      <cml:checkbox label=\"{{ tok }}\" value=\"{{ forloop.index0 }}\" />\n" + "    {% endfor %}\n"
				+ "  </cml:checkboxes>\n" + "</div>\n" + "{% if detail %}\n"
				+ "  <div class=\"well\">{{detail}}</div>\n" + "{% endif %}\n" + "<div class=\"gate-no-entities\">\n"
				// TODO check how to customize the validation error
				// message
				+ "  <cml:checkbox name=\"noentities\" label=\"");
		// StringEscapeUtils.escapeXml(cml, noEntitiesCaption);
		cml.append("\" value=\"1\"\n" + "      only-if=\"!answer:required\" validates=\"required\"/>\n" + "</div>\n");
		System.out.println("cml: " + cml.toString());

	}

	public static void cml2r() {
		String caption = "caption";
		String noEntitiesCaption = "noEntitiesCaption";

		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("my.cml"), "UTF-8"));
			writer.write("<div id=\"myarea\" class=\"myarea\">\n");
			writer.write("{% assign no = 'No data available' %}" + "\n");
			writer.write(
					"{% assign ic = '<div class=\"whit\" tabindex=\"0\" ><i class=\"icon-remove-sign\"></i> schlie�en</div>' %}"
							+ "\n");
			// writer,writer("tokens\n")
			for (int i = 0; i < ccmax; i++) {
				String ti = "t" + i;

				StringWriter cml = new StringWriter();
				cml.append("{% if " + ti + " != no %}" + "\n" + "<div class=\"mys\" >" + "\n"
						+ "<div tabindex=\"0\" class=\"myt\" id=\"{{  id  }}" + ti + "\">{{ " + ti + " }}</div>" + "\n"
						+ "<div id=\"myc{{  id  }}" + ti + "\" class=\"myc\" name=\"myc\" >" + "\n"
						+ "<cml:radios label=\"\" name=\"answer" + ti + "\">\n"
						+ "<cml:radio class=\"sPER\" label=\"Person\"></cml:radio>" + "\n"
						+ "<cml:radio class=\"sLOC\" label=\"Ort\" ></cml:radio>" + "\n"
						+ "<cml:radio class=\"sORG\"  label=\"Organisation\" ></cml:radio>" + "\n" + "</cml:radios>"
						+ "\n" + "{{ ic }}" + "\n" + "</div>" + "\n" + "</div>" + "\n" + "{% endif %}" + "\n");

				writer.write(cml.toString().replaceAll("\n", ""));
				// writer.write("\n");
			}

			writer.write("<br clear=\"all\" />" + "\n");
			writer.write("<br />" + "\n");
			writer.write(
					"<cml:checkbox name=\"noentities\" label=\"es sind keine Namen im Text vorhanden\" value=\"1\" validates=\"required\"></cml:checkbox>"
							+ "\n");
			writer.write("</div>");
			writer.write("\n");

			writer.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static void cml2o() {
		String caption = "caption";
		String noEntitiesCaption = "noEntitiesCaption";

		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("my.cml"), "UTF-8"));
			writer.write("<div id=\"myarea\" class=\"myarea\">\n");
			writer.write("<cml:checkboxes label=\"\" name=\"answer\">\n");
			writer.write("{% assign no = 'No data available' %}" + "\n");
			writer.write(
					"{% assign ic = '<div class=\"whit\" tabindex=\"0\" ><i class=\"icon-remove-sign\"></i> schlie�en</div>' %}"
							+ "\n");
			// writer,writer("tokens\n")
			for (int i = 0; i < ccmax; i++) {
				String ti = "t" + i;

				StringWriter cml = new StringWriter();
				cml.append("{% if " + ti + " != no %}" + "\n" + "<div class=\"mys\" >" + "\n"
						+ "<div tabindex=\"0\" class=\"myt\" id=\"{{  id  }}" + ti + "\">{{ " + ti + " }}</div>" + "\n"
						+ "<div id=\"myc{{  id  }}" + ti + "\" class=\"myc\" name=\"myc\" >" + "\n"
						+ "<div class=\"sPER\" ><cml:checkbox  label=\"Person\" value=\"" + ti
						+ "_PER\"></cml:checkbox></div>" + "\n"
						+ "<div class=\"sLOC\" ><cml:checkbox  label=\"Ort\" value=\"" + ti
						+ "_LOC\"></cml:checkbox></div>" + "\n"
						+ "<div class=\"sORG\" ><cml:checkbox  label=\"Organisation\" value=\"" + ti
						+ "_ORG\"></cml:checkbox></div>" + "\n" + "{{ ic }}" + "\n" + "</div>" + "\n" + "</div>" + "\n"
						+ "{% endif %}" + "\n");

				writer.write(cml.toString().replaceAll("\n", ""));
				writer.write("\n");
			}
			writer.write("</cml:checkboxes>" + "\n");
			writer.write("<br clear=\"all\" />" + "\n");
			writer.write("<br />" + "\n");
			writer.write(
					"<cml:checkbox name=\"noentities\" label=\"es sind keine Namen im Text vorhanden\" value=\"1\" ></cml:checkbox>"
							+ "\n");
			writer.write("</div>");
			writer.write("\n");

			writer.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static void cml2() {
		String caption = "caption";
		String noEntitiesCaption = "noEntitiesCaption";

		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("my.cml"), "UTF-8"));
			writer.write("<div id=\"myarea\" class=\"myarea\">\n");
			writer.write("<cml:checkboxes label=\"\" name=\"answer\" validates=\"required\">\n");
			// writer.write("{% assign no = 'No data available' %}"+"\n");
			// writer.write("{% assign ic = '<div class=\"whit\" tabindex=\"0\" ><i
			// class=\"icon-remove-sign\"></i> schließen</div>' %}"+"\n");
			// writer,writer("tokens\n")
			for (int i = 0; i < ccmax; i++) {
				String ti = "t" + i;

				StringWriter cml = new StringWriter();
				cml.append("<a>{{" + ti + "}}</a>" + "\n" + "<cml:checkbox value=\"" + ti + "_PER\"></cml:checkbox>"
						+ "\n" + "<cml:checkbox value=\"" + ti + "_LOC\"></cml:checkbox>" + "\n"
						+ "<cml:checkbox value=\"" + ti + "_ORG\"></cml:checkbox>" + "\n" + "" + "\n");

				writer.write(cml.toString().replaceAll("\n", ""));
				// writer.write("\n");
			}
			writer.write("</cml:checkboxes>" + "\n");
			writer.write("</div>");
			writer.write("<br clear=\"all\" />" + "\n");
			writer.write("<br />" + "\n");
			writer.write(
					"<cml:checkbox name=\"noentities\" label=\"es sind keine Namen im Text vorhanden\" value=\"1\" only-if=\"!answer:required\" validates=\"required\" ></cml:checkbox>"
							+ "\n");

			writer.write("\n");

			writer.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static void cml4() {
		String caption = "caption";
		String noEntitiesCaption = "noEntitiesCaption";

		try {
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("my.cml"), "UTF-8"));
			writer.write("<div id=\"myarea\" class=\"myarea\">\n");
			// writer.write("<cml:checkboxes label=\"\" name=\"answer\"
			// validates=\"required\">\n");
			// writer.write("{% assign no = 'No data available' %}"+"\n");
			// writer.write("{% assign ic = '<div class=\"whit\" tabindex=\"0\" ><i
			// class=\"icon-remove-sign\"></i> schließen</div>' %}"+"\n");
			// writer,writer("tokens\n")
			for (int i = 0; i < ccmax; i++) {
				String ti = "t" + i;

				StringWriter cml = new StringWriter();
				cml.append("<a>{{" + ti + "}}</a>" + "\n" + "<cml:checkbox label=\"" + ti + "_PER\"></cml:checkbox>"
						+ "\n" + "<cml:checkbox label=\"" + ti + "_LOC\"></cml:checkbox>" + "\n"
						+ "<cml:checkbox label=\"" + ti + "_ORG\"></cml:checkbox>" + "\n" + "" + "\n");

				writer.write(cml.toString().replaceAll("\n", ""));
				// writer.write("\n");
			}
			// writer.write("</cml:checkboxes>" + "\n");
			writer.write("</div>");
			writer.write("<br clear=\"all\" />" + "\n");
			writer.write("<br />" + "\n");
			writer.write(
					"<cml:checkbox name=\"noentities\" label=\"es sind keine Namen im Text vorhanden\" value=\"1\" only-if=\"!answer:required\" validates=\"required\" ></cml:checkbox>"
							+ "\n");

			writer.write("\n");

			writer.close();

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	public static void tsv() {

		DatabaseConnection dbcon = new DatabaseConnection();
		ArrayList<String[]> Documents = dbcon.getDocuments(ccmax);

		try {

			CSVReader reader = new CSVReader(new FileReader("job_gold_report.csv"));
			HashMap<String, String> answer_goldMap = new HashMap<String, String>();
			HashMap<String, String> answer_gold_reasonMap = new HashMap<String, String>();

			String[] nextLine;
			while ((nextLine = reader.readNext()) != null) {
				// nextLine[] is an array of values from the line
				System.out.println(nextLine[0] + nextLine[10] + "etc...");
				answer_goldMap.put(nextLine[10], nextLine[8]);
				answer_gold_reasonMap.put(nextLine[10], nextLine[9]);

			}

			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("data4a.tsv"), "UTF-8"));
			writer.write(
					"ID\tlen\twc\t_golden\tanswer_gold\tanswer_gold_reason\tnoentities_gold\tnoentities_gold_reason\t");
			// writer,writer("tokens\n")
			for (int i = 0; i < ccmax; i++) {
				writer.write("t" + i + "\t");
			}
			writer.write("\n");
			int count = 0;
			for (String[] document : Documents) {
				String golden = "";
				String answer_gold = "";
				String answer_gold_reason = "";
				String noentities = "";
				if (answer_goldMap.containsKey(document[0])) {
					golden = "TRUE";
					answer_gold = "\"" + answer_goldMap.get(document[0]).replaceAll("\"", "''") + "\"";
					answer_gold_reason = "\"" + answer_gold_reasonMap.get(document[0]).replaceAll("\"", "''") + "\"";
					noentities = "0";
				}
				String line = document[0] + "\t" + document[1].length() + "\t" + document[1].split("\t").length + "\t"
						+ golden + "\t" + answer_gold + "\t\t" + noentities + "\t" + answer_gold_reason + "\t"
						+ document[1] + "\n";
				writer.write(line);
				count++;
			}
			writer.close();
			System.out.println(count + " units " + Documents.size());

		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//
		cml4();
		tsv();

	}

}
