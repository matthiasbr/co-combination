/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.tools.crowd;

/**
 * Container for text parts of the hhp database.
 *
 */
public class TextIDContainer {

	/**
	 * ID of the text
	 */
	Long textid;
	/**
	 * ID of the object (e. g. letter)
	 */
	String objid;
	/**
	 * ID of the page
	 */
	String pageid;
	/**
	 * ID of the line on the page
	 */
	String lineid;
	/**
	 * position in text
	 */
	int pos;
	/**
	 * type of text
	 */
	String type;
	/**
	 * style information
	 */
	int style;
	/**
	 * ID of the sentence
	 */
	int sentenceid;
	/**
	 * string representing the word
	 */
	String word;

	/**
	 * getter
	 * @return objid
	 */
	public String getObjid() {
		return objid;
	}

	/**
	 * setter
	 * @param objid
	 */
	public void setObjid(String objid) {
		this.objid = objid;
	}

	/**
	 * getter
	 * @return pageid
	 */
	public String getPageid() {
		return pageid;
	}

	/**
	 * setter 
	 * @param pageid
	 */
	public void setPageid(String pageid) {
		this.pageid = pageid;
	}

	/**
	 * getter
	 * @return lineid
	 */
	public String getLineid() {
		return lineid;
	}

	/**
	 * setter
	 * @param lineid
	 */
	public void setLineid(String lineid) {
		this.lineid = lineid;
	}

	/**
	 * getter
	 * @return pos
	 */
	public int getPos() {
		return pos;
	}

	/**
	 * setter
	 * @param pos
	 */
	public void setPos(int pos) {
		this.pos = pos;
	}

	/**
	 * getter
	 * @return type
	 */
	public String getType() {
		return type;
	}

	/**
	 * setter
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * getter
	 * @return style
	 */
	public int getStyle() {
		return style;
	}

	/**
	 * setter
	 * @param style
	 */
	public void setStyle(int style) {
		this.style = style;
	}

	/**
	 * getter
	 * @return sentenceid
	 */
	public int getSentenceid() {
		return sentenceid;
	}

	/**
	 * setter
	 * @param sentenceid
	 */
	public void setSentenceid(int sentenceid) {
		this.sentenceid = sentenceid;
	}

	/**
	 * getter
	 * @return word
	 */
	public String getWord() {
		return word;
	}

	/**
	 * setter
	 * @param word
	 */
	public void setWord(String word) {
		this.word = word;
	}

	/**
	 * contructor
	 * @param textid
	 * @param objid
	 * @param pageid
	 * @param lineid
	 * @param pos
	 * @param type
	 * @param style
	 * @param sentenceid
	 * @param word
	 */
	public TextIDContainer(Long textid, String objid, String pageid, String lineid, int pos, String type, int style,
			int sentenceid, String word) {
		super();
		this.textid = textid;
		this.objid = objid;
		this.pageid = pageid;
		this.lineid = lineid;
		this.pos = pos;
		this.type = type;
		this.style = style;
		this.sentenceid = sentenceid;
		this.word = word;
	}

	/**
	 * constructor
	 */
	public TextIDContainer() {

	}

	/**
	 * getter
	 * @return textid
	 */
	public Long getTextid() {
		return textid;
	}

	/**
	 * setter 
	 * @param textid
	 */
	public void setTextid(Long textid) {
		this.textid = textid;
	}

}
