/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.util;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * Reads the configuration file and returns elements.
 *
 */
public class Configuration {

	/**
	 * Container for XML document.
	 */
	private static Document doc;
	/**
	 * Root Element of XML document.
	 */
	private static Element root;

	/**
	 * Parses the given XML file.
	 * @param filename name of the XML file
	 */
	public static void setConfigXml(String filename) {
		doc = parseXmlFile(filename, false);
		root = doc.getDocumentElement();
	}

	/**
	 * Returns root element of XML.
	 * @return root
	 */
	public static Element getConfigXMLRoot() {
		return root;
	}

	/**
	 * Returns a configuration value.
	 * @param name name of the configuration to read
	 * @return configuration value
	 */
	public static String getConfigXMLvalue(String name) {
		String value;

		NodeList valueNodes = root.getElementsByTagName(name);

		Node valueNode = valueNodes.item(0);
		value = valueNode.getTextContent();

		return value;
	}

	/**
	 * Attempts to parse the XML file returns a DOM document.
	 * @param filename name of the XML file
	 * @param validating If validating is true, the contents is validated against the DTD specified in the file.
	 * @return null
	 */
	private static Document parseXmlFile(String filename, boolean validating) {
		filename = filename.replaceAll("/", File.separator);

		try {
			// Create a builder factory
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(validating);

			// Create the builder and parse the file
			Document doc = factory.newDocumentBuilder().parse(new File(filename));
			return doc;
		} catch (SAXException e) {
			System.out.println(e);
		} catch (ParserConfigurationException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		return null;
	}

}
