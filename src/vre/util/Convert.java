/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Conversions for elements of the VREs programming language Tcl/Tk.
 *
 */
public class Convert {
	/**
	 * Converts ArrayList to Tcl list.
	 * @param toB ArrayList of strings.
	 * @return string which represents a Tcl list
	 */
	public static String l(ArrayList<String> toB) {
		StringBuffer l = new StringBuffer();
		Iterator<String> toBiterator = toB.iterator();
		while (toBiterator.hasNext()) {
			String toBnext = toBiterator.next();

			Boolean aB = true;
			if (toBnext.indexOf(32) == -1)
				aB = false;
			if (toBnext.indexOf(13) != -1)
				aB = true;
			if (toBnext.indexOf(10) != -1)
				aB = true;
			if (toBnext.isEmpty())
				aB = true;

			if (aB)
				l.append("{");
			l.append(toBnext);
			if (aB)
				l.append("}");

			if (toBiterator.hasNext())
				l.append(" ");

		}
		return l.toString();
	}

	/**
	 * Converts a Tcl list to Array List.
	 * @param toAL string which represents the Tcl list
	 * @return ArrayList of strings
	 */
	public static ArrayList<String> aL(String toAL) {
		String[] sArr = toAL.split(" ");
		ArrayList<String> aL = new ArrayList<String>();
		for (int i = 0; i < sArr.length; i++) {
			if (sArr[i].startsWith("{"))
				sArr[i] = sArr[i].substring(1, sArr[i].length() - 2);
			aL.add(sArr[i]);
		}

		return aL;
	}

	/**
	 * Converts position in string to a format "line.char", which is used in the Tcl/Tk textwidget.
	 * @param number postion in string
	 * @param text string
	 * @return int array containing line and char position
	 */
	public static int[] getPosFromNumber(int number, String text) {
		int line = 0;
		int column = 0;

		String[] textSplit = text.split("\n");
		int textLines = textSplit.length;
		int counter = 0;
		for (line = 0; line < textLines; line++) {

			int linelength = textSplit[line].length();

			if (counter + linelength + 1 > number) { // number includes \n therefore +1
				// Log.o3(textSplit[line] + " " + textSplit[line].length());

				column = number - counter;
				break;

			}

			counter += linelength + 1; // number includes \n therefore +1

		}

		int[] lineColumn = new int[3];
		lineColumn[0] = line + 1; // Tk line starts with 1
		lineColumn[1] = column;
		lineColumn[2] = number;
		Log.o3(getPosFromNumberToString(lineColumn));
		return lineColumn;

	}

	/**
	 * Generates debug print string for text position.
	 * @param pos position array
	 * @return debug string
	 */
	public static String getPosFromNumberToString(int[] pos) {
		return "L:" + pos[0] + "C:" + pos[1] + "was:" + pos[2];
	}

	/**
	 * Shortcut to convert string to double.
	 * @param s string
	 * @return double
	 */
	public static double getDoubleFromString(String s) {
		return Double.parseDouble(s);
	}

}
