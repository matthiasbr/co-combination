/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import vre.analysis.AbstractAnalyzer;
import vre.analysis.DatabaseConnection;
import vre.analysis.util.Cfunit;
import vre.analysis.util.DBTextpassage;

/**
 * Imports the crowd flower data.
 *
 */
public class ImportCrowdflower extends AbstractAnalyzer {

	/**
	 * Starts the import.
	 * @param dbcon database connection
	 */
	public void doimport(DatabaseConnection dbcon) {

		ArrayList<Integer> cfdocs = dbcon.getCFdocIDs();

		for (int docID : cfdocs) {

			ArrayList<Cfunit> cfunits = dbcon.getCFTextpassages(docID);

			HashSet<Integer> currentLemmaIDs = new HashSet<Integer>();
			ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();

			for (Cfunit cfunit : cfunits) {

				DBTextpassage textpassage = new DBTextpassage();
				String cftext = cfunit.getText();
				String cftag = cfunit.getTag();
				textpassage.setName(cftext + " # " + cftag);
				int[] start = new int[2];
				start[0] = cfunit.getPlb();
				start[1] = cfunit.getPcb();
				textpassage.setStart(start);
				int[] end = new int[2];
				end[0] = cfunit.getPle();
				end[1] = cfunit.getPce();
				textpassage.setEnd(end);
				// textpassage.setWeight(cfunit.getTrust()+"");
				HashMap<Long, Double> cfweights = dbcon.getCFTextpassageWeight(docID, cfunit.getPart(),
						cfunit.getTid());

				for (long cfuserid : cfweights.keySet()) {
					textpassage.otherweights.put(cfuserid, cfweights.get(cfuserid).toString());
				}

				TextPassages.add(textpassage);

				HashSet<String> RegisterTopic = RegisterTopics.get(cfunit.getTag());
				if (RegisterTopic == null)
					RegisterTopic = new HashSet<String>();
				RegisterTopic.add(cftext);

				RegisterTopics.put(cftag, RegisterTopic);

				if (LemmaIDs.get(cftag) == null)
					LemmaIDs.put(cftag, lemmaIdCounter++);

				String lemmaString = cftext + " # " + cftag;
				int currentLemmaID;
				if (LemmaIDs.get(lemmaString) == null) {
					currentLemmaID = lemmaIdCounter++;
					LemmaIDs.put(lemmaString, currentLemmaID);
				}
				currentLemmaID = LemmaIDs.get(lemmaString);
				currentLemmaIDs.add(currentLemmaID);

			}
			DocumentLemma.put(docID, currentLemmaIDs);
			DocumentTextPassages.put(docID, TextPassages);

		}

	}

}
