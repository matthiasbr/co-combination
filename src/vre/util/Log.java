/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.util;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * A logger class.
 *
 */
public class Log {

	/**
	 * Holds the detail level of logging.
	 */
	static int detail = 2;
	
	/**
	 * Create a log file.
	 */
	static boolean doLogFile = false;

	/**
	 * Shortcut for log level 3. 
	 * @param s string to log
	 */
	public static void o3(String s) {
		o(s, 3, true);
	}
	
	/**
	 * Shortcut for log level 3 with no newline. 
	 * @param s string to log
	 */
	public static void o3n(String s) {
		o(s, 3, false);
	}

	/**
	 * Shortcut for log level 2.
	 * @param s string to log
	 */
	public static void o2(String s) {
		o(s, 2, true);
	}

	/**
	 * Shortcut for log level 2 with no newline.
	 * @param s string to log
	 */
	public static void o2n(String s) {
		o(s, 2, false);
	}
	
	/**
	 * Shortcut for log level 1.
	 * @param s string to Log
	 */
	public static void o1(String s) {
		o(s, 1, true);
	}

	/**
	 * Shortcut for log level 1 with no newline.
	 * @param s string to log
	 */
	public static void o1n(String s) {
		o(s, 1, false);
	}

	/**
	 * Method for general log command.
	 * 
	 * @param s string to log
	 * @param cd log level
	 * @param newline add a newline to output
	 */
	public static void o(String s, int cd, boolean newline) {
		if (cd <= detail) {
			if (newline) {
				System.out.print("LOG" + cd + ":");
			}
			System.out.print(s);
			if (newline) {
				System.out.println();
			}

			if (doLogFile) {
				try {
					// create file
					FileWriter fstream = new FileWriter("out"+cd+".txt",true);
					BufferedWriter out = new BufferedWriter(fstream);
					out.write(s);
					// close the output stream
					out.close();
				} catch (Exception e) {   // catch exception if any
					System.err.println("Error: " + e.getMessage());
				}
			}

		}
	}

	/**
	 * Getter for log level.
	 * @return log level
	 */
	public static int getDetail() {
		return detail;
	}

	/**
	 * Setter for log level.
	 * @param detail
	 */
	public static void setDetail(int detail) {
		Log.detail = detail;
	}
}
