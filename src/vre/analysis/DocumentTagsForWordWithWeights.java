/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

/**
 * Contains the tags of the document assigning the corresponding weights to the words.
 *
 */
public class DocumentTagsForWordWithWeights {

	/**
	 * contains weights on line start position
	 */
	TreeMap<Integer, ArrayList<Integer>> lbMap;
	/**
	 * contains weights on char start position
	 */
	TreeMap<Integer, ArrayList<Integer>> cbMap;
	/**
	 * contains weights on line end position
	 */
	TreeMap<Integer, ArrayList<Integer>> leMap;
	/**
	 * contains weights on char end position
	 */
	TreeMap<Integer, ArrayList<Integer>> ceMap;
	/**
	 * list with weights 
	 */
	HashMap<Integer, WeightEntry> lWE;
	/**
	 * users of the document
	 */
	HashSet<Integer> dUsers;

	/**
	 * getter
	 * @return lbMap
	 */
	public TreeMap<Integer, ArrayList<Integer>> getLbMap() {
		return lbMap;
	}

	/**
	 * setter
	 * @param lbMap
	 */
	public void setLbMap(TreeMap<Integer, ArrayList<Integer>> lbMap) {
		this.lbMap = lbMap;
	}

	/**
	 * getter
	 * @return cbMap
	 */
	public TreeMap<Integer, ArrayList<Integer>> getCbMap() {
		return cbMap;
	}

	/**
	 * setter 
	 * @param cbMap
	 */
	public void setCbMap(TreeMap<Integer, ArrayList<Integer>> cbMap) {
		this.cbMap = cbMap;
	}

	/**
	 * getter
	 * @return leMap
	 */
	public TreeMap<Integer, ArrayList<Integer>> getLeMap() {
		return leMap;
	}

	/**
	 * setter
	 * @param leMap
	 */
	public void setLeMap(TreeMap<Integer, ArrayList<Integer>> leMap) {
		this.leMap = leMap;
	}

	/**
	 * getter
	 * @return ceMap
	 */
	public TreeMap<Integer, ArrayList<Integer>> getCeMap() {
		return ceMap;
	}

	/**
	 * setter
	 * @param ceMap
	 */
	public void setCeMap(TreeMap<Integer, ArrayList<Integer>> ceMap) {
		this.ceMap = ceMap;
	}

	/**
	 * getter
	 * @return lWE
	 */
	public HashMap<Integer, WeightEntry> getLWE() {
		return lWE;
	}

	/**
	 * setter
	 * @param lwe
	 */
	public void setLWE(HashMap<Integer, WeightEntry> lwe) {
		lWE = lwe;
	}

	/**
	 * constructor
	 * 
	 * @param lbMap
	 * @param cbMap
	 * @param leMap
	 * @param ceMap
	 * @param lwe
	 * @param dusers
	 */
	public DocumentTagsForWordWithWeights(TreeMap<Integer, ArrayList<Integer>> lbMap,
			TreeMap<Integer, ArrayList<Integer>> cbMap, TreeMap<Integer, ArrayList<Integer>> leMap,
			TreeMap<Integer, ArrayList<Integer>> ceMap, HashMap<Integer, WeightEntry> lwe, HashSet<Integer> dusers) {
		super();
		this.lbMap = lbMap;
		this.cbMap = cbMap;
		this.leMap = leMap;
		this.ceMap = ceMap;
		lWE = lwe;
		this.dUsers = dusers;
	}

	/**
	 * Gets the weight entries for given position.
	 * 
	 * @param lb line begin
	 * @param cb char begin
	 * @param le line end
	 * @param ce char end
	 * @return list with weights
	 */
	public ArrayList<WeightEntry> getWeightEntryKeys(int lb, int cb, int le, int ce) {
		ArrayList<WeightEntry> weList = new ArrayList<WeightEntry>();

		HashSet<Integer> weKeys = new HashSet<Integer>();
		HashSet<Integer> weleKeys = new HashSet<Integer>();
		HashSet<Integer> wecbKeys = new HashSet<Integer>();
		HashSet<Integer> weceKeys = new HashSet<Integer>();

		for (Integer we : lbMap.headMap(lb, true).keySet()) {
			weKeys.addAll(lbMap.get(we));
		}

		for (Integer we : leMap.tailMap(le, true).keySet()) {
			weleKeys.addAll(leMap.get(we));
		}

		for (Integer we : cbMap.headMap(cb, true).keySet()) {
			wecbKeys.addAll(cbMap.get(we));
		}

		for (Integer we : ceMap.tailMap(ce, true).keySet()) {
			weceKeys.addAll(ceMap.get(we));
		}

		weKeys.retainAll(weleKeys);
		weKeys.retainAll(wecbKeys);
		weKeys.retainAll(weceKeys);

		for (Integer wek : weKeys) {
			weList.add(lWE.get(wek));
		}

		return weList;
	}

	/**
	 * getter
	 * @return users of document
	 */
	public HashSet<Integer> getDUsers() {
		return dUsers;
	}

	/**
	 * setter
	 * @param users
	 */
	public void setDUsers(HashSet<Integer> users) {
		dUsers = users;
	}

}
