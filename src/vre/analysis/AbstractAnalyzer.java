/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import static vre.util.Convert.l;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;

import vre.analysis.util.DBTextpassage;

/**
 * Abstract class to define analyzer.
 *
 */
public abstract class AbstractAnalyzer {

	/**
	 * container to collect entity classes
	 */
	protected HashMap<String, HashSet<String>> RegisterTopics = new HashMap<String, HashSet<String>>();
	/**
	 * container to collect lemma IDs
	 */
	protected HashMap<String, Integer> LemmaIDs = new HashMap<String, Integer>();
	/**
	 * container to collect textpassages per document
	 */
	protected HashMap<Integer, ArrayList<DBTextpassage>> DocumentTextPassages = new HashMap<Integer, ArrayList<DBTextpassage>>();
	/**
	 * container to collect lemma per document
	 */
	protected HashMap<Integer, HashSet<Integer>> DocumentLemma = new HashMap<Integer, HashSet<Integer>>();
	/**
	 * counter for lemma IDs
	 */
	protected int lemmaIdCounter = 1; 

	/**
	 * Creates SQL string.
	 * @param con
	 * @param name
	 * @return SQL string
	 */
	public String createSQL(DatabaseConnection con, String name) {

		ArrayList<String> returnsql = new ArrayList<String>();
		int kateID = 1;
		int userID = con.getUser();

		int maxID = 1; 
		int prevIDa = 1;
		int prevID = 1;
		int curID = 0;

		int TLmaxID = 1;
		int TmaxID = 1;

		int addkateID = 1;
		int addmaxID = 1;
		int addTLmaxID = 1;
		int addTmaxID = 1;

		if (con != null) {
			addkateID = con.getNextID("categorynames");
			addmaxID = con.getNextID("categoryentries");
			addTLmaxID = con.getNextID("lnk_text_index");
			addTmaxID = con.getNextID("textpassages");

		}

		ArrayList<String> Categories = new ArrayList<String>();
		ArrayList<String> lnktextindex = new ArrayList<String>();
		ArrayList<String> Textpassages = new ArrayList<String>();

		returnsql.add("insert into categorynames values (" + (kateID - 1 + addkateID) + ",'','2',4,'NER"
				+ (kateID - 1 + addkateID) + "_" + name + "'," + userID + ",NOW(),'no','')");

		Iterator<String> registerTopicsiterator = RegisterTopics.keySet().iterator();
		while (registerTopicsiterator.hasNext()) {
			String nextTopic = registerTopicsiterator.next();
			int lemmaID = LemmaIDs.get(nextTopic);

			returnsql.add("insert into categoryentries values (" + (lemmaID - 1 + addmaxID) + "," + (kateID - 1 + addkateID)
					+ ",1,0,'j'," + (prevIDa - 1 + addmaxID) + ",'" + nextTopic.replace("'", "").replace("\\", "")
					+ "','" + nextTopic.replace("'", "").replace("\\", "") + "'," + userID
					+ ",' ',' ',' ','n','background:yellow',NOW())");

			ArrayList<String> CategoryEntryA = new ArrayList<String>();
			CategoryEntryA.add(String.valueOf(lemmaID));
			CategoryEntryA.add("1");
			CategoryEntryA.add("0");
			CategoryEntryA.add("j");
			CategoryEntryA.add(String.valueOf(prevIDa));
			CategoryEntryA.add(nextTopic.replace("'", "").replace("\\", ""));
			Categories.add(l(CategoryEntryA));

			prevIDa++;
			curID = lemmaID;
			maxID++;

			Iterator<String> registerTopicNamedEntitiesiterator = RegisterTopics.get(nextTopic).iterator();
			while (registerTopicNamedEntitiesiterator.hasNext()) {
				String nextTopicNamedEntity = registerTopicNamedEntitiesiterator.next();
				lemmaID = LemmaIDs.get(nextTopicNamedEntity + " # " + nextTopic);
				String lemmaText = nextTopicNamedEntity.replace("'", "").replace("\\", "");
				if (lemmaText.length() > 245)
					lemmaText = lemmaText.substring(0, 245);
				returnsql.add(
						"insert into categoryentries values (" + (lemmaID - 1 + addmaxID) + "," + (kateID - 1 + addkateID)
								+ ",2," + (curID - 1 + addmaxID) + ",'n'," + (prevID - 1 + addmaxID) + ",'" + lemmaText
								+ "','" + lemmaText + "'," + userID + ",' ',' ',' ','n','background:yellow',NOW())");

				ArrayList<String> CategoryEntry = new ArrayList<String>();
				CategoryEntry.add(String.valueOf(lemmaID));
				CategoryEntry.add("2");
				CategoryEntry.add(String.valueOf(curID));
				CategoryEntry.add("n");
				CategoryEntry.add(String.valueOf(prevID));
				CategoryEntry.add(nextTopicNamedEntity.replace("'", "").replace("\\", ""));
				Categories.add(l(CategoryEntry));
				prevID++;
				maxID++;

			}

			prevID = 1;

		}

		Iterator<Integer> Documentiterator = DocumentLemma.keySet().iterator();
		while (Documentiterator.hasNext()) {

			HashMap<Integer, Integer> lnk = new HashMap<Integer, Integer>();

			Integer nextDocument = Documentiterator.next();
			HashSet<Integer> DocumentLemmaIDs = DocumentLemma.get(nextDocument);
			Iterator<Integer> DocumentLemmaIDsiterator = DocumentLemmaIDs.iterator();
			while (DocumentLemmaIDsiterator.hasNext()) {
				Integer nextDocumentLemmaID = DocumentLemmaIDsiterator.next();

				returnsql.add("insert into lnk_text_index values (" + (TLmaxID - 1 + addTLmaxID) + ", "
						+ (nextDocumentLemmaID - 1 + addmaxID) + ", " + (kateID - 1 + addkateID) + ", " + nextDocument
						+ ", 0, " + userID + ", 'background:yellow', NOW())");

				ArrayList<String> lnktextindexEntry = new ArrayList<String>();
				lnktextindexEntry.add(String.valueOf(TLmaxID));
				lnktextindexEntry.add(String.valueOf(nextDocumentLemmaID));
				lnktextindexEntry.add(String.valueOf(nextDocument));
				lnktextindex.add(l(lnktextindexEntry));

				lnk.put(nextDocumentLemmaID, TLmaxID);
				TLmaxID++;

			}

			ArrayList<DBTextpassage> TextPassages = DocumentTextPassages.get(nextDocument);
			Iterator<DBTextpassage> TextPassagesiterator = TextPassages.iterator();
			while (TextPassagesiterator.hasNext()) {
				DBTextpassage nextTextPassage = TextPassagesiterator.next();

				returnsql.add(
						"insert into textpassages (ID,lnkID,pos_line_begin,pos_char_begin,pos_line_end,pos_char_end,textpassage,comment) values ("
								+ (TmaxID - 1 + addTmaxID) + ", "
								+ (lnk.get(LemmaIDs.get(nextTextPassage.getName())) - 1 + addTLmaxID) + ", "
								+ nextTextPassage.getStart()[0] + ", " + nextTextPassage.getStart()[1] + ", "
								+ nextTextPassage.getEnd()[0] + ", " + nextTextPassage.getEnd()[1] + ", '"
								+ nextTextPassage.getName().replace("'", "").replace("\\", "") + "', ' ')");

				ArrayList<String> TextpassagesEntry = new ArrayList<String>();
				TextpassagesEntry.add(String.valueOf(TmaxID));
				TextpassagesEntry.add(String.valueOf(lnk.get(LemmaIDs.get(nextTextPassage.getName()))));
				TextpassagesEntry.add(String.valueOf(nextTextPassage.getStart()[0]));
				TextpassagesEntry.add(String.valueOf(nextTextPassage.getStart()[1]));
				TextpassagesEntry.add(String.valueOf(nextTextPassage.getEnd()[0]));
				TextpassagesEntry.add(String.valueOf(nextTextPassage.getEnd()[1]));
				TextpassagesEntry.add(nextTextPassage.getName().replace("'", "").replace("\\", ""));

				returnsql.add("DELETE FROM tp_weight WHERE tID =" + (TmaxID - 1 + addTmaxID));

				String lweight = "";
				ArrayList<String> TextpassagesWeightEntry = new ArrayList<String>();

				if (nextTextPassage.getWeight() != null) {

					returnsql.add("INSERT INTO tp_weight VALUES (" + (TmaxID - 1 + addTmaxID) + ", " + userID + ", '"
							+ nextTextPassage.getWeight() + "')");

					lweight = nextTextPassage.getWeight();
				}

				for (Entry<Long, String> owe : nextTextPassage.otherweights.entrySet()) {

					returnsql.add("INSERT INTO tp_weight VALUES (" + (TmaxID - 1 + addTmaxID) + ", " + owe.getKey()
							+ ", '" + owe.getValue() + "')");

					TextpassagesWeightEntry.add(owe.getKey().toString());
					TextpassagesWeightEntry.add(owe.getValue());

				}

				if (!TextpassagesWeightEntry.isEmpty()) {
					lweight = l(TextpassagesWeightEntry);
				}

				TextpassagesEntry.add(lweight);

				Textpassages.add(l(TextpassagesEntry));

				TmaxID++;

			}

		}

		ArrayList<String> returnList = new ArrayList<String>();
		returnList.add(l(Categories));
		returnList.add(l(lnktextindex));
		returnList.add(l(Textpassages));

		String returnString = "";
		if (con == null) {
			returnString = l(returnList);
		} else {
			StringBuilder returnStringsql = new StringBuilder();
			for (String singlesql : returnsql) {
				returnStringsql.append(singlesql + ";;;");
			}

			returnString = returnStringsql.toString();
		}

		return returnString;

	}

}
