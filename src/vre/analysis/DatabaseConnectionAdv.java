/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;

import vre.util.Log;

/**
 * Advanced methods for the database connection.
 *
 */
public class DatabaseConnectionAdv extends DatabaseConnection {
	
	/**
	 * constructor
	 */
	public DatabaseConnectionAdv() {
		super();
	}

	/* (non-Javadoc)
	 * @see vre.analysis.DatabaseConnection#getTagsForWord(int, int, int, int, int, int)
	 */
	public HashSet<String> getTagsForWord(int cID, int dID, int sentence, int startOfToken, int endOfToken, int uID) {
		HashSet<String> Tags = new HashSet<String>();

		PreparedStatement ps;

		sentence++; // tk text widget counting

		try {

			String sql = "SELECT content FROM (categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID  WHERE k.indexID = ? AND l.documentID = ? AND t.pos_line_begin <= ? AND t.pos_line_end >= ? AND t.pos_char_begin <= ? AND t.pos_char_end >= ?";
			String sqlRestrict = getRestrictionsSQLString(uID, "cu");
			sql += sqlRestrict;

			ps = con.prepareStatement(sql);

			ps.setInt(1, cID);
			ps.setInt(2, dID);
			ps.setInt(3, sentence);
			ps.setInt(4, sentence);
			ps.setInt(5, startOfToken);
			ps.setInt(6, endOfToken);

			Log.o2("SELECT content FROM (categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID  WHERE k.indexID = "
					+ cID + " AND l.documentID = " + dID + " AND t.pos_line_begin <= " + sentence
					+ " AND t.pos_line_end >= " + sentence + " AND t.pos_char_begin <= " + startOfToken
					+ " AND t.pos_char_end >= " + endOfToken + " " + sqlRestrict);

			ResultSet rs = ps.executeQuery(); // LIMIT 0,1

			if (rs.next()) {
				String name = rs.getString(1);
				Log.o2("name " + name);
				Tags.add("PERS");

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return Tags;

	}

	
	
	/**
	 * Gets taggings (with score) for a word.
	 * 
	 * @param cID category ID
	 * @param dID document ID
	 * @param sentence sentence ID
	 * @param startOfToken start position
	 * @param endOfToken end position
	 * @param uID user ID
	 * @return hash map
	 */
	public HashMap<String, HashMap<Integer, String>> getTagsForWordWithWeights(int cID, int dID, int sentence,
			int startOfToken, int endOfToken, int uID) { 

		HashMap<String, HashMap<Integer, String>> TagsWithWeights = new HashMap<String, HashMap<Integer, String>>();

		PreparedStatement ps;

		sentence++; // tk text widget counting

		try {

			String sql = "SELECT content, tw.uID, tw.val, l.ownerID FROM (categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID LEFT JOIN tp_weight tw ON t.ID = tw.tID WHERE k.indexID = ? AND l.documentID = ? AND t.pos_line_begin <= ? AND t.pos_line_end >= ? AND t.pos_char_begin <= ? AND t.pos_char_end >= ? ";
			String sqlRestrict = getRestrictionsSQLString(uID, "cu");
			sql += sqlRestrict;
			sql += " ORDER BY content";

			ps = con.prepareStatement(sql);

			ps.setInt(1, cID);
			ps.setInt(2, dID);
			ps.setInt(3, sentence);
			ps.setInt(4, sentence);
			ps.setInt(5, startOfToken);
			ps.setInt(6, endOfToken);

			Log.o2("SELECT content, tw.uID, tw.val, l.ownerID FROM (categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID LEFT JOIN tp_weight tw ON t.ID = tw.tID WHERE k.indexID = "
					+ cID + " AND l.documentID = " + dID + " AND t.pos_line_begin <= " + sentence
					+ " AND t.pos_line_end >= " + sentence + " AND t.pos_char_begin <= " + startOfToken
					+ " AND t.pos_char_end >= " + endOfToken + " " + sqlRestrict + " ORDER BY content");

			ResultSet rs = ps.executeQuery(); // LIMIT 0,1

			String lastname = "";
			boolean firstname = true;
			HashMap<Integer, String> aggList = new HashMap<Integer, String>();

			while (rs.next()) {
				String name = rs.getString(1);
				Log.o2("name " + name);
				name = "PERS";
				int userint = rs.getInt(4); 
				String agg = "0.8"; 
				if (rs.getInt(2) != 0) { 											
					userint = rs.getInt(2);
					agg = rs.getString(3);
				}
				aggList.put(userint, agg);

				if (!lastname.equals(name) || firstname) {
					firstname = false;
					TagsWithWeights.put(name, aggList);
					aggList = new HashMap<Integer, String>();
				}

			}
			ps.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return TagsWithWeights;

	}

}
