/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import static vre.util.Convert.getDoubleFromString;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import com.aliasi.util.Strings;

import vre.analysis.util.DBTextpassage;
import vre.util.Log;

/**
 * Final step in Co-Forest (Li, Ming und Zhou, Zhi-Hua (2007): 
 *  Improve Computer-Aided Diagnosis With Machine Learning Techniques Using Undiagnosed Samples.
 *  In: Trans. Sys. Man Cyber. Part A 37.6, S. 1088–1098. issn: 1083-
 *  4427. doi: 10.1109/TSMCA.2007.904745) used model on text.
 *
 */
public class CoForestOutputArgMaxNER extends AbstractAnalyzer {

	/**
	 * are tokens separated by whitespace or already array
	 */
	private static boolean tokeninzewhitespace = true;
	/**
	 * database connection
	 */
	protected DatabaseConnection mycon;
	/**
	 * minimum confidence
	 */
	protected double minConf = 0.25;
	/**
	 * ID of category
	 */
	protected int cID;
	/**
	 *  hash set of views
	 */
	protected HashSet<Integer> views;
	/**
	 *  BIO scheme out tag
	 */
	protected final String mOutTag = "O";
	/**
	 * maximum number of users
	 */
	protected int maxuser;
	/**
	 * user ID
	 */
	protected int uID;
	/**
	 * partition of the data (for evaluation runs)
	 */
	protected int partition;
	/**
	 * size of the training set (for evaluation runs)
	 */
	protected double ts;
	/**
	 * is it a development or test run (for evaluation runs)
	 */
	protected String developmentortest;
	/**
	 * level of the index, where the entity names are saved
	 */
	protected int level;

	/**
	 * type of user (for evaluation runs)
	 */
	protected String usertype;

	/**
	 * tagging (with score) of the documents 
	 */
	protected HashMap<Integer, DocumentTagsForWordWithWeights> dList;

	/**
	 * constructor
	 * 
	 * @param cID
	 * @param views
	 * @param uID
	 * @param mycon
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param minConf
	 */
	public CoForestOutputArgMaxNER(int cID, HashSet<Integer> views, int uID, DatabaseConnection mycon, int partition,
			double ts, String developmentortest, int level, String usertype, double minConf) {
		this.cID = cID;
		this.views = views;
		this.uID = uID;
		this.mycon = mycon;
		this.partition = partition;
		this.ts = ts;
		this.level = level;
		this.usertype = usertype;
		this.developmentortest = developmentortest;
		this.minConf = minConf;

	}

	/**
	 * Prepare tagging (with score) of the documents. 
	 * 
	 * @param docs Documents
	 */
	public void prepare(HashMap<Integer, String> docs) {
		// HashMap<Integer, String> docs =
		// mycon.getDocumentsbymIDcID(0,0,partition,ts,false,useDev,useTest,"cu",uID);
		ArrayList<Integer> dIDList = new ArrayList<Integer>();
		for (Integer k : docs.keySet()) {
			dIDList.add(k);
		}
		ArrayList<Integer> cIDList = new ArrayList<Integer>();
		for (int l = cID; l >= cID - views.size(); l--) {
			cIDList.add(l);
		}

		dList = mycon.getDocumentTagsForWordWithWeights(cIDList, dIDList, uID, level, "cu", "");

		int sumdocs = docs.size();
	}

	/**
	 * Runs one classification.
	 * 
	 * @param docId ID of the document
	 * @param s text string
	 * @return classification result
	 */
	public String classify(int docId, String s) {

		String returnvalue = "";

		String text = s;

		char[] cs = text.toCharArray();
		if (!dList.containsKey(docId))
			return returnvalue;
		maxuser = dList.get(docId).getDUsers().size();

		// for classification
		HashSet<Integer> currentLemmaIDs = new HashSet<Integer>();
		ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();

		DocumentTagsForWordWithWeights d = dList.get(docId);
		Log.o2n(".");

		// start of processSentence

		String in = s;
		// part 1
		String[] sentences = in.split("\n");
		int silensum = 0;
		for (int si = 0; si < sentences.length; ++si) {
			if (Strings.allWhitespace(sentences[si]))
				continue;
			String sentence = sentences[si];
			String[] tagTokens = sentence.split(" ");
			String[] tokens = new String[tagTokens.length];
			String[] tags = new String[tagTokens.length];
			String[] scores = new String[tagTokens.length];
			Log.o2(" in:: " + sentence);

			// for classification 
			int[] tokensstart = new int[tagTokens.length];
			int[] tokensend = new int[tagTokens.length];

			int startOfToken = 0;

			// create tagging directly from Database TODO: separate Database access, therefore
			// the step of creating an appropriate string representation is kept
			for (int i = 0; i < tagTokens.length; ++i) {
				Log.o2(tagTokens[i]);
				tokens[i] = tagTokens[i];

				int endOfToken = sentence.indexOf(32, startOfToken);

				if (endOfToken == -1) {
					endOfToken = sentence.length();
				}

				// System.out.println("Token: " + tokens[i]);

				// reduce queries idea: one query per document here, rewrite to read directly from memory

				// +1 tk widget counting

				ArrayList<WeightEntry> weList = d.getWeightEntryKeys(si + 1, startOfToken, si + 1, endOfToken);

				String tag = mOutTag;
				Double score = 1.0;
				int scorecount = 0;

				HashMap<String, Double> scoreweights = new HashMap<String, Double>();

				// SubsamlingAndConfidence
				// must be subsampled so that W_i,t is less than (e_i,t-1 W_i,t-1) / e_i,t

				if (weList.size() > 0) {
					Log.o3("size: " + weList.size());
				}
				// SubsamplingAndConfidence
				// no score added, only test to reduce running time, better sort of break;
					// SubsamplingAndConfidence End

				for (WeightEntry we : weList) {
					String ctag = we.getName();
					String cweight = we.getWeight();
					double cscore = getDoubleFromString(cweight);
					Log.o2("cTAG: " + ctag + ":" + cscore);

					if (scoreweights.containsKey(ctag)) {
						double ascore = scoreweights.get(ctag);
						cscore += ascore;
					}
					scoreweights.put(ctag, cscore);
					if (cscore > score || tag.equals("O")) {
						tag = ctag;
						score = cscore;
					}
					scorecount++;

				}

				// if no weight information is given, a standard weight is used
				if (scorecount == 0) {
					score = 1.0;
				} else {
					score = score / (double) maxuser;
				}

				
				// SubsamplingAndConfidence (removed score adding part, like stated above)
				/*
				 * if (false) { tag = mOutTag; score = 1.0; break; }
				 * 
				 * // next is confidence could be simple score weights only with
				 * subsampled tags
				 * 
				 * double confidence = score; if (false) { tag = mOutTag; score = 1.0; break; }
				 */
				// adding to W
				// WSubit += confidence;

				// SubsamplingAndConfidence End

				Log.o2("TAG: " + tag + ":" + score);


				tags[i] = tag;
				scores[i] = score.toString();

				tokensstart[i] = startOfToken;
				tokensend[i] = endOfToken;

				startOfToken += tagTokens[i].length() + 1;

			}

			// end of part processSentence

			// part of classification
			for (int i = 0; i < tokens.length; i++) {
				String type = tags[i];
				// double conf = Math.pow(2.0,chunk.score());
				String score = scores[i];
				int start = tokensstart[i];
				int end = tokensend[i];
				String phrase = tokens[i];

				if (type.equals(mOutTag))
					continue;

				DecimalFormat df = new DecimalFormat("0.0000");
				Log.o2(i + " " + score // Strings.decimalFormat(conf,"0.0000",12)
						+ "       (" + start + ", " + end + ")       " + type + "         " + phrase);

				// TODO use variable to represent parameter test on different occurences of minconf
				if (Double.valueOf(score) < minConf) continue; // 1 3 5
				// if (Double.valueOf(score) < 0.1) continue; // 4
				// NONE 2
				Log.o1(">minconf");

				// token split
				String[] phraseT;
				if (tokeninzewhitespace) {
					phraseT = phrase.split(" ");
				} else {
					phraseT = new String[1];
					phraseT[0] = phrase;
				}
				int mid = 0;
				for (String phraseTs : phraseT) {
					int len = phraseTs.length();

					DBTextpassage textpassage = new DBTextpassage();
					textpassage.setName(phraseTs + " # " + type);
					// int csstart = silensum;
					// textpassage.setStart(getPosFromNumber(start+mid+csstart,s));
					// textpassage.setEnd(getPosFromNumber(start+mid+len+csstart,s));
					int[] lineColumnStart = new int[3];
					lineColumnStart[0] = si + 1; // tk line starts with 1 !
					lineColumnStart[1] = start + mid;
					lineColumnStart[2] = silensum + start + mid;
					int[] lineColumnEnd = new int[3];
					lineColumnEnd[0] = si + 1; // tk line starts with 1 !
					lineColumnEnd[1] = start + mid + len;
					lineColumnEnd[2] = silensum + start + mid + len;
					textpassage.setStart(lineColumnStart);
					textpassage.setEnd(lineColumnEnd);
					textpassage.setWeight(score);
					TextPassages.add(textpassage);

					HashSet<String> RegisterTopic = RegisterTopics.get(type);
					if (RegisterTopic == null)
						RegisterTopic = new HashSet<String>();
					RegisterTopic.add(phraseTs);

					RegisterTopics.put(type, RegisterTopic);

					if (LemmaIDs.get(type) == null)
						LemmaIDs.put(type, lemmaIdCounter++);

					String lemmaString = phraseTs + " # " + type;
					int currentLemmaID;
					if (LemmaIDs.get(lemmaString) == null) {
						currentLemmaID = lemmaIdCounter++;
						LemmaIDs.put(lemmaString, currentLemmaID);
					}
					currentLemmaID = LemmaIDs.get(lemmaString);
					currentLemmaIDs.add(currentLemmaID);

					mid += len;
					// whitespace
					mid++;
				}

			}

			int silen = sentence.length();
			silensum += silen;

		}

		DocumentLemma.put(docId, currentLemmaIDs);
		DocumentTextPassages.put(docId, TextPassages);
		return returnvalue;
	}

}
