/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import vre.analysis.util.Cfunit;
import vre.util.Configuration;
import vre.util.Log;

/**
 * Database Connection. This program is designed to run with a variant of the
 * Database of the virtual research environment: Research Network and
 * Database system for the Collaborative Research Center CRC 600 Strangers and
 * Poor People by the Trier Center for Digital Humanities, Research Center
 * Europe, Service Center eSciences at Trier University.
 */
public class DatabaseConnection {

	/**
	 * connection
	 */
	protected Connection con;
	/**
	 * user ID
	 */
	public int user = 4;
	/**
	 * name of process database
	 */
	public String processdb = "";
	/**
	 * name of gold database
	 */
	public String golddb = "";

	/**
	 * list containing user groups
	 */
	public static HashMap<Integer, HashSet<Integer>> guList = new HashMap<Integer, HashSet<Integer>>();

	/**
	 * constructor
	 */
	public DatabaseConnection() {
		
		user = new Integer(Configuration.getConfigXMLvalue("accountid"));

		// JDBC driver for mariadb.
		try {
			//Class.forName("com.mariadb.jdbc.Driver");

			String url = ""; // "jdbc:mariadb://localhost:3306/vre_database";

			// Configuration.setConfigXml("config.xml");
			String srv = Configuration.getConfigXMLvalue("srvconn");
			processdb = Configuration.getConfigXMLvalue("database");

			try {
				golddb = Configuration.getConfigXMLvalue("golddatabase");
			} catch (Exception e) {
				Log.o1("No seperate gold found");
			}

			url = srv + processdb;
			Log.o1("DATABASE:" + url);

			// con = DriverManager.getConnection(url, "vre", "change");
			String dbuser = "vre";
			String pwd = "change";
			dbuser = Configuration.getConfigXMLvalue("user");
			Log.o1("USER:" + dbuser);
			pwd = Configuration.getConfigXMLvalue("password");
			url += "?autoReconnect=true";
			con = DriverManager.getConnection(url, dbuser, pwd);
		} catch (SQLException e) {
			
			e.printStackTrace();
		} 

	}
	
	
	/**
	 * Gets Documents by folder ID and list of category IDs.
	 * (set mID = cID = 0 for all documents)
	 * 
	 * @param mID folder ID in the VRE
	 * @param cID category ID
	 * @param partition partition of the data (for evaluation runs)
	 * @param ts size of the training set (for evaluation runs)
	 * @param setname training set, development or test (for evaluation runs)
	 * @param usertype type of user (for evaluation runs)
	 * @param uID user ID 
	 * @return hash map 
	 */
	public HashMap<Integer, String> getDocumentsbymIDcID(Integer mID, Integer cID, int partition, double ts,
		String setname, String usertype, long uID) {
		ArrayList<Integer> cIDList = new ArrayList<Integer>();
		cIDList.add(cID);
		return getDocumentsbymIDcIDs(mID, cIDList, partition, ts, setname, usertype, uID);
	}

	
	/**
	 * Gets Documents by folder ID and list of category IDs.
	 * 
	 * @param mID folder ID in the VRE
	 * @param cIDList list of category IDs
	 * @param partition partition of the data (for evaluation runs)
	 * @param ts size of the training set (for evaluation runs)
	 * @param setname training set, development or test (for evaluation runs)
	 * @param usertype type of user (for evaluation runs)
	 * @param uID user ID 
	 * @return hash map 
	 */
	public HashMap<Integer, String> getDocumentsbymIDcIDs(Integer mID, ArrayList<Integer> cIDList, int partition,
			double ts, String setname, String usertype, long uID) {

		HashMap<Integer, String> Documents = new HashMap<Integer, String>();

		Statement s;

		try {
			s = con.createStatement();

			String sql = "SELECT DISTINCT dv.ID AS dID, fulltext FROM doc_fulltext dv ";
			String sqlW = "";

			if (mID > 0) {
				sql += " INNER JOIN doc_folderContent dm ON dv.ID = dm.cntID ";
				sqlW += " dm.folderID=" + mID;
			}

			String cIDs = StringUtils.join(cIDList, ",");

			if (cIDList.size() > 0 && !setname.equals("compare")) {

				sql += " INNER JOIN lnk_text_index lt ON dv.ID= lt.documentID ";
				if (!sqlW.equals("")) {
					sqlW += " AND ";
				}
				sqlW += " lt.category IN (" + cIDs + ")";
			}

			if (uID > 0) {
				// to deselect unannotated documents
				if (usertype.equals("wu")) {
					sql += " INNER JOIN textpassages t ON lt.ID = t.lnkID LEFT JOIN tp_weight tw ON t.ID = tw.tID ";
					if (!sqlW.equals("")) {
						sqlW += " AND ";
					}
					sqlW += " tw.uID =" + uID;

				}
				if (usertype.equals("wul") || usertype.equals("wulr") || usertype.equals("wug")
						|| usertype.equals("wugr")) {
					// selected by category user replacement for all documents
				}
				if (usertype.equals("cu")) {
					// allready selected by category id (no further user restriction,
				}
				if (usertype.equals("fu")) {
					if (!sqlW.equals("")) {
						sqlW += " AND ";
					}
					sqlW += " lt.ownerID =" + uID;
				}

			}

			if (!sqlW.equals("")) {
				sql += "WHERE " + sqlW;
			}

			sql += " ORDER BY dv.ID";

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1
			int sumdocs = 0;
			while (rs.next()) {
				sumdocs++;
			}

			rs.beforeFirst();

			int dCnt = 0;
			Log.o3(setname+" ");
			while (rs.next()) {
				int id = rs.getInt(1);

				dCnt++;

				boolean inSet = inSet(partition, dCnt, sumdocs, ts, setname);
				if (!inSet)
					continue;

				String text = rs.getString(2);

				text = getTextfromDBstring(text);
				Log.o2n(id + " ");
				Documents.put(id, text);

			}
			Log.o2("");

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return Documents;

	}

	/**
	 * Checks if current document is in set.
	 * 
	 * @param partition partition of the data (for evaluation runs)
	 * @param dCnt document count
	 * @param sumdocs document sum
	 * @param ts size of the training set (for evaluation runs)
	 * @param setname training set, development or test (for evaluation runs)
	 * @return boolean, if document is in set
	 */
	public static boolean inSet(int partition, int dCnt, int sumdocs, double ts, String setname) {
		double dPart = (double) dCnt / (double) sumdocs;
		boolean inPartition = false;
		boolean inTest = false;
		double dPartTestStart = 1.0 - ts;
		// End is 1.0
		double dPartPartStart = dPartTestStart * ((double) partition / 10.0);
		double dPartPartEnd = dPartTestStart * ((double) (partition + 1) / 10.0);

		// last (ts * 100) % are test
		// remaining are divided in 10 % partitions

		if (ts > 0.0) {
			if (dPart >= dPartTestStart) {
				inTest = true;
			}
		}

		if (partition > -1) {
			if (!(dPart < dPartPartStart || dPart > dPartPartEnd)) {
				inPartition = true;
			}
		}

		boolean rboolean = false;

		if (setname.equals("training")) {
			rboolean = !(inPartition || inTest);
		}
		if (setname.equals("development")) {
			rboolean = rboolean || inPartition;
		}
		if (setname.equals("test")) {
			rboolean = rboolean || inTest;
		}
		if (setname.equals("compare") || setname.equals("comparetraining") || setname.equals("compareestimator")) {
			rboolean = !inPartition;
		}

		return rboolean;

	}

	/**
	 * Gets document text.
	 * 
	 * @param docID document ID
	 * @return text
	 */
	public String getDocumentTextbydocID(Integer docID) {

		Statement s;
		String text = "";

		try {
			s = con.createStatement();

			String sql = "SELECT fulltext FROM doc_fulltext WHERE ID = " + docID;

			ResultSet rs = s.executeQuery(sql);

			while (rs.next()) {
				text = rs.getString(1);
			}

			text = getTextfromDBstring(text);

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return text;

	}

	/**
	 * Builds a dictionary by given category.
	 * 
	 * @param cID category ID
	 * @return hash map of string dictionary entries
	 */
	public HashMap<String,String> getCategoriesbyCID(Integer cID) {

		HashMap<String,String> toFind = new HashMap<String,String>();

		try {
			Statement s;

			s = con.createStatement();

			ResultSet rs1 = s.executeQuery("SELECT content FROM categoryentries k WHERE IndexID = " + cID); // LIMIT 0,1

			String name1 = null;
			while (rs1.next()) {
				name1 = rs1.getString(1);
			}

			ResultSet rs2 = s.executeQuery(
					"SELECT textpassage FROM categoryentries k inner JOIN lnk_text_index l ON k.ID = lemmaID INNER JOIN textpassages t ON l.ID = t.lnkID WHERE  textpassage != '' AND length(textpassage) > 2 and indexID ="
							+ cID);

			String name2 = null;
			while (rs2.next()) {
				name2 = rs2.getString(1);
			}
			
			if (name1 != null && name2 != null) {
				toFind.put(name1,name2);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return toFind;

	}

	/**
	 * Count users per document.
	 * 
	 * @param cID category ID
	 * @param dID document ID
	 * @param uID user ID
	 * @param usertype type of user (for evaluation runs)
	 * @return count users per document
	 */
	public int getCountUserforDocID(int dID, int cID, int uID, String usertype) {

		if (usertype.equals("cu")) {
			return 1;
		}

		int countuser = 0;

		PreparedStatement ps;

		String sql = "";

		try {
			String sqla = "";

			if (usertype.equals("wu") || usertype.equals("wul") || usertype.equals("wulr")) {
				sqla = "SELECT count(*) FROM (SELECT DISTINCT tw.uID ";

			}
			if (usertype.equals("fu")) {
				sqla = "SELECT count(*) FROM (SELECT DISTINCT uID ";
			}

			sql += sqla
					+ " FROM (lnk_text_index l INNER JOIN textpassages t ON l.ID = t.lnkID) INNER JOIN tp_weight tw ON t.ID = tw.tID WHERE l.documentID = ? AND l.category = ? ";

			String sqlRestrict = getRestrictionsSQLString(uID, usertype);
			sql += sqlRestrict + " ) ctbl";

			ps = con.prepareStatement(sql);

			ps.setInt(1, dID);
			ps.setInt(2, cID);

			Log.o2(sqla
					+ " FROM (lnk_text_index l INNER JOIN textpassages t ON l.ID = t.lnkID) INNER JOIN tp_weight tw ON t.ID = tw.tID WHERE  l.documentID = "
					+ dID + "  AND  l.category = " + cID + " " + sqlRestrict + ") ctbl");

			ResultSet rs = ps.executeQuery(); // LIMIT 0,1

			while (rs.next()) {
				countuser = rs.getInt(1);

			}

		} catch (SQLException e) {
			
			Log.o1("SQL" + sql);
			e.printStackTrace();
		}

		return countuser;

	}
	
	/**
	 * Gets taggings for a word.
	 * 
	 * @param cID category ID
	 * @param dID document ID
	 * @param sentence sentence ID
	 * @param startOfToken start position
	 * @param endOfToken end position
	 * @param uID user ID
	 * @return hash set
	 */
	public HashSet<String> getTagsForWord(int cID, int dID, int sentence, int startOfToken, int endOfToken, int uID) { 
		
		HashSet<String> Tags = new HashSet<String>();

		PreparedStatement ps;

		sentence++; // tk text widget counting

		try {

			String sql = "SELECT content FROM (categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID  WHERE k.indexID = ? AND l.documentID = ? AND t.pos_line_begin <= ? AND t.pos_line_end >= ? AND t.pos_char_begin <= ? AND t.pos_char_end >= ?";
			String sqlRestrict = getRestrictionsSQLString(uID, "cu");
			sql += sqlRestrict;

			ps = con.prepareStatement(sql);

			ps.setInt(1, cID);
			ps.setInt(2, dID);
			ps.setInt(3, sentence);
			ps.setInt(4, sentence);
			ps.setInt(5, startOfToken);
			ps.setInt(6, endOfToken);

			Log.o2("SELECT content FROM (categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID  WHERE k.indexID = "
					+ cID + " AND l.documentID = " + dID + " AND t.pos_line_begin <= " + sentence
					+ " AND t.pos_line_end >= " + sentence + " AND t.pos_char_begin <= " + startOfToken
					+ " AND t.pos_char_end >= " + endOfToken + " " + sqlRestrict);

			ResultSet rs = ps.executeQuery(); // LIMIT 0,1

			while (rs.next()) {
				String name = rs.getString(1);
				Log.o2("name " + name);
				Tags.add(name);

			}

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return Tags;

	}

	/**
	 * Gets taggings (with score) for a word.
	 * 
	 * @param cID category ID
	 * @param dID document ID
	 * @param sentence sentence ID
	 * @param startOfToken start position
	 * @param endOfToken end position
	 * @param uID user ID
	 * @param level level of the index, where the entity names are saved
	 * @return hash map
	 */
	public HashMap<String, HashMap<Integer, String>> getTagsForWordWithWeights(int cID, int dID, int sentence,
			int startOfToken, int endOfToken, int uID, int level) { 

		HashMap<String, HashMap<Integer, String>> TagsWithWeights = new HashMap<String, HashMap<Integer, String>>();

		PreparedStatement ps;

		sentence++; // tk text widget counting

		try {
			String rsql = "";
			String ksql = Math.abs((level)) + "";
			if (level != 0) {
				if (level < 0) {
					int llevel = level;
					while (llevel != 0) {
						ksql = Math.abs((llevel)) + "";
						String kbefore = Math.abs((llevel + 1)) + "";

						rsql += " INNER JOIN categoryentries k" + ksql + " ON k" + ksql + ".ID = k" + kbefore + ".parentID ";

						llevel++;
					}
				}
			}

			String sql = "SELECT k" + ksql
					+ ".content, tw.uID, tw.val, l.ownerID, k0.parentID, k0.content FROM (categoryentries k0 " + rsql
					+ " INNER JOIN lnk_text_index l ON k0.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID LEFT JOIN tp_weight tw ON t.ID = tw.tID WHERE k0.indexID = ? AND l.documentID = ? ";

			if (sentence > -1) {
				sql += " AND t.pos_line_begin <= ? AND t.pos_line_end >= ? ";
			}
			if (startOfToken > -1) {
				sql += " AND t.pos_char_begin <= ? AND t.pos_char_end >= ? ";
			}

			String sqlRestrict = getRestrictionsSQLString(uID, "cu");
			sql += sqlRestrict;
			sql += " ORDER BY k" + ksql + ".content";

			ps = con.prepareStatement(sql);

			ps.setInt(1, cID);
			ps.setInt(2, dID);
			if (sentence > -1) {
				ps.setInt(3, sentence);
				ps.setInt(4, sentence);
			}
			if (startOfToken > -1) {
				ps.setInt(5, startOfToken);
				ps.setInt(6, endOfToken);
			}

			// Log.o2(sql);

			Log.o2("SELECT k" + ksql
					+ ".content, tw.uID, tw.val, l.ownerID, k0.parentID, k0.content FROM (categoryentries k0 " + rsql
					+ "  INNER JOIN lnk_text_index l ON k0.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID LEFT JOIN tp_weight tw ON t.ID = tw.tID WHERE k0.indexID = "
					+ cID + " AND l.documentID = " + dID + " AND t.pos_line_begin <= " + sentence
					+ " AND t.pos_line_end >= " + sentence + " AND t.pos_char_begin <= " + startOfToken
					+ " AND t.pos_char_end >= " + endOfToken + " " + sqlRestrict + " ORDER BY k" + ksql + ".content");

			ResultSet rs = ps.executeQuery(); // LIMIT 0,1

			// implement scoring function here
			String lastname = "";
			HashMap<Integer, String> aggList = new HashMap<Integer, String>();

			while (rs.next()) {
				String name = rs.getString(1);
				String content = rs.getString(6);

				int userint = rs.getInt(4); // if no weight is given use entry from ownerID
				String agg = "0.8"; // standard weight
				if (rs.getInt(2) != 0) { // the column value, if the value is SQL NULL, the value returned is 0
											// http://docs.oracle.com/javase/6/docs/api/java/sql/ResultSet.html#getInt%28int%29
					userint = rs.getInt(2);
					agg = rs.getString(3);
				}
				Log.o2("name " + name + " user " + userint + " w:" + agg + " content " + content);
				aggList.put(userint, agg);

				if (!lastname.equals(name) && !lastname.isEmpty()) {

					TagsWithWeights.put(name, aggList);
					aggList = new HashMap<Integer, String>();
				}
				lastname = name;

			}
			if (!lastname.isEmpty()) {
				TagsWithWeights.put(lastname, aggList);
			}

			ps.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return TagsWithWeights;

	}

	/**
	 *  Gets taggings (with score) per document.
	 * 
	 * @param cID category ID
	 * @param dIDList list of document IDs
	 * @param uID user ID 
	 * @param level level of the index, where the entity names are saved
	 * @param usertype type of user (for evaluation runs)
	 * @param otherdb database to check with gold values 
	 * @return hash map
	 */
	public HashMap<Integer, DocumentTagsForWordWithWeights> getDocumentTagsForWordWithWeights(int cID,
		ArrayList<Integer> dIDList, int uID, int level, String usertype, String otherdb) {
		ArrayList<Integer> cIDList = new ArrayList<Integer>();
		cIDList.add(cID);
		return getDocumentTagsForWordWithWeights(cIDList, dIDList, uID, level, usertype, otherdb);
	}

	/**
	 * Gets part of SQL Query to join with a gold database.
	 * 
	 * @param level level of the index, where the entity names are saved
	 * @param otherdb database to check with gold values 
	 * @return part of SQL Query
	 */
	public String[] getKRSql(int level, String otherdb) {
		String rsql = "";
		String ksql = Math.abs((level)) + "";
		if (level != 0) {
			if (level < 0) {
				int llevel = level;
				while (llevel != 0) {
					ksql = Math.abs((llevel)) + "";
					String kbefore = Math.abs((llevel + 1)) + "";

					rsql += " INNER JOIN " + otherdb + "categoryentries k" + ksql + " ON k" + ksql + ".ID = k" + kbefore
							+ ".parentID ";

					llevel++;
				}
			}
		}
		String[] krsql = new String[2];
		krsql[0] = ksql;
		krsql[1] = rsql;
		return krsql;

	}

	/**
	 * Gets taggings (with score) per document.
	 * 
	 * @param cIDList list of category IDs
	 * @param dIDList list of document IDs
	 * @param uID user ID 
	 * @param level level of the index, where the entity names are saved
	 * @param usertype type of user (for evaluation runs)
	 * @param otherdb database to check with gold values 
	 * @return hash map 
	 */
	public HashMap<Integer, DocumentTagsForWordWithWeights> getDocumentTagsForWordWithWeights(
			ArrayList<Integer> cIDList, ArrayList<Integer> dIDList, int uID, int level, String usertype,
			String otherdb) { // word also named token
		// other gold db
		if (otherdb != "") {
			otherdb += ".";
		}

		HashMap<Integer, DocumentTagsForWordWithWeights> dListreturn = new HashMap<Integer, DocumentTagsForWordWithWeights>();

		DocumentTagsForWordWithWeights dreturn;

		TreeMap<Integer, ArrayList<Integer>> lbMap = new TreeMap<Integer, ArrayList<Integer>>();
		TreeMap<Integer, ArrayList<Integer>> cbMap = new TreeMap<Integer, ArrayList<Integer>>();
		TreeMap<Integer, ArrayList<Integer>> leMap = new TreeMap<Integer, ArrayList<Integer>>();
		TreeMap<Integer, ArrayList<Integer>> ceMap = new TreeMap<Integer, ArrayList<Integer>>();
		HashMap<Integer, WeightEntry> lWE = new HashMap<Integer, WeightEntry>();
		HashSet<Integer> dusers = new HashSet<Integer>();

		PreparedStatement ps;

		try {
			String[] krsql = getKRSql(level, otherdb);
			String rsql = krsql[1];
			String ksql = krsql[0];

			String cIDs = StringUtils.join(cIDList, ",");
			String dIDs = StringUtils.join(dIDList, ",");

			String sql = "SELECT k" + ksql
					+ ".content, tw.uID, tw.val, l.ownerID, k0.parentID, k0.content, t.pos_line_begin, t.pos_line_end, t.pos_char_begin, t.pos_char_end, k0.indexID, l.documentID FROM ("
					+ otherdb + "categoryentries k0 " + rsql + " INNER JOIN " + otherdb
					+ "lnk_text_index l ON k0.ID = l.lemmaID) INNER JOIN " + otherdb
					+ "textpassages t ON l.ID = t.lnkID LEFT JOIN " + otherdb
					+ "tp_weight tw ON t.ID = tw.tID WHERE k0.indexID IN (" + cIDs + ") AND l.documentID IN (" + dIDs
					+ ") ";

			String sqlRestrict = getRestrictionsSQLString(uID, usertype);
			sql += sqlRestrict;
			// sql += " ORDER BY k"+ksql+".content";
			sql += " ORDER BY l.documentID";

			ps = con.prepareStatement(sql);

			// ps.setInt(1, cID);
			// ps.setInt(1, dID);

			Log.o2(sql);

			// Log.o2("SELECT k"+ksql+".content, tw.uID, tw.val, l.ownerID, k0.parentID,
			// k0.content, t.pos_line_begin, t.pos_line_end, t.pos_char_begin,
			// t.pos_char_end, k0.indexID, l.documentID FROM (categoryentries k0 "+rsql+" INNER
			// JOIN lnk_text_index l ON k0.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID
			// = t.lnkID LEFT JOIN tp_weight tw ON t.ID = tw.tID WHERE k0.indexID IN
			// ("+cIDs+") AND l.documentID IN ("+dIDs+") " + sqlRestrict + " ORDER BY
			// l.documentID, k"+ksql+".content");

			ResultSet rs = ps.executeQuery(); // LIMIT 0,1

			int wcnt = 0;
			int olddocument = -1;

			while (rs.next()) {
				String name = rs.getString(1);
				String content = rs.getString(6);
				int document = rs.getInt(12);
				int lb = rs.getInt(7);
				int le = rs.getInt(8);
				int cb = rs.getInt(9);
				int ce = rs.getInt(10);

				int userint = rs.getInt(4); 
				String agg = "0.8"; 
				if (rs.getInt(2) != 0) { // the column value; if the value is SQL NULL, the value returned is 0
											// http://docs.oracle.com/javase/6/docs/api/java/sql/ResultSet.html#getInt%28int%29
					if (usertype.equals("wu") || usertype.equals("wul") || usertype.equals("wulr")
							|| usertype.equals("wug") || usertype.equals("wugr")) {
						userint = rs.getInt(2);
					}
					if (usertype.equals("cu")) {
						userint = rs.getInt(11);
					}
					if (usertype.equals("fu")) {
						userint = rs.getInt(4);
					}
					agg = rs.getString(3);
				}
				dusers.add(userint);
				Log.o2("P" + lb + "." + cb + "-" + le + "." + ce + "_name " + name + " user " + userint + " w:" + agg
						+ " content " + content + " document " + document);

				if (olddocument != -1 && olddocument != document) {
					dreturn = new DocumentTagsForWordWithWeights(lbMap, cbMap, leMap, ceMap, lWE, dusers);
					dListreturn.put(olddocument, dreturn);
					lbMap = new TreeMap<Integer, ArrayList<Integer>>();
					cbMap = new TreeMap<Integer, ArrayList<Integer>>();
					leMap = new TreeMap<Integer, ArrayList<Integer>>();
					ceMap = new TreeMap<Integer, ArrayList<Integer>>();
					lWE = new HashMap<Integer, WeightEntry>();
					dusers = new HashSet<Integer>();
				}

				WeightEntry we = new WeightEntry(userint, name, agg);
				lbMap = putList(lbMap, lb, wcnt);
				cbMap = putList(cbMap, cb, wcnt);
				leMap = putList(leMap, le, wcnt);
				ceMap = putList(ceMap, ce, wcnt);

				lWE.put(wcnt, we);
				wcnt++;
				olddocument = document;
			}

			dreturn = new DocumentTagsForWordWithWeights(lbMap, cbMap, leMap, ceMap, lWE, dusers);
			dListreturn.put(olddocument, dreturn);

			ps.close();
		} catch (SQLException e) {
			
			e.printStackTrace();
			System.exit(0);
		}

		return dListreturn;

	}

	/**
	 * Puts a value into a list, which itself is part of a tree map.
	 * 
	 * @param h tree map
	 * @param k key
	 * @param v value
	 * @return tree map with inserted entry
	 */
	private TreeMap<Integer, ArrayList<Integer>> putList(TreeMap<Integer, ArrayList<Integer>> h, Integer k, Integer v) {
		ArrayList<Integer> a;
		if (!h.containsKey(k)) {
			a = new ArrayList<Integer>();
		} else {
			a = h.get(k);
		}
		a.add(v);

		h.put(k, a);

		return h;
	}

	/**
	 * Gets a filter by user for a SQL Query.
	 * @param uID user ID 
	 * @param usertype type of user (for evaluation runs)
	 * @return part of SQL Statement
	 */
	protected String getRestrictionsSQLString(int uID, String usertype) {
		String sqlRestrict = "";
		if (uID > 0) {
			if (usertype.equals("wu")) {
				sqlRestrict = " AND tw.uID = " + uID;
			}
			// no restriction for wul and wug
			if (usertype.equals("fu")) {
				sqlRestrict = " AND l.ownerID = " + uID;
			}

		}
		return sqlRestrict;
	}

	// getTagforWordswithWeightbySpecificAttribute

	// getTagforWordwithWeightbyUser

	/**
	 * Gets users involved to create a category.
	 * @param cID category ID
	 * @param usertype type of user (for evaluation runs)
	 * @return list of user IDs
	 */
	public HashSet<Integer> getUsersOfCategory(int cID, String usertype) {
		HashSet<Integer> users = new HashSet<Integer>();
		try {
			Statement s = con.createStatement();
			String sql = "";
			// not used by wug
			if (usertype.equals("wu") || usertype.equals("wul") || usertype.equals("wulr")) {
				sql = "SELECT DISTINCT tw.uID FROM (categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID) INNER JOIN textpassages t ON l.ID = t.lnkID LEFT JOIN tp_weight tw ON t.ID = tw.tID WHERE k.indexID = "
						+ cID;

			}
			if (usertype.equals("fu")) {
				sql = "SELECT DISTINCT l.ownerID FROM categoryentries k INNER JOIN lnk_text_index l ON k.ID = l.lemmaID WHERE indexID = "
						+ cID;
			}
			ResultSet rs;

			rs = s.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt(1);
				users.add(id);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return users;
	}

	// getTagforWordwithWeightbyTimeFrame

	/**
	 * Converts XML text to plain text.
	 * 
	 * @param text XML containing the text
	 * @return plain text
	 */
	private String getTextfromDBstring(String text) {

		text = text.replaceAll("</hi>", "");
		text = text.replaceAll("<hi[^<>]*>", "");
		text = text.replaceAll("<lb/>", "\n");

		text = StringEscapeUtils.unescapeHtml4(text);

		return text;
	}

	/**
	 * Gets name of the chunker by category ID
	 * @param cID category ID
	 * @return chunker name
	 */
	public String getChunkerNamebyID(Integer cID) {

		Statement s;
		String text = "";

		try {
			s = con.createStatement();

			String sql = "SELECT name FROM an_chunkers WHERE ID = " + cID;

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			while (rs.next()) {
				text = rs.getString(1);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return text;

	}

	/**
	 * Gets document ID, which have Crowd Flower results.
	 * @return list of document IDs
	 */
	public ArrayList<Integer> getCFdocIDs() {
		ArrayList<Integer> cfdocs = new ArrayList<Integer>();
		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT DISTINCT docID FROM cf_results ";

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			while (rs.next()) {
				int docID = rs.getInt(1);
				cfdocs.add(docID);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return cfdocs;

	}

	/**
	 * Gets Crowd Flower units for one document.
	 * 
	 * @param docID document ID
	 * @return list of Crowd Flower units
	 */
	public ArrayList<Cfunit> getCFTextpassages(int docID) {

		ArrayList<Cfunit> cfunits = new ArrayList<Cfunit>();
		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT DISTINCT docID,part,tid,tag,plb,pcb,ple,pce,text FROM cf_results WHERE docID=" + docID
					+ " ORDER BY docID,part,tid";
			;

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			while (rs.next()) {
				int cdocID = rs.getInt(1);
				int part = rs.getInt(2);
				long tid = rs.getLong(3);
				String tag = rs.getString(4);
				int plb = rs.getInt(5);
				int pcb = rs.getInt(6);
				int ple = rs.getInt(7);
				int pce = rs.getInt(8);
				String text = rs.getString(9);

				Cfunit cfunit = new Cfunit(docID, part, tid, -1, -1, tag, -1.0, plb, pcb, ple, pce, text);

				cfunits.add(cfunit);

			}

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return cfunits;

	}

	/**
	 * Gets the weights of one textpassage in the Crowd Flower table.
	 * 
	 * @param docID document ID
	 * @param part number of passage
	 * @param tid textpassage id
	 * @return hash map with key user ID and value Crowd Flower trust 
	 */
	public HashMap<Long, Double> getCFTextpassageWeight(int docID, int part, long tid) {

		HashMap<Long, Double> cfweights = new HashMap<Long, Double>();
		try {
			Statement s;
			s = con.createStatement();
			String sql = "SELECT cfuserID,trust FROM cf_results WHERE docID=" + docID + " AND part = " + part
					+ " AND tid = " + tid;

			ResultSet rs = s.executeQuery(sql); // LIMIT 0,1

			while (rs.next()) {
				long cfuserid = rs.getLong(1);
				double trust = rs.getDouble(2);
				cfweights.put(cfuserid, trust);

			}

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return cfweights;

	}
	

	/**
	 * Gets next ID of table.
	 * To be implemented in derived class. 
	 * 
	 * @param table
	 * @return next ID
	 */
	public int getNextID(String table) {
		return 1;
	}

	/**
	 * Executes a SQL Query.
	 * To be implemented in derived class. 
	 * 
	 * @param sql SQL Query
	 */
	public void executeQuery(String sql) {
		return;
	}

	/**
	 * getter
	 * @return user
	 */
	public int getUser() {
		return user;
	}

	/**
	 * @param user
	 */
	/**
	 * @param user
	 */
	public void setUser(int user) {
		this.user = user;
	}

}
