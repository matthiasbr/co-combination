/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

/**
 * Container for weight entries.
 *
 */
public class WeightEntry {
	/**
	 * user ID
	 */
	private int userw;
	/**
	 * name
	 */
	private String name;
	/**
	 * weight
	 */
	private String weight;

	/**
	 * constructor
	 * 
	 * @param userw
	 * @param name
	 * @param weight
	 */
	public WeightEntry(int userw, String name, String weight) {
		super();
		this.userw = userw;
		this.name = name;
		this.weight = weight;
	}

	/**
	 * getter
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * setter
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * getter
	 * @return weight
	 */
	public String getWeight() {
		return weight;
	}

	/**
	 * setter
	 * @param weight
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}
	
	/**
	 * getter
	 * @return userw
	 */
	public int getUserw() {
		return userw;
	}

	/**
	 * setter
	 * @param userw
	 */
	public void setUserw(int userw) {
		this.userw = userw;
	}

}
