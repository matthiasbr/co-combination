/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import com.aliasi.chunk.CharLmHmmChunker;
import com.aliasi.chunk.ConfidenceChunker;
import com.aliasi.hmm.HmmCharLmEstimator;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;

import vre.analysis.util.DBCatParser;
import vre.util.Log;

/**
 *  Iteration of the Co-Forest (Li, Ming und Zhou, Zhi-Hua (2007): 
 *  Improve Computer-Aided Diagnosis With Machine Learning Techniques Using Undiagnosed Samples.
 *  In: Trans. Sys. Man Cyber. Part A 37.6, S. 1088–1098. issn: 1083-
 *  4427. doi: 10.1109/TSMCA.2007.904745) algorithm.
 *
 */
public class CoForestIterationStatNER extends AbstractAnalyzerExt {

	static final int MAX_N_GRAM = 8;
	static final int NUM_CHARS = 256;
	static final double LM_INTERPOLATION = MAX_N_GRAM; // default behavior

	/**
	 * ID of category
	 */
	int cID;
	/**
	 * ID of view
	 */
	int vID;
	/**
	 * database connection
	 */
	private DatabaseConnection con;
	/**
	 * error estimation of iteration
	 */
	private double eSubit;
	/**
	 * error estimation of previous iteration
	 */
	private double eSubitPrev;
	/**
	 * predictive confidence of previous iteration
	 */
	private double WSubitPrev;
	/**
	 * predictive confidence of iteration
	 */
	private double WSubit = 0;
	/**
	 * confidence threshold
	 */
	private double confidenceThreshold;
	/**
	 * partition of the data (for evaluation runs)
	 */
	private int partition;
	/**
	 * size of the training set (for evaluation runs)
	 */
	private double ts;
	/**
	 * is it a development or test run (for evaluation runs)
	 */
	private String developmentortest;
	/**
	 * level of the index, where the entity names are saved
	 */
	private int level;
	/**
	 * type of user (for evaluation runs)
	 */
	private String usertype;
	/**
	 * size of views
	 */
	private int viewsize;
	/**
	 * initial ID of category
	 */
	private int initcID;

	/**
	 * Switch to use subsampling and confidence extension of Co-Forest
	 */
	private boolean subsamplingAndConfidence;

	/**
	 * map containing category IDs representing latest iteration
	 */
	private HashMap<Integer, Integer> highestcIDMapt = new HashMap<Integer, Integer>();

	/**
	 * constructor
	 * 
	 * @param vID
	 * @param cID
	 * @param con
	 * @param highestcIDMapt
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param eSubit
	 * @param eSubitPrev
	 * @param WSubitPrev
	 * @param confidenceThreshold
	 * @param viewsize
	 * @param initcID
	 * @param classifiertype
	 * @param minConf
	 * @param subsamplingAndConfidence
	 */
	public CoForestIterationStatNER(int vID, int cID, DatabaseConnection con, HashMap<Integer, Integer> highestcIDMapt,
			int partition, double ts, String developmentortest, int level, String usertype, double eSubit,
			double eSubitPrev, double WSubitPrev, double confidenceThreshold, int viewsize, int initcID,
			String classifiertype, double minConf, boolean subsamplingAndConfidence) {
		this.cID = cID;
		this.vID = vID;

		this.con = con;

		this.eSubit = eSubit;
		this.eSubitPrev = eSubitPrev;
		this.WSubitPrev = WSubitPrev;
		this.confidenceThreshold = confidenceThreshold;

		this.partition = partition;
		this.ts = ts;
		this.developmentortest = developmentortest;
		this.level = level;
		this.usertype = usertype;
		this.viewsize = viewsize;
		this.highestcIDMapt = highestcIDMapt;
		this.initcID = initcID;

		this.classifiertype = classifiertype;
		this.minConf = minConf;

		this.subsamplingAndConfidence = subsamplingAndConfidence;

		chunker = getChunker();
	}

	/**
	 * Getter for the chunker.
	 * 
	 * @return chunker
	 */
	protected ConfidenceChunker getChunker() {

		Log.o1("Setting up Chunker Estimator");
		TokenizerFactory factory = IndoEuropeanTokenizerFactory.INSTANCE;
		HmmCharLmEstimator hmmEstimator = new HmmCharLmEstimator(MAX_N_GRAM, NUM_CHARS, LM_INTERPOLATION);
		CharLmHmmChunker chunkerEstimator = new CharLmHmmChunker(factory, hmmEstimator);

		Log.o1("Setting up Data Parser");
		@SuppressWarnings("deprecation")

		DBCatParser parser = new DBCatParser(vID, con, highestcIDMapt, partition, ts, developmentortest, level,
				usertype, eSubit, eSubitPrev, WSubitPrev, confidenceThreshold, viewsize, initcID,
				subsamplingAndConfidence);
		parser.setHandler(chunkerEstimator);

		Log.o1("Training with Data from Categories");
		parser.parseCategory(Integer.valueOf(cID));

		WSubit = parser.getW();

		File modelFile = new File("chunkers/" + cID + "chunker.txt");

		Log.o1("Compiling and Writing Model to File=" + modelFile);
		try {
			AbstractExternalizable.compileTo(chunkerEstimator, modelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}


		Log.o1("Reading chunker from file=" + modelFile);
		ConfidenceChunker chunker;
		try {
			chunker = (ConfidenceChunker) AbstractExternalizable.readObject(modelFile);
			return chunker;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * getter
	 * @return WSubit
	 */
	public double getW() {
		return WSubit;
	}

}
