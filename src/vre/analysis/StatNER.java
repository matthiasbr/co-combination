/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import java.io.File;
import java.io.IOException;

import com.aliasi.chunk.CharLmHmmChunker;
import com.aliasi.chunk.ConfidenceChunker;
import com.aliasi.hmm.HmmCharLmEstimator;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;
import com.aliasi.util.AbstractExternalizable;

import vre.analysis.util.CFTableParser;
import vre.analysis.util.DBCatParser;
import vre.analysis.util.TextAParser;
import vre.util.Log;

/**
 * Generates a statistical parser.
 *
 */
public class StatNER extends AbstractAnalyzerExt {

	static final int MAX_N_GRAM = 8;
	static final int NUM_CHARS = 256; 
	static final double LM_INTERPOLATION = MAX_N_GRAM; // default behavior

	/**
	 * IDs of the categories
	 */
	int[] cID;
	/**
	 * IDs of the views
	 */
	int[] vID;
	/**
	 * database connection
	 */
	DatabaseConnectionAdv[] con;
	/**
	 * file path
	 */
	String path = "";
	/**
	 * filename of the chunker
	 */
	String chunkerFileName;
	/**
	 * type of datasource
	 */
	String sourcetype = "";
	/**
	 * partition of the data (for evaluation runs)
	 */
	int partition = 0;
	/**
	 * size of the training set (for evaluation runs)
	 */
	double ts = 0.0;
	/**
	 *  is it a development or test run (for evaluation runs)
	 */
	String developmentortest;
	/**
	 * level of the index, where the entity names are saved
	 */
	int level = 0;
	/**
	 *  type of user (for evaluation runs)
	 */
	String usertype = "fu";

	/**
	 * constructor for sourcetype "chunker"
	 * 
	 * @param chunkerID
	 * @param con
	 * @param name
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param classifiertype
	 * @param minConf
	 */
	public StatNER(int chunkerID, DatabaseConnectionAdv con, String name, int partition, double ts,
			String developmentortest, int level, String usertype, String classifiertype, double minConf) {
		this.cID = new int[1];
		this.vID = new int[1];
		this.con = new DatabaseConnectionAdv[1];
		this.cID[0] = 0;
		this.vID[0] = chunkerID;
		this.con[0] = con;
		this.partition = partition;
		this.ts = ts;
		this.level = level;
		this.usertype = usertype;
		this.developmentortest = developmentortest;
		this.classifiertype = classifiertype;
		this.minConf = minConf;
		sourcetype = "chunker";
		this.chunkerFileName = con.getChunkerNamebyID(vID[0]);
		chunker = getChunker();

	}

	/**
	 * constructor for sourcetype "con" database
	 * 
	 * @param cID
	 * @param uID
	 * @param con
	 * @param name
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param classifiertype
	 * @param minConf
	 */
	public StatNER(int cID, int uID, DatabaseConnectionAdv con, String name, int partition, double ts,
			String developmentortest, int level, String usertype, String classifiertype, double minConf) {
		this.cID = new int[1];
		this.vID = new int[1];
		this.con = new DatabaseConnectionAdv[1];
		this.cID[0] = cID;
		this.vID[0] = uID;
		this.con[0] = con;
		this.partition = partition;
		this.ts = ts;
		this.developmentortest = developmentortest;
		this.level = level;
		this.usertype = usertype;
		this.classifiertype = classifiertype;
		this.minConf = minConf;
		sourcetype = "con";
		this.chunkerFileName = "c" + cID + "_u" + uID + "_p" + partition + name + "_chunker.txt";
		setChunker();
		chunker = getChunker();

	}

	/**
	 * constructor for sourcetype "chunker"
	 * 
	 * @param chunkername
	 * @param con
	 * @param name
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param classifiertype
	 * @param minConf
	 */
	public StatNER(String chunkername, DatabaseConnectionAdv con, String name, int partition, double ts,
			String developmentortest, int level, String usertype, String classifiertype, double minConf) {
		this.cID = new int[1];
		this.vID = new int[1];
		this.con = new DatabaseConnectionAdv[1];
		this.cID[0] = 0;
		this.vID[0] = 0;
		this.con[0] = con;
		this.partition = partition;
		this.ts = ts;
		this.level = level;
		this.usertype = usertype;
		this.classifiertype = classifiertype;
		this.minConf = minConf;
		sourcetype = "chunker";
		this.chunkerFileName = chunkername;
		chunker = getChunker();

	}

	/**
	 * constructor for sourcetype "conmul": multiple databases
	 * 
	 * @param cID
	 * @param uID
	 * @param con
	 * @param name
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param classifiertype
	 * @param minConf
	 */
	public StatNER(int cID[], int uID[], DatabaseConnectionAdv[] con, String name, int partition, double ts,
			String developmentortest, int level, String usertype, String classifiertype, double minConf) {
		this.cID = cID;
		this.vID = uID;
		this.con = con;
		this.partition = partition;
		this.ts = ts;
		this.developmentortest = developmentortest;
		this.level = level;
		this.usertype = usertype;
		this.classifiertype = classifiertype;
		this.minConf = minConf;
		sourcetype = "conmul";
		this.chunkerFileName = "Multi_" + name + "_chunker.txt";

		setChunker(); 
		chunker = getChunker();

	}

	/**
	 * constructor for sourcetype "cftable": Crowd Flower table
	 * 
	 * @param uID
	 * @param con
	 * @param name
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param usertype
	 * @param classifiertype
	 * @param minConf
	 */
	public StatNER(int uID[], DatabaseConnectionAdv con, String name, int partition, double ts,
			String developmentortest, String usertype, String classifiertype, double minConf) {
		this.vID = uID;
		this.con = new DatabaseConnectionAdv[1];
		this.con[0] = con;
		this.partition = partition;
		this.ts = ts;
		this.developmentortest = developmentortest;
		this.usertype = usertype;
		this.classifiertype = classifiertype;
		this.minConf = minConf;
		sourcetype = "cftable";
		this.chunkerFileName = "CF_" + name + "_chunker.txt";
		setChunker();
		chunker = getChunker();

	}

	/**
	 * constructor for sourcetype "file": direct loading of chunker model file
	 * 
	 * @param vID
	 * @param con
	 * @param path
	 * @param name
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param classifiertype
	 * @param minConf
	 */
	public StatNER(int vID, DatabaseConnectionAdv con, String path, String name, int partition, double ts,
			String developmentortest, int level, String usertype, String classifiertype, double minConf) {
		this.cID = new int[1];
		this.vID = new int[1];
		this.con = new DatabaseConnectionAdv[1];
		this.vID[0] = vID;
		this.con[0] = con;
		this.path = path;
		this.partition = partition;
		this.ts = ts;
		this.developmentortest = developmentortest;
		this.level = level;
		this.usertype = usertype;
		this.classifiertype = classifiertype;
		this.minConf = minConf;
		sourcetype = "file";
		this.chunkerFileName = vID + "_" + name + "_chunker.txt";
		setChunker();
		chunker = getChunker();

	}


	/**
	 * Setting up the chunker.
	 */
	protected void setChunker() {

		Log.o1("Setting up Chunker Estimator");
		TokenizerFactory factory = IndoEuropeanTokenizerFactory.INSTANCE;
		HmmCharLmEstimator hmmEstimator = new HmmCharLmEstimator(MAX_N_GRAM, NUM_CHARS, LM_INTERPOLATION);
		CharLmHmmChunker chunkerEstimator = new CharLmHmmChunker(factory, hmmEstimator);

		Log.o1("Setting up Data Parser");

		if (sourcetype.equals("file")) {
			TextAParser parser = new TextAParser();
			parser.setHandler(chunkerEstimator);

			Log.o1("Training with Data from Categories in File");
			parser.parseText(path);

		} else if (sourcetype.equals("cftable")) {
			CFTableParser parser = new CFTableParser();
			parser.setHandler(chunkerEstimator);

			Log.o1("Training with Data from Categories in Crowdflower Table");
			parser.parseCFTable();

		} else if (sourcetype.equals("conmul")) {
			DBCatParser parser = new DBCatParser(vID[0], con[0], partition, ts, developmentortest, level, usertype); 																							
			parser.setHandler(chunkerEstimator);
			Log.o1("Training with Data from Categories");

			for (int j = 0; j < vID.length; j++) {
				Log.o1("Training with Data from Category" + cID[j]);
				parser.parseDatabase(Integer.valueOf(cID[j]), con[j]);
			}

		} else {
			DBCatParser parser = new DBCatParser(vID[0], con[0], partition, ts, developmentortest, level, usertype);
			parser.setHandler(chunkerEstimator);
			Log.o1("Training with Data from Categories");
			parser.parseCategory(Integer.valueOf(cID[0]));
		}

		File modelFile = new File("chunkers/" + chunkerFileName);

		Log.o1("Compiling and Writing Model to File=" + modelFile);
		try {
			AbstractExternalizable.compileTo(chunkerEstimator, modelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Getter for the chunker.
	 */
	protected ConfidenceChunker getChunker() {

		File modelFile = new File("chunkers/" + chunkerFileName);

		Log.o1("Reading chunker from file=" + modelFile);
		// Chunker chunker;
		// NBestChunker chunker;
		ConfidenceChunker chunker;

		try {
			// chunker = (Chunker) AbstractExternalizable.readObject(modelFile);
			// chunker = (NBestChunker) AbstractExternalizable.readObject(modelFile);
			chunker = (ConfidenceChunker) AbstractExternalizable.readObject(modelFile);

			return chunker;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return null;

	}

	/**
	 * getter 
	 * @return partition
	 */
	public int getPartition() {
		return partition;
	}

	/**
	 * setter
	 * @param partition
	 */
	public void setPartition(int partition) {
		this.partition = partition;
	}

}
