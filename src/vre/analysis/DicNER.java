/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import static vre.util.Convert.getPosFromNumber;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.Chunking;
import com.aliasi.dict.ApproxDictionaryChunker;
import com.aliasi.dict.DictionaryEntry;
import com.aliasi.dict.ExactDictionaryChunker;
import com.aliasi.dict.MapDictionary;
import com.aliasi.dict.TrieDictionary;
import com.aliasi.spell.FixedWeightEditDistance;
import com.aliasi.spell.WeightedEditDistance;
import com.aliasi.tokenizer.IndoEuropeanTokenizerFactory;
import com.aliasi.tokenizer.TokenizerFactory;

import vre.analysis.util.DBTextpassage;
import vre.util.Log;

/**
 * Analyzer which uses dictionaries.
 *
 */
public class DicNER extends AbstractAnalyzer {

	final double CHUNK_SCORE = 1.0;
	/**
	 * dictionary, which maps two strings
	 */
	private MapDictionary<String> dictionary = new MapDictionary<String>();

	/**
	 * dictionary, which maps strings to class
	 */
	private TrieDictionary<String> dict = new TrieDictionary<String>();

	/**
	 * constructor
	 * 
	 * @param toFind hash set of strings
	 */
	public DicNER(HashMap<String,String> toFind) {
		for (Entry<String, String> name : toFind.entrySet()) {
			dict.addEntry(new DictionaryEntry<String>(name.getKey(), name.getKey()));
			dictionary.addEntry(new DictionaryEntry<String>(name.getKey(), name.getValue(), CHUNK_SCORE));

		}

	}

	/**
	 * Method for exact dictionary lookup
	 * 
	 * @param docId documentID
	 * @param s string
	 * @return classification result
	 */
	public String classify(int docId, String s) {

		ExactDictionaryChunker dictionaryChunkerTF = new ExactDictionaryChunker(dictionary,
				IndoEuropeanTokenizerFactory.INSTANCE, true, false);

		String returnvalue = "";
		HashSet<Integer> currentLemmaIDs = new HashSet<Integer>();
		ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();

		String text = s;

		Chunking chunking = dictionaryChunkerTF.chunk(text);
		for (Chunk chunk : chunking.chunkSet()) {
			int start = chunk.start();
			int end = chunk.end();
			String type = chunk.type();
			double score = chunk.score();
			String phrase = text.substring(start, end);
			Log.o3("     phrase=|" + phrase + "|" + " start=" + start + " end=" + end + " type=" + type
					+ " score=" + score);

			TextPassages.add(new DBTextpassage(phrase + " # " + type, getPosFromNumber(start, s),
					getPosFromNumber(end, s), String.valueOf(score)));

			HashSet<String> RegisterTopic = RegisterTopics.get(type);
			if (RegisterTopic == null)
				RegisterTopic = new HashSet<String>();
			RegisterTopic.add(phrase);

			RegisterTopics.put(type, RegisterTopic);

			if (LemmaIDs.get(type) == null)
				LemmaIDs.put(type, lemmaIdCounter++);

			String lemmaString = phrase + " # " + type;
			int currentLemmaID;
			if (LemmaIDs.get(lemmaString) == null) {
				currentLemmaID = lemmaIdCounter++;
				LemmaIDs.put(lemmaString, currentLemmaID);
			}
			currentLemmaID = LemmaIDs.get(lemmaString);
			currentLemmaIDs.add(currentLemmaID);

		}

		DocumentLemma.put(docId, currentLemmaIDs);
		DocumentTextPassages.put(docId, TextPassages);
		return returnvalue;

	}

	/**
	 * Method for not exact dictionary lookup.
	 * 
	 * @param docId documentID
	 * @param s string
	 * @return classification result
	 */
	public String classify_(int docId, String s) {

		TokenizerFactory tokenizerFactory = IndoEuropeanTokenizerFactory.INSTANCE;

		WeightedEditDistance editDistance
				= new FixedWeightEditDistance(0, -1, -1, -1, Double.NaN);

		double maxDistance = 1.0;

		ApproxDictionaryChunker chunker = new ApproxDictionaryChunker(dict, tokenizerFactory, editDistance,
				maxDistance);

		String returnvalue = "";
		HashSet<Integer> currentLemmaIDs = new HashSet<Integer>();
		ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();

		String text = s;

		Chunking chunking = chunker.chunk(text);

		for (Chunk chunk : chunking.chunkSet()) {
			int start = chunk.start();
			int end = chunk.end();
			String type = chunk.type();
			double score = chunk.score();
			String phrase = text.substring(start, end);
			Log.o3("     phrase=|" + phrase + "|" + " start=" + start + " end=" + end + " type=" + type
					+ " score=" + score);

			TextPassages.add(new DBTextpassage(phrase + " # " + type, getPosFromNumber(start, s),
					getPosFromNumber(end, s), String.valueOf(score)));

			HashSet<String> RegisterTopic = RegisterTopics.get(type);
			if (RegisterTopic == null)
				RegisterTopic = new HashSet<String>();
			RegisterTopic.add(phrase);

			RegisterTopics.put(type, RegisterTopic);

			if (LemmaIDs.get(type) == null)
				LemmaIDs.put(type, lemmaIdCounter++);

			String lemmaString = phrase + " # " + type;
			int currentLemmaID;
			if (LemmaIDs.get(lemmaString) == null) {
				currentLemmaID = lemmaIdCounter++;
				LemmaIDs.put(lemmaString, currentLemmaID);
			}
			currentLemmaID = LemmaIDs.get(lemmaString);
			currentLemmaIDs.add(currentLemmaID);

		}

		DocumentLemma.put(docId, currentLemmaIDs);
		DocumentTextPassages.put(docId, TextPassages);
		return returnvalue;

	}

}
