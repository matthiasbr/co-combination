/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis.util;

/**
 * Crowd Flower unit
 *
 */
public class Cfunit {
	/**
	 * document ID
	 */
	private int docID;
	/**
	 * part of text
	 */
	private int part;
	/**
	 * textpassage ID
	 */
	private long tid;
	/**
	 * Crowd Flower ID
	 */
	private long cfid;
	/**
	 * Crowd Flower user ID
	 */
	private long cfuserid;
	/**
	 * tag, which was assigned
	 */
	private String tag;
	/**
	 * trust in decision of user
	 */
	private double trust;
	/**
	 * position line begin
	 */
	private int plb;
	/**
	 * position char begin
	 */
	private int pcb;
	/**
	 * position line end
	 */
	private int ple;
	/**
	 * position char end
	 */
	private int pce;
	/**
	 * text of Crowd Flower unit
	 */
	private String text;

	/**
	 * Constructor
	 * 
	 * @param docID
	 * @param part
	 * @param tid
	 * @param cfid
	 * @param cfuserid
	 * @param tag
	 * @param trust
	 * @param plb
	 * @param pcb
	 * @param ple
	 * @param pce
	 * @param text
	 */
	public Cfunit(int docID, int part, long tid, long cfid, long cfuserid, String tag, double trust, int plb, int pcb,
			int ple, int pce, String text) {
		super();
		this.docID = docID;
		this.part = part;
		this.tid = tid;
		this.cfid = cfid;
		this.cfuserid = cfuserid;
		this.tag = tag;
		this.trust = trust;
		this.plb = plb;
		this.pcb = pcb;
		this.ple = ple;
		this.pce = pce;
		this.text = text;
	}

	/**
	 * getter
	 * @return docID
	 */
	public int getDocID() {
		return docID;
	}

	/**
	 * setter
	 * @param docID
	 */
	public void setDocID(int docID) {
		this.docID = docID;
	}

	/**
	 * getter
	 * @return part
	 */
	public int getPart() {
		return part;
	}

	/**
	 * setter
	 * @param part
	 */
	public void setPart(int part) {
		this.part = part;
	}

	/**
	 * getter
	 * @return tid
	 */
	public long getTid() {
		return tid;
	}

	/**
	 * setter
	 * @param tid
	 */
	public void setTid(long tid) {
		this.tid = tid;
	}

	/**
	 * getter
	 * @return cfid
	 */
	public long getCfid() {
		return cfid;
	}

	/**
	 * setter
	 * @param cfid
	 */
	public void setCfid(long cfid) {
		this.cfid = cfid;
	}

	/**
	 * getter
	 * @return cfuserid
	 */
	public long getCfuserid() {
		return cfuserid;
	}

	/**
	 * setter
	 * @param cfuserid
	 */
	public void setCfuserid(long cfuserid) {
		this.cfuserid = cfuserid;
	}

	/**
	 * getter
	 * @return tag
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * setter
	 * @param tag
	 */
	public void setTag(String tag) {
		this.tag = tag;
	}

	/**
	 * getter
	 * @return trust
	 */
	public double getTrust() {
		return trust;
	}

	/**
	 * setter
	 * @param trust
	 */
	public void setTrust(double trust) {
		this.trust = trust;
	}

	/**
	 * getter
	 * @return plb
	 */
	public int getPlb() {
		return plb;
	}

	/**
	 * setter
	 * @param plb
	 */
	public void setPlb(int plb) {
		this.plb = plb;
	}

	/**
	 * getter
	 * @return pcb
	 */
	public int getPcb() {
		return pcb;
	}

	/**
	 * setter 
	 * @param pcb
	 */
	public void setPcb(int pcb) {
		this.pcb = pcb;
	}

	/**
	 * getter
	 * @return ple
	 */
	public int getPle() {
		return ple;
	}

	/**
	 * setter
	 * @param ple
	 */
	public void setPle(int ple) {
		this.ple = ple;
	}

	/**
	 * getter
	 * @return pce
	 */
	public int getPce() {
		return pce;
	}

	/**
	 * setter
	 * @param pce
	 */
	public void setPce(int pce) {
		this.pce = pce;
	}

	/**
	 * getter
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * setter
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

}
