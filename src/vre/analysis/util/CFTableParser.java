/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.ChunkFactory;
import com.aliasi.chunk.Chunking;
import com.aliasi.chunk.ChunkingImpl;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.tag.StringTagging;

import vre.analysis.DatabaseConnection;
import vre.util.Log;

/**
 * A parser working on the Crowd Flower result table
 *
 */
public class CFTableParser extends ExtendedStringParser<ObjectHandler<Chunking>> {

	/**
	 * BIO scheme begin tag
	 */
	private final String mBeginTagPrefix = "B-";
	/**
	 * BIO scheme inner tag
	 */
	private final String mInTagPrefix = "I-";
	/**
	 * BIO scheme out tag
	 */
	private final String mOutTag = "O";

	/**
	 * database connection
	 */
	DatabaseConnection dbcon = new DatabaseConnection();

	/**
	 * constructor
	 */
	public CFTableParser() {
		super();

	}

	/**
	 * Parses the Crowd Flower table.
	 */
	public void parseCFTable() {

		ArrayList<Integer> cfus = dbcon.getCFdocIDs();

		for (Integer docID : cfus) {
			Log.o2(docID.toString());
			parsedoc(docID);
		}

	}

	/**
	 * Parses one document.
	 * @param docID ID of the document
	 */
	public void parsedoc(int docID) {

		ArrayList<Cfunit> cfunits = dbcon.getCFTextpassages(docID);

		ArrayList<String> tokenList = new ArrayList<String>();
		ArrayList<String> tagList = new ArrayList<String>();
		ArrayList<Double> scoresList = new ArrayList<Double>();

		for (Cfunit cfunit : cfunits) {
			tokenList.add(cfunit.getText());
			tagList.add(cfunit.getTag());
			HashMap<Long, Double> cfweights = dbcon.getCFTextpassageWeight(docID, cfunit.getPart(), cfunit.getTid());
			int aggc = 0;
			double aggw = 0.0;
			for (long cfuserid : cfweights.keySet()) {
				aggw += cfweights.get(cfuserid);
				aggc++;
			}
			double agg = aggw / aggc;

			scoresList.add(agg);
		}

		String[] tokens = new String[tokenList.size()];
		tokens = tokenList.toArray(tokens);
		String[] tags = new String[tagList.size()];
		tags = tagList.toArray(tags);
		Double[] scores = new Double[scoresList.size()];
		scores = scoresList.toArray(scores);

		processSentence(tokens, tags, scores);
		tokenList = null;
		tagList = null;
		scoresList = null;

	}

	/**
	 * Parses the input string.
	 *
	 * @param cs    character array representing the string
	 * @param start first char
	 * @param end   last char
	 */
	@Override
	public void parseString(char[] cs, int start, int end) {
		String in = new String(cs, start, end - start);
		String[] lines = in.split("\n");
		for (int i = 0; i < lines.length; ++i) {
			try {
				parse(lines[i]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Processes one sentence.
	 * @param tokens tokens
	 * @param tags tags
	 * @param scores scores for the tags
	 */
	void processSentence(String[] tokens, String[] tags, Double[] scores) {
		Log.o2(" in:: " + tokens.toString());

		int[] tokenStarts = new int[tokens.length];
		int[] tokenEnds = new int[tokens.length];
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tokens.length; ++i) {
			tokenStarts[i] = sb.length();
			sb.append(tokens[i]);
			tokenEnds[i] = sb.length();
			if (i + 1 < tokens.length)
				sb.append(' '); 
		}
		StringTagging tagging = new StringTagging(Arrays.asList(tokens), Arrays.asList(tags), sb.toString(),
				tokenStarts, tokenEnds);


		ChunkingImpl chunking = new ChunkingImpl(tagging.characters());
		for (int n = 0; n < tagging.size(); ++n) {
			String tag = tagging.tag(n);
			if (mOutTag.equals(tag))
				continue;
			if (!tag.startsWith(mBeginTagPrefix)) {
				if (n == 0) {
					String msg = "First tag must be out or begin." + " Found tagging.tag(0)=" + tagging.tag(0);
					throw new IllegalArgumentException(msg);
				}
				String msg = "Illegal tag sequence." + " tagging.tag(" + (n - 1) + ")=" + tagging.tag(n - 1)
						+ " tagging.tag(" + n + ")=" + tagging.tag(n);
				throw new IllegalArgumentException(msg);
			}
			String type = tag.substring(2);
			int start = tagging.tokenStart(n);
			String inTag = mInTagPrefix + type;
			while ((n + 1) < tagging.size() && inTag.equals(tagging.tag(n + 1)))
				++n;
			int end = tagging.tokenEnd(n);
			if (mOutTag.equals(type))
				continue;
			Chunk chunk = ChunkFactory.createChunk(start, end, type);
			chunking.add(chunk);
			Log.o2("Added Chunk with " + tag);
		}

		getHandler().handle(chunking);
	}

	/**
	 * Calculates average score.
	 * 
	 * @param weigths weights of the tokens
	 * @return average score
	 */
	Double scoreWeigths(HashMap<Integer, String> weigths) {
		int cnt = weigths.size();
		double agg = 0;
		for (String weigth : weigths.values()) {
			double d;
			try {
				d = Double.valueOf(weigth.trim()).doubleValue();
			} catch (NumberFormatException e) {
				System.out.println("NumberFormatException: " + e.getMessage());
				d = 1.0;
			}
			agg += d;
		}

		return (agg / cnt);
	}

}
