/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis.util;

import static vre.util.Convert.getDoubleFromString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import com.aliasi.chunk.BioTagChunkCodec;
import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.ChunkFactory;
import com.aliasi.chunk.Chunking;
import com.aliasi.chunk.ChunkingImpl;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.corpus.StringParser;
import com.aliasi.tag.StringTagging;
import com.aliasi.util.Strings;

import vre.analysis.DatabaseConnection;
import vre.analysis.DocumentTagsForWordWithWeights;
import vre.analysis.WeightEntry;
import vre.util.Log;



/**
 * Based on a string parser. Works within the database.
 * Subsampling and prediction of confidence extensions of Co-Forest (Li, Ming und Zhou, Zhi-Hua (2007): 
 *  Improve Computer-Aided Diagnosis With Machine Learning Techniques Using Undiagnosed Samples.
 *  In: Trans. Sys. Man Cyber. Part A 37.6, S. 1088–1098. issn: 1083-
 *  4427. doi: 10.1109/TSMCA.2007.904745) can be activated via variable. 
 *
 */
public class DBCatParser extends StringParser<ObjectHandler<Chunking>> {

	/**
	 * category ID
	 */
	protected int cID;
	/**
	 * document ID
	 */
	protected int dID;
	/**
	 * database connection
	 */
	protected DatabaseConnection mycon;
	/**
	 * tagging (with score) of the documents 
	 */
	protected HashMap<Integer, DocumentTagsForWordWithWeights> dList;
	/**
	 * user ID
	 */
	protected int uID;
	/**
	 * partition of the data (for evaluation runs)
	 */
	protected int partition;
	/**
	 * partition of the data (for evaluation runs)
	 */
	protected double ts;
	/**
	 * is it a development or test run (for evaluation runs)
	 */
	protected String developmentortest;
	/**
	 * for supervised algorithms, which work only on trainingset or initial phase of training
	 */
	protected boolean onlyTrainingSet;
	/**
	 * level of the index, where the entity names are saved
	 */
	protected int level;
	/**
	 * type of user (for evaluation runs)
	 */
	protected String usertype;
	/**
	 * maximum number of users
	 */
	protected int maxuser;
	/******
	 *  Subsampling and predictive Confidence
	 *****/
	/**
	 * Switch to use subsampling and confidence extension of Co-Forest
	 */
	private boolean subsamplingAndConfidence = false;
	/**
	 * error estimation of iteration
	 */
	private double eSubit;
	/**
	 * error estimation of previous iteration
	 */
	private double eSubitPrev;
	/**
	 * predictive confidence of iteration
	 */
	private double WSubitPrev;
	/**
	 * predictive confidence of previous iteration
	 */
	private double WSubit = 0;
	/**
	 * confidence threshold
	 */
	private double confidenceThreshold;
	/**
	 * size of views
	 */
	private int viewsize;
	/**
	 * initial ID of category
	 */
	private int initcID = -1;
	/**
	 * map containing category IDs representing latest iteration
	 */
	private HashMap<Integer, Integer> highestcIDMapt = new HashMap<Integer, Integer>();
	/**
	 * size of groups
	 */
	public static int groupsize;
	/**
	 * groups of users (with their IDs)
	 */
	public static HashMap<Integer, ArrayList<Integer>> usergs = new HashMap<Integer, ArrayList<Integer>>();
	/**
	 * user ID for document
	 */
	protected int duID;
	/**
	 * user ID for category
	 */
	protected int cuID;

	/**
	 * BIO scheme begin tag
	 */
	protected final String mBeginTagPrefix = "B_";
	/**
	 * BIO scheme inner tag
	 */
	protected final String mInTagPrefix = "I_";
	/**
	 * BIO scheme out tag
	 */
	protected final String mOutTag = "O";

	/**
	 * default constructor
	 */
	public DBCatParser() {
		super();
		uID = 0;
		mycon = new DatabaseConnection();
		partition = -1;
		ts = 0.1;
		level = 0;
		usertype = "fu";
		developmentortest = "development";
		onlyTrainingSet = true;
	}

	/**
	 * constructor for a regular parser
	 * 
	 * @param uID
	 * @param mycon
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 */
	public DBCatParser(int uID, DatabaseConnection mycon, int partition, double ts, String developmentortest, int level,
			String usertype) {
		super();
		this.uID = uID;
		this.mycon = mycon;
		this.partition = partition;
		this.ts = ts;
		this.level = level;
		this.usertype = usertype;
		this.developmentortest = developmentortest;
		onlyTrainingSet = true;
	}

	/**
	 * constructor for a parser supporting subsampling and confidence prediction
	 * 
	 * @param uID
	 * @param mycon
	 * @param highestcIDMapt
	 * @param partition
	 * @param ts
	 * @param developmentortest
	 * @param level
	 * @param usertype
	 * @param eSubit
	 * @param eSubitPrev
	 * @param WSubitPrev
	 * @param confidenceThreshold
	 * @param viewsize
	 * @param initcID
	 * @param subsamplingAndConfidence
	 */
	public DBCatParser(int uID, DatabaseConnection mycon, HashMap<Integer, Integer> highestcIDMapt, int partition,
			double ts, String developmentortest, int level, String usertype, double eSubit, double eSubitPrev,
			double WSubitPrev, double confidenceThreshold, int viewsize, int initcID,
			boolean subsamplingAndConfidence) {
		super();
		this.uID = uID;
		this.mycon = mycon;
		this.partition = partition;
		this.ts = ts;
		this.level = level;
		this.usertype = usertype;
		this.subsamplingAndConfidence = subsamplingAndConfidence;
		this.eSubit = eSubit;
		this.eSubitPrev = eSubitPrev;
		this.WSubitPrev = WSubitPrev;
		this.confidenceThreshold = confidenceThreshold;
		this.developmentortest = developmentortest;
		onlyTrainingSet = false;
		// onlyTrainingSet = true;
		this.viewsize = viewsize;
		this.highestcIDMapt = highestcIDMapt;
		this.initcID = initcID;
	}

	/**
	 * Parse given database by category ID
	 * 
	 * @param catid ID of category
	 * @param mycon database connection
	 */
	public void parseDatabase(int catid, DatabaseConnection mycon) {
		this.mycon = mycon;
		parseCategory(catid);
	}

	/**
	 * Parser by ID of category. Document set is collected by reference to category ID.
	 * 
	 * @param catid ID of category
	 */
	public void parseCategory(int catid) {

		for (int i = 0; i <= 1; i++) {
			// i == 0 standard
			// i == 1 additional

			boolean useTrain;
			boolean useDev;
			boolean useTest;
			String setname = "training";
			String otherdb = "";

			cuID = uID;
			int ccatid = catid;

			ArrayList<Integer> cIDList = new ArrayList<Integer>();

			if (developmentortest.equals("compare")) {
				setname = developmentortest;
				if (i == 0) {
					setname = "comparetraining";
					// cIDList.add(catid);

					if (initcID > -1) {
						cIDList.add(initcID);
						ccatid = initcID;
					} else {
						cIDList.add(catid);
						ccatid = catid;
					}
					// otherdb="";
					usertype = "wug";
				} else if (i == 1) {
					if (onlyTrainingSet)
						break;

					setname = "comparetraining";
					otherdb = "";
					usertype = "cu";
					cuID = 0;
					ccatid = catid;

					for (Entry<Integer, Integer> e : highestcIDMapt.entrySet()) {
						cIDList.add(e.getValue());
					}

				} else {
					break;
				}

			} else {
				if (i == 0) {
					useTrain = true;
					useDev = false;
					useTest = false;
					setname = "training";
					cIDList.add(catid);
					if (initcID > -1) {
						cIDList.add(initcID);
						ccatid = initcID;
					} else {
						cIDList.add(catid);
						ccatid = catid;
					}
				} else if (i == 1) {
					if (onlyTrainingSet)
						break;
					useTrain = false;
					useDev = (!onlyTrainingSet && developmentortest.equals("development"));
					useTest = (!onlyTrainingSet && developmentortest.equals("test"));
					setname = developmentortest;
					usertype = "cu";
					cuID = 0;
					ccatid = catid;

					for (Entry<Integer, Integer> e : highestcIDMapt.entrySet()) {
						cIDList.add(e.getValue());
					}

				} else {
					break;
				}
			}

			HashMap<Integer, String> docs = mycon.getDocumentsbymIDcIDs(0, cIDList, partition, ts, setname, usertype,
					cuID);
			ArrayList<Integer> dIDList = new ArrayList<Integer>();
			for (Integer k : docs.keySet()) {
				dIDList.add(k);
			}
			if (dIDList.size() == 0)
				continue;
			dList = mycon.getDocumentTagsForWordWithWeights(cIDList, dIDList, cuID, level, usertype, otherdb);

			int sumdocs = docs.size();

			int dCnt = 0;

			for (Entry<Integer, String> doc : docs.entrySet()) {

				dCnt++;
				String in = doc.getValue();
				cID = ccatid;
				dID = doc.getKey();

				if (!dList.containsKey(dID)) {
					Log.o1("WARNING No Infomration for Doc ID " + dID + " catid " + cID);
					continue;
				}

				HashSet<Integer> dusers = dList.get(dID).getDUsers();

				maxuser = dusers.size();

				if ((usertype.equals("wug") || usertype.equals("wugr")) && uID > 0) {

					// look up users in group by uID
					ArrayList<Integer> usergN = usergs.get(uID);

					int uindex = -1;
					duID = -1;
					// look for document users
					for (Integer duser : dusers) {

						if (usergN.contains(duser)) {
							int cuindex = usergN.indexOf(duser);
							if (uindex == -1) {
								uindex = cuindex;
								duID = duser;
							} else if (cuindex < uindex) {
								uindex = cuindex;
								duID = duser;
							}

						}
					}

					if (duID == -1) {
						// look for document users
						for (Integer duser : dusers) {

							// whether they are in a group
							boolean doescontain = false;
							for (Integer userg : usergs.keySet()) {
								ArrayList<Integer> usergS = usergs.get(userg);
								if (usergS.contains(duser)) {
									doescontain = true;
								}
							}

							if (!doescontain) {
								usergN.add(duser);
								usergs.put(uID, usergN);
								duID = duser;
								break;
							}

						}
					}

					if (duID == -1) {
						Log.o1("WARNING No User for Doc ID " + dID + " in view " + uID);

					}

				}

				Log.o2n("p");
				Log.o3("parse Doc for traing");
				parseString(in.toCharArray(), 0, in.length());

			}

		}

	}

	/**
	 * Parses the input string.
	 *
	 * @param cs    character array representing the string
	 * @param start first char
	 * @param end   last char
	 */
	@Override
	public void parseString(char[] cs, int start, int end) {
		String in = new String(cs, start, end - start);
		DocumentTagsForWordWithWeights d = dList.get(dID);
		processSentences(in, d);
		Log.o2n(".");
	}

	/**
	 * Process the sentences of the string.
	 * 
	 * @param in sentence string
	 * @param d weights for the document
	 */
	void processSentences(String in, DocumentTagsForWordWithWeights d) {

		// part 1
		String[] sentences = in.split("\n");
		for (int si = 0; si < sentences.length; ++si) {
			if (Strings.allWhitespace(sentences[si]))
				continue;
			String sentence = sentences[si];
			String[] tagTokens = sentence.split(" ");
			String[] tokens = new String[tagTokens.length];
			String[] tags = new String[tagTokens.length];
			String[] scores = new String[tagTokens.length];
			Log.o2(" in:: " + sentence);

			int startOfToken = 0;

			// create tagging directly from database 
			// therefore the step of creating a appropriate string representation is kept
			for (int i = 0; i < tagTokens.length; ++i) {
				Log.o2(tagTokens[i]);
				tokens[i] = tagTokens[i];

				int endOfToken = sentence.indexOf(32, startOfToken);

				if (endOfToken == -1) {
					endOfToken = sentence.length();
				}

				Log.o3("Token: " + tokens[i]);

				// +1 tk widget counting

				ArrayList<WeightEntry> weList = d.getWeightEntryKeys(si + 1, startOfToken, si + 1, endOfToken);

				String tag = mOutTag;
				Double score = 1.0;
				int scorecount = 0;

				HashMap<String, Double> scoreweights = new HashMap<String, Double>();

				// SubsamlingAndConfidence
				// must be subsampled so that W_i,t is less than (e_i,t-1 W_i,t-1) / e_i,t

				if (weList.size() > 0 && !subsampleCondition(0)) {
					Log.o2("size: " + weList.size());
				}
				// subampling and predictive confidence
				if (!subsamplingAndConfidence || !subsampleCondition(0)) { 

					boolean dUserscontainsuID = false;

					if (usertype.equals("wul") || usertype.equals("wulr")) {

						HashSet<Integer> dUsers = d.getDUsers();
						// TODO check uID or cuID
						dUserscontainsuID = dUsers.contains(uID);
					}
					if ((usertype.equals("wug") || usertype.equals("wug")) && uID > 0 && duID > -1) {
						dUserscontainsuID = true;
						// TODO
						if (!subsamplingAndConfidence) {
							maxuser = 1;
						}
					}
					maxuser = 1;

					// only if wug usertype handling
					if (dUserscontainsuID) {

						if (!subsamplingAndConfidence && !usertype.equals("wugr") && !usertype.equals("wulr")) {
							for (WeightEntry we : weList) {
								String ctag = we.getName();
								String cweight = we.getWeight();
								int cuser = we.getUserw();
								double cscore = getDoubleFromString(cweight);
								Log.o2("cTAG: " + ctag + ":" + cscore);
								// cuser == duID
								if ((usertype.equals("wul") && cuser == uID)
										|| (usertype.equals("wug") && usergs.get(uID).contains(cuser))) {
									tag = ctag;
									score = cscore;
									scorecount++;
									Log.o2("IS USER");
									break;
								}

							}

							// no weight information found
							if (scorecount == 0) {
								score = 1.0;
							}

						} else {

							for (WeightEntry we : weList) {
								String ctag = we.getName();
								String cweight = we.getWeight();
								int cuser = we.getUserw();
								double cscore = getDoubleFromString(cweight);
								Log.o2("cTAG: " + ctag + ":" + cscore);
								// cuser == duID
								if (((usertype.equals("wul") || usertype.equals("wulr")) && cuser == uID)
										|| ((usertype.equals("wug") || usertype.equals("wugr"))
												&& usergs.get(uID).contains(cuser))) {

									Log.o2("NOT USER");
									continue;

								}

								if (scoreweights.containsKey(ctag)) {
									double ascore = scoreweights.get(ctag);
									cscore += ascore;
								}
								scoreweights.put(ctag, cscore);
								if (cscore > score || tag.equals("O")) {
									tag = ctag;
									score = cscore;
								}
								scorecount++;

							}

							// no weight information found
							if (scorecount == 0) {
								score = 1.0;
							} else {
								score = score / (double) maxuser - 1;
							}

						}

					} else {

						for (WeightEntry we : weList) {
							String ctag = we.getName();
							String cweight = we.getWeight();
							int cuser = we.getUserw();
							double cscore = getDoubleFromString(cweight);
							Log.o2("cTAG: " + ctag + ":" + cscore);

							if (scoreweights.containsKey(ctag)) {
								double ascore = scoreweights.get(ctag);
								cscore += ascore;
							}
							scoreweights.put(ctag, cscore);
							if (cscore > score || tag.equals("O")) {
								tag = ctag;
								score = cscore;
							}
							scorecount++;

						}

						// no weight information found
						if (scorecount == 0) {
							score = 1.0;
						} else {
							score = score / (double) maxuser;
						}

					}

					boolean hasConfidence = true;
					// subampling and predictive confidence
					if (subsamplingAndConfidence && subsampleCondition(score)) {
						tag = mOutTag;
						score = 1.0;
						hasConfidence = false;
					}

					// next is confidence could be simple scoreweigths of course only with
					// subsampled tags

					double confidence = score;
					if (subsamplingAndConfidence && confidence < confidenceThreshold) {
						tag = mOutTag;
						score = 1.0;
						hasConfidence = false;
					}
					// adding to W
					if (hasConfidence) {
						WSubit += confidence;
					}

				}
				// subampling and predictive confidence End

				Log.o2("TAG: " + tag + ":" + score);

				if (score > 1.0) {
					score = 1.0;
				}

				tags[i] = tag;
				scores[i] = score.toString();
				startOfToken += tagTokens[i].length() + 1;

			}

			String[] normalTags = normalizeTags(tags);

			int[] tokenStarts = new int[tokens.length];
			int[] tokenEnds = new int[tokens.length];
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < tokens.length; ++i) {
				tokenStarts[i] = sb.length();
				sb.append(tokens[i]);
				tokenEnds[i] = sb.length();
				if (i + 1 < tokens.length)
					sb.append(' '); 
			}

			// part 2
			// parser
			ChunkingImpl chunking = new ChunkingImpl(sb.toString());
			for (int n = 0; n < tokens.length; ++n) {
				String tag = normalTags[n];
				if (mOutTag.equals(tag))
					continue; 
				if (!tag.startsWith(mBeginTagPrefix)) {
					if (n == 0) {
						String msg = "First tag must be out or begin." + " Found tagging.tag(0)=" + normalTags[0];
						throw new IllegalArgumentException(msg);
					}
					String msg = "Illegal tag sequence." + " tagging.tag(" + (n - 1) + ")=" + normalTags[n - 1]
							+ " tagging.tag(" + n + ")=" + normalTags[n];
					throw new IllegalArgumentException(msg);
				}
				String type = tag.substring(2);
				int start = tokenStarts[n];
				String inTag = mInTagPrefix + type;
				while ((n + 1) < tokens.length && inTag.equals(normalTags[n + 1]))
					++n;
				int end = tokenEnds[n];
				if (mOutTag.equals(type))
					continue; 
				String scoreString = scores[n];
				double score = getDoubleFromString(scoreString);
				Chunk chunk = ChunkFactory.createChunk(start, end, type, score);
				chunking.add(chunk);
				Log.o2("Added Chunk with " + tag + " and score" + score);
			}

			// part 3
			getHandler().handle(chunking);

		}
	}


	/**
	 * Process the sentences of the string.
	 * 
	 * @param in sentence string
	 */
	void processSentences(String in) {
		String[] sentences = in.split("\n");
		for (int i = 0; i < sentences.length; ++i) {
			if (Strings.allWhitespace(sentences[i]))
				continue;
			processSentence(sentences[i], i);
		}

	}

	/**
	 * Process one sentence.
	 * @param sentence string representation of the sentence
	 * @param sentenceI number of the sentence
	 * @param d weights for the document
	 */
	StringTagging getStringTagging(String sentence, int sentenceI, DocumentTagsForWordWithWeights d) {
		String[] tagTokens = sentence.split(" ");
		String[] tokens = new String[tagTokens.length];
		String[] tags = new String[tagTokens.length];
		String[] scores = new String[tagTokens.length];
		Log.o2(" in:: " + sentence);

		int startOfToken = 0;

		for (int i = 0; i < tagTokens.length; ++i) {
			Log.o2(tagTokens[i]);
			tokens[i] = tagTokens[i];

			int endOfToken = sentence.indexOf(32, startOfToken);

			if (endOfToken == -1) {
				endOfToken = sentence.length();
			}

			Log.o3("Token: " + tokens[i]);
			
			// +1 tk widget counting

			ArrayList<WeightEntry> weList = d.getWeightEntryKeys(sentenceI + 1, startOfToken, sentenceI + 1,
					endOfToken);

			String tag = mOutTag;
			Double score = 0.0;
			int scorecount = 0;

			// subampling and predictive confidence
			// must be subsampled so that W_i,t is less than (e_i,t-1 W_i,t-1) / e_i,t

			HashMap<String, Double> scoreweights = new HashMap<String, Double>();

			if (weList.size() > 0 && !subsampleCondition(0)) {
				Log.o3("size: " + weList.size());
			}

			if (!subsampleCondition(0)) { // no score added only test to reduce runningtime better sort of break;
				for (WeightEntry we : weList) {
					String ctag = we.getName();
					String cweight = we.getWeight();
					double cscore = getDoubleFromString(cweight);
					Log.o2("cTAG: " + ctag + ":" + cscore);

					if (scoreweights.containsKey(ctag)) {
						double ascore = scoreweights.get(ctag);
						cscore += ascore;
					}
					scoreweights.put(ctag, cscore);
					if (cscore > score) {
						tag = ctag;
						score = cscore;
					}
					scorecount++;

				}

				// no weight information found
				if (scorecount == 0) {
					score = 1.0;
				} else {
					score = score / (double) maxuser;
				}
			}

			Log.o2("TAG: " + tag + ":" + score);

			tags[i] = tag;
			scores[i] = score.toString();
			startOfToken += tagTokens[i].length() + 1;

		}

		String[] normalTags = normalizeTags(tags);

		int[] tokenStarts = new int[tokens.length];
		int[] tokenEnds = new int[tokens.length];
		StringBuilder sb = new StringBuilder();
		for (int ii = 0; ii < tokens.length; ++ii) {
			tokenStarts[ii] = sb.length();
			sb.append(tokens[ii]);
			tokenEnds[ii] = sb.length();
			if (ii + 1 < tokens.length)
				sb.append(' '); // single space separator except at end
		}
		StringTagging tagging = new StringTagging(Arrays.asList(tokens), Arrays.asList(normalTags), sb.toString(),
				tokenStarts, tokenEnds);
		return tagging;
	}

	/**
	 * Process one sentence.
	 * @param sentence string representation of the sentence
	 * @param sentenceI number of the sentence
	 * @return weights for the document
	 */
	StringTagging getStringTagging(String sentence, int sentenceI) {
		String[] tagTokens = sentence.split(" ");
		String[] tokens = new String[tagTokens.length];
		String[] tags = new String[tagTokens.length];
		String[] scores = new String[tagTokens.length];
		Log.o2(" in:: " + sentence);

		for (int i = 0; i < tagTokens.length; ++i) {
			Log.o2(tagTokens[i]);
			tokens[i] = tagTokens[i];

			int startOfToken = sentence.indexOf(tokens[i]);
			int endOfToken = sentence.indexOf(32, startOfToken);

			if (endOfToken == -1) {
				endOfToken = sentence.length();
			}

			Log.o3("Token: " + tokens[i]);

			HashMap<String, HashMap<Integer, String>> TagsForWordWithWeights = mycon.getTagsForWordWithWeights(cID, dID,
					sentenceI, startOfToken, endOfToken, cuID, level);

			String tag = mOutTag;
			Double score = 0.0;
			int scorecount = 0;
			int tagcount = 0;

			HashMap<String, Double> scoreweights = new HashMap<String, Double>();

			for (Entry<String, HashMap<Integer, String>> tagwithweightsentry : TagsForWordWithWeights.entrySet()) {
				String ctag = tagwithweightsentry.getKey();
				HashMap<Integer, String> cWeigths = tagwithweightsentry.getValue();
				double cscore = scoreWeigths(cWeigths);
				int cnum = cWeigths.size();
				cscore *= cnum; 
				Log.o2("TAG: " + ctag);

				if (scoreweights.containsKey(ctag)) {
					double ascore = scoreweights.get(ctag);
					cscore += ascore;
				}
				if (cscore > score) {
					tag = ctag;
					score = cscore;
				}

				scoreweights.put(ctag, cscore);
				tagcount++;
				scorecount += cnum;

			}

			// no weight information found
			if (scorecount == 0) {
				score = 1.0;
			} else {
				score = score / (double) maxuser;
			}
			// subampling and predictive confidence
			tags[i] = tag;
			scores[i] = score.toString();

		}

		String[] normalTags = normalizeTags(tags);

		int[] tokenStarts = new int[tokens.length];
		int[] tokenEnds = new int[tokens.length];
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tokens.length; ++i) {
			tokenStarts[i] = sb.length();
			sb.append(tokens[i]);
			tokenEnds[i] = sb.length();
			if (i + 1 < tokens.length)
				sb.append(' '); 
		}
		StringTagging tagging = new StringTagging(Arrays.asList(tokens), Arrays.asList(normalTags), sb.toString(),
				tokenStarts, tokenEnds);
		return tagging;
	}

	/**
	 * Get chunking from string tagging.
	 * 
	 * @param tagging tagging
	 * @return chunking
	 */
	ChunkingImpl getChunking(StringTagging tagging) {
		// Parser
		ChunkingImpl chunking = new ChunkingImpl(tagging.characters());
		for (int n = 0; n < tagging.size(); ++n) {
			String tag = tagging.tag(n);
			if (mOutTag.equals(tag))
				continue; // no match tag is B_O or I_O, should correct String tagging  compare with type
			// see below
			if (!tag.startsWith(mBeginTagPrefix)) {
				if (n == 0) {
					String msg = "First tag must be out or begin." + " Found tagging.tag(0)=" + tagging.tag(0);
					throw new IllegalArgumentException(msg);
				}
				String msg = "Illegal tag sequence." + " tagging.tag(" + (n - 1) + ")=" + tagging.tag(n - 1)
						+ " tagging.tag(" + n + ")=" + tagging.tag(n);
				throw new IllegalArgumentException(msg);
			}
			String type = tag.substring(2);
			int start = tagging.tokenStart(n);
			String inTag = mInTagPrefix + type;
			while ((n + 1) < tagging.size() && inTag.equals(tagging.tag(n + 1)))
				++n;
			int end = tagging.tokenEnd(n);
			if (mOutTag.equals(type))
				continue; // comparison with types
			Chunk chunk = ChunkFactory.createChunk(start, end, type);
			chunking.add(chunk);
			Log.o2("Added Chunk with " + tag);
		}
		return chunking;
	}

	/**
	 * Process one sentence.
	 * 
	 * @param sentence string representation of the sentence
	 * @param sentenceI number of the sentence
	 * @param d weights for the document
	 */
	void processSentence(String sentence, int sentenceI, DocumentTagsForWordWithWeights d) {
		StringTagging tagging = getStringTagging(sentence, sentenceI, d);
		ChunkingImpl chunking = getChunking(tagging);
		getHandler().handle(chunking);
	}

	/**
	 * Process one sentence.
	 * 
	 * @param sentence string representation of the sentence
	 * @param sentenceI number of the sentence
	 */
	void processSentence(String sentence, int sentenceI) {
		StringTagging tagging = getStringTagging(sentence, sentenceI);
		ChunkingImpl chunking = getChunking(tagging);
		getHandler().handle(chunking);
	}

	/**
	 * Calculate average weight for a list of weights.
	 * 
	 * @param weigths
	 * @return average weight
	 */
	Double scoreWeigths(HashMap<Integer, String> weigths) {
		int cnt = weigths.size();
		double agg = 0;
		for (String weigth : weigths.values()) {
			double d;
			try {
				d = Double.valueOf(weigth.trim()).doubleValue();
			} catch (NumberFormatException e) {
				System.out.println("NumberFormatException: " + e.getMessage());
				d = 1.0;
			}
			agg += d;
		}

		return (agg / cnt);
	}

	/**
	 * @param predictedConfidence
	 * @return
	 */
	boolean subsampleCondition(double predictedConfidence) {
		// use 0 for a simple test only if conditions still holds
		// predicted confidence = score
		// must be subsampled so that W_i,t "WSubit" is less than expression
		// only a subset (subsampled) is choosen to train, which meets these
		// conditions:
		if (WSubit + predictedConfidence < eSubitPrev * WSubitPrev / eSubit) {
			// WSubit += predictedConfidence; //doing this later
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Converts tagging to a BIO scheme
	 * @param tags
	 * @return string array with tags according to BIO scheme
	 */
	String[] normalizeTags(String[] tags) {
		String[] result = new String[tags.length];
		for (int i = 0; i < tags.length;) {
			if (!(tags[i].equals("TAG"))) { 
				String tag = tags[i];
				result[i] = "B_" + tag;
				++i;
				while (i < tags.length && tags[i].equals(tag))
					result[i++] = "I_" + tag; 
			} else { 
				result[i++] = "O";
			}
		}
		return result;
	}

	/**
	 * getter
	 * @return mycon
	 */
	public DatabaseConnection getMycon() {
		return mycon;
	}

	/**
	 * setter
	 * @param mycon
	 */
	public void setMycon(DatabaseConnection mycon) {
		this.mycon = mycon;
	}

	/**
	 * getter
	 * @return WSubit
	 */
	public double getW() {
		return WSubit;
	}

}
