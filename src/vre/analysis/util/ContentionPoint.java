/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis.util;

import java.util.ArrayList;

/**
 * Representation of a contention point of Co-Testing 
 * (Muslea, Ion, Minton, Steven und Knoblock, Craig A. (2000): Selective
 * Sampling with Redundant Views. In: Proceedings of the Seventeenth
 * National Conference on Artificial Intelligence and Twelfth Conference
 * on on Innovative Applications of Artificial Intelligence, July 30
 * - August 3, 2000, Austin, Texas, USA. Hrsg. von Kautz, Henry A.
 * und Porter, Bruce W. AAAI Press / The MIT Press, S. 621–626.
 * isbn: 0-262-51112-6. url: http://www.aaai.org/Library/AAAI/
 * 2000/aaai00-095.php)
 *
 */
public class ContentionPoint {
	/**
	 * text passage IDs, which are a contention point
	 */
	ArrayList<Integer> tids;
	/**
	 * label of the contention point
	 */
	String label;
	/**
	 * ID of the document, where the contention point is located
	 */
	int docID;
	/**
	 * position line begin
	 */
	int plb;
	/**
	 * position char begin
	 */
	int pcb;
	/**
	 * position line begin
	 */
	int ple;
	/**
	 * position char end
	 */
	int pce;

	/**
	 * getter
	 * @return textpassage IDs 
	 */
	public ArrayList<Integer> getTids() {
		return tids;
	}

	/**
	 * setter
	 * @param tids
	 */
	public void setTids(ArrayList<Integer> tids) {
		this.tids = tids;
	}

	/**
	 * getter
	 * @return label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * setter
	 * @param label
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * getter
	 * @return docID
	 */
	public int getDocID() {
		return docID;
	}

	/**
	 * setter
	 * @param docID
	 */
	public void setDocID(int docID) {
		this.docID = docID;
	}

	/**
	 * getter
	 * @return plb
	 */
	public int getPlb() {
		return plb;
	}

	/**
	 * setter
	 * @param plb
	 */
	public void setPlb(int plb) {
		this.plb = plb;
	}

	/**
	 * getter
	 * @return pcb
	 */
	public int getPcb() {
		return pcb;
	}

	/**
	 * setter
	 * @param pcb
	 */
	public void setPcb(int pcb) {
		this.pcb = pcb;
	}

	/**
	 * getter
	 * @return ple
	 */
	public int getPle() {
		return ple;
	}

	/**
	 * setter
	 * @param ple
	 */
	public void setPle(int ple) {
		this.ple = ple;
	}

	/**
	 * getter
	 * @return pce
	 */
	public int getPce() {
		return pce;
	}

	/**
	 * setter
	 * @param pce
	 */
	public void setPce(int pce) {
		this.pce = pce;
	}

	/**
	 * constructor
	 * 
	 * @param tids
	 * @param label
	 * @param docID
	 * @param plb
	 * @param pcb
	 * @param ple
	 * @param pce
	 */
	public ContentionPoint(ArrayList<Integer> tids, String label, int docID, int plb, int pcb, int ple, int pce) {
		super();
		this.tids = tids;
		this.label = label;
		this.docID = docID;
		this.plb = plb;
		this.pcb = pcb;
		this.ple = ple;
		this.pce = pce;
	}

}
