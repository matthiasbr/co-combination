/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis.util;

import java.util.HashMap;

/**
 * Container for text passages.
 *
 */
public class DBTextpassage {
	/**
	 * name of the textpassage 
	 */
	private String name;
	/**
	 * start position
	 */
	private int[] start;
	/**
	 * end position
	 */
	private int[] end;
	/**
	 * weight representation
	 */
	private String weight;
	/**
	 * other weights assigned to this passage
	 */
	public HashMap<Long, String> otherweights = new HashMap<Long, String>();

	/**
	 * constructor
	 * 
	 * @param name 
	 * @param start
	 * @param end
	 * @param weight
	 */
	public DBTextpassage(String name, int[] start, int[] end, String weight) {
		this.name = name;
		this.start = start;
		this.end = end;
		this.weight = weight;

	}

	/**
	 * constructor
	 * 
	 * @param name
	 * @param start
	 * @param end
	 */
	public DBTextpassage(String name, int[] start, int[] end) {
		this.name = name;
		this.start = start;
		this.end = end;
		this.weight = "1.0";

	}

	/**
	 * constructor
	 */
	public DBTextpassage() {

	}

	/**
	 * getter
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * setter
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * getter
	 * @return start
	 */
	public int[] getStart() {
		return start;
	}

	/**
	 * setter
	 * @param start
	 */
	public void setStart(int[] start) {
		this.start = start;
	}

	/**
	 * getter
	 * @return end
	 */
	public int[] getEnd() {
		return end;
	}

	/**
	 * setter
	 * @param end
	 */
	public void setEnd(int[] end) {
		this.end = end;
	}

	/**
	 * getter
	 * @return weight
	 */
	public String getWeight() {
		return weight;
	}

	/**
	 * setter
	 * @param weight
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}

}
