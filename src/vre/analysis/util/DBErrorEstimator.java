/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis.util;

import static vre.util.Convert.getDoubleFromString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

import com.aliasi.util.Strings;

import vre.analysis.DatabaseConnection;
import vre.analysis.DocumentTagsForWordWithWeights;
import vre.analysis.WeightEntry;
import vre.util.Log;

/**
 * Error estimator of CoForest (Li, Ming und Zhou, Zhi-Hua (2007): 
 *  Improve Computer-Aided Diagnosis With Machine Learning Techniques Using Undiagnosed Samples.
 *  In: Trans. Sys. Man Cyber. Part A 37.6, S. 1088–1098. issn: 1083-
 *  4427. doi: 10.1109/TSMCA.2007.904745) working with the database.
 *
 */
public class DBErrorEstimator {

	/**
	 * document ID
	 */
	private int dID;
	/**
	 * database connection
	 */
	private DatabaseConnection mycon;
	/**
	 * tagging (with score) of the documents 
	 */
	private HashMap<Integer, DocumentTagsForWordWithWeights> dList;

	/**
	 * view ID
	 */
	private int vID;
	/**
	 * hash set of views
	 */
	private HashSet<Integer> views;

	/**
	 * sum of weights
	 */
	private double padder = 0;
	/**
	 * number of weights
	 */
	private int pcount = 0;

	/**
	 * mapping cID to vID
	 */
	private HashMap<Integer, Integer> cIDMapt;

	/**
	 * partition of the data (for evaluation runs)
	 */
	protected int partition;
	/**
	 * level of the index, where the entity names are saved
	 */
	protected int level;

	/**
	 * error estimation of Co-Forest
	 */
	private double estimatederror;

	/**
	 * constructor
	 * @param vID
	 * @param mycon
	 * @param views
	 * @param cIDMapt
	 * @param partition
	 * @param level
	 */
	public DBErrorEstimator(int vID, DatabaseConnection mycon, HashSet<Integer> views,
			HashMap<Integer, Integer> cIDMapt, int partition, int level) {
		super();
		this.vID = vID;
		this.mycon = mycon;
		this.views = views;
		this.cIDMapt = cIDMapt;
		this.partition = partition;
		this.level = level;
	}

	/**
	 * Parsing per category.
	 * @param catid ID of the category
	 * @param ts size of the training set (for evaluation runs)
	 * @param developmentortest is it a development or test run (for evaluation runs)
	 * @return estimated error
	 */
	public double parseCategory(int catid, double ts, String developmentortest) {

		boolean useDev = developmentortest.equals("development");
		boolean useTest = developmentortest.equals("test");
		if (developmentortest.equals("compare")) {
			developmentortest = "compareestimator";
		}
		HashMap<Integer, String> docs = mycon.getDocumentsbymIDcID(0, catid, partition, ts, developmentortest, "cu",
				-1);
		ArrayList<Integer> dIDList = new ArrayList<Integer>();
		for (Integer k : docs.keySet()) {
			dIDList.add(k);
		}

		ArrayList<Integer> cIDList = new ArrayList<Integer>();

		for (int view : views) {

			int cID = -1;
			try {
				cID = cIDMapt.get(view);
			} catch (Exception e) {
				Log.o2("NOT FOUND CATEGORY ID for " + view);
			}
			;
			if (cID == -1)
				continue;
			cIDList.add(cID);
		}

		if (dIDList.size() == 0) {
			return 0;
		}

		dList = mycon.getDocumentTagsForWordWithWeights(cIDList, dIDList, 0, level, "cu", "");

		padder = 0;
		pcount = 0;

		for (Entry<Integer, String> doc : docs.entrySet()) {
			String in = doc.getValue();
			dID = doc.getKey();
			parseString(in.toCharArray(), 0, in.length());
		}

		return (padder / pcount);

	}

	/**
	 * Parses the input string.
	 *
	 * @param cs    character array representing the string
	 * @param start first char
	 * @param end   last char
	 */
	public void parseString(char[] cs, int start, int end) {
		String in = new String(cs, start, end - start);

		DocumentTagsForWordWithWeights d = dList.get(dID); 
		if (d != null) {
			processSentences(in, d);
		}
		Log.o2n(".");
	}

	/**
	 * Process the sentences of the string.
	 * 
	 * @param in sentence string
	 * @param d weights for the document
	 */
	void processSentences(String in, DocumentTagsForWordWithWeights d) {

		// part 1
		String[] sentences = in.split("\n");
		for (int si = 0; si < sentences.length; ++si) {
			if (Strings.allWhitespace(sentences[si]))
				continue;
			String sentence = sentences[si];
			String[] tagTokens = sentence.split(" ");
			String[] tokens = new String[tagTokens.length];
			String[] tags = new String[tagTokens.length];
			String[] scores = new String[tagTokens.length];
			Log.o2(" in:: " + sentence);

			int startOfToken = 0;

			// create tagging directly from Database TODO separate Database Access therefore
			// the step of creating a apropriate string representation is kept
			for (int i = 0; i < tagTokens.length; ++i) {
				Log.o2(tagTokens[i]);
				tokens[i] = tagTokens[i];

				int endOfToken = sentence.indexOf(32, startOfToken);

				if (endOfToken == -1) {
					endOfToken = sentence.length();
				}

				/*
				 * Breiman, Leo (2001): Random Forests. In: Machine Learning 45.1, S. 5–32. doi: 10.1023/A:1010933404324
				 */

				String ownTag = "NONE";
				Double ownScore = 0.0;
				ArrayList<String> othersTag = new ArrayList<String>();
				ArrayList<Double> othersScore = new ArrayList<Double>();

				// +1 tk widget counting

				ArrayList<WeightEntry> weList = d.getWeightEntryKeys(si + 1, startOfToken, si + 1, endOfToken);

				int cID = -1;
				try {
					cID = cIDMapt.get(vID);
				} catch (Exception e) {
					Log.o2(tagTokens[i] + " View " + vID + "not FOUND!");
					// System.exit(1);
				}
				;
				if (cID == -1)
					continue;

				// estimate over new category, all tags belong to user initiating algorithm
				// alternative: give ownership according to classifier
				for (WeightEntry we : weList) {

					int cuser = we.getUserw();
					String ctag = we.getName();
					String cweight = we.getWeight();
					double cscore = getDoubleFromString(cweight);
					Log.o2(tagTokens[i] + " EE" + cuser + " cVIEW: " + cID + " cTAG: " + ctag + ":" + cscore);

					// if (view == vID) {
					if (cID == cuser) {
						ownTag = ctag;
						ownScore = cscore;
					} else {
						othersTag.add(ctag);
						othersScore.add(cscore);
					}

					break; // here only first is chosen // more possible but not easy applicalble to user
				}

				// margin function

				int kagree = 0;
				int kdisagree = 0;
				int sizeOfOthersTag = othersTag.size();
				double p = 0;

				if (sizeOfOthersTag != 0) {

					for (int j = 0; j < sizeOfOthersTag; j++) {
						if (ownTag == othersTag.get(j))
							kagree++;
						else
							kdisagree++;
						// ++ makes indicator function average is not implemeted jet combine with score
					}

					double avgkagree = kagree / sizeOfOthersTag;
					double avgkdisagree = kdisagree / sizeOfOthersTag;

					p = avgkagree - avgkdisagree;

				}

				// to calculate probability margin (mg) < 0
				pcount++;
				if (p < 0) {
					padder++;
				}

				startOfToken += tagTokens[i].length() + 1;
			}
		}
	}

	/**
	 * Process one sentence.
	 * @param sentence string representation of the sentence
	 * @param sentenceI number of the sentence
	 */
	void processSentence(String sentence, int sentenceI) {
		String[] tagTokens = sentence.split(" ");
		String[] tokens = new String[tagTokens.length];
		String[] tags = new String[tagTokens.length];
		String[] scores = new String[tagTokens.length];
		Log.o2(" in:: " + sentence);

		int startOfToken = 0;

		// create tagging directly from Database TODO separate database access therefore
		// the step of creating a appropriate string representation is kept
		for (int i = 0; i < tagTokens.length; ++i) {
			tokens[i] = tagTokens[i];

			int endOfToken = sentence.indexOf(32, startOfToken);

			if (endOfToken == -1) {
				endOfToken = sentence.length();
			}

			// System.out.println("Token: " + tokens[i]);

			/*
			 * Breiman, Leo (2001): Random Forests. In: Machine Learning 45.1, S. 5–32. doi: 10.1023/A:1010933404324
			 */

			String ownTag = "NONE";
			Double ownScore = 0.0;
			ArrayList<String> othersTag = new ArrayList<String>();
			ArrayList<Double> othersScore = new ArrayList<Double>();

			for (int view : views) {

				int cID = -1;
				try {
					cID = cIDMapt.get(view);
				} catch (Exception e) {

				}
				;
				if (cID == -1)
					continue;
				// estimate over new category, all tags belong to user initiating algorithm
				// alternative: give ownership according to classifier
				HashMap<String, HashMap<Integer, String>> TagsForWordWithWeigths = mycon.getTagsForWordWithWeights(cID,
						dID, sentenceI, startOfToken, endOfToken, 0, 0);

				for (Entry<String, HashMap<Integer, String>> tagwithweigthsentry : TagsForWordWithWeigths.entrySet()) {
					String tag = tagwithweigthsentry.getKey();
					Double score = scoreWeigths(tagwithweigthsentry.getValue());

					if (view == vID) {
						ownTag = tag;
						ownScore = score;
					} else {
						othersTag.add(tag);
						othersScore.add(score);
					}

					break; // here only first is chosen // more possible but not easy applicalble to user
				}

			}

			// margin function

			int kagree = 0;
			int kdisagree = 0;
			int sizeOfOthersTag = othersTag.size();
			double p = 0;

			if (sizeOfOthersTag != 0) {

				for (int j = 0; j < sizeOfOthersTag; j++) {
					if (ownTag == othersTag.get(j))
						kagree++;
					else
						kdisagree++;
					// ++ makes indicator function average, is not implemented jet, combine with score
				}

				double avgkagree = kagree / sizeOfOthersTag;
				double avgkdisagree = kdisagree / sizeOfOthersTag;

				p = avgkagree - avgkdisagree;

			}

			// to calculate probability margin (mg) < 0
			pcount++;
			if (p < 0) {
				padder++;
			}

			startOfToken += tagTokens[i].length() + 1;

		}

	}

	/**
	 * Calculate average weight for a list of weights.
	 * 
	 * @param weigths
	 * @return average weight
	 */
	Double scoreWeigths(HashMap<Integer, String> weigths) {
		int cnt = weigths.size();
		double agg = 0;
		for (String weigth : weigths.values()) {
			double d;
			try {
				d = Double.valueOf(weigth.trim()).doubleValue();
			} catch (NumberFormatException e) {
				System.out.println("NumberFormatException: " + e.getMessage());
				d = 1.0;
			}
			agg += d;
		}

		return (agg / cnt);
	}

	/**
	 * Converts tagging to a BIO scheme
	 * @param tags
	 * @return string array with tags according to BIO scheme
	 */
	String[] normalizeTags(String[] tags) {
		String[] result = new String[tags.length];
		for (int i = 0; i < tags.length;) {
			if (!(tags[i].equals("TAG"))) { 
				String tag = tags[i];
				result[i] = "B_" + tag;
				++i;
				while (i < tags.length && tags[i].equals(tag))
					result[i++] = "I_" + tag; 
			} else { 
				result[i++] = "O";
			}
		}
		return result;
	}

}
