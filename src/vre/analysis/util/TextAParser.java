/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis.util;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.ChunkFactory;
import com.aliasi.chunk.Chunking;
import com.aliasi.chunk.ChunkingImpl;
import com.aliasi.corpus.ObjectHandler;
import com.aliasi.tag.StringTagging;

import vre.util.Log;

/**
 * Parses text strings.
 *
 */
public class TextAParser extends ExtendedStringParser<ObjectHandler<Chunking>> {

	/**
	 * BIO scheme begin tag
	 */
	private final String mBeginTagPrefix = "B-";
	/**
	 * BIO scheme inner tag
	 */
	private final String mInTagPrefix = "I-";
	/**
	 * BIO scheme out tag
	 */
	private final String mOutTag = "O";

	/**
	 * title of the text
	 */
	String title = "";
	/**
	 * date of the text
	 */
	String date = "";
	/**
	 * list of tokens
	 */
	ArrayList<String> tokenList = new ArrayList<String>();
	/**
	 * list of tags
	 */
	ArrayList<String> tagList = new ArrayList<String>();

	/**
	 * constructor
	 */
	public TextAParser() {
		super();
	}

	/**
	 * Method for parsing the text.
	 * @param path path to file with text
	 */
	public void parseText(String path) {

		// Stream to read file
		FileInputStream fin;
		DataInputStream din;

		title = "";
		date = "";
		tokenList = new ArrayList<String>();
		tagList = new ArrayList<String>();

		// Open an input stream
		try {
			fin = new FileInputStream(path);

			din = new DataInputStream(fin);

			// Read a line of text
			while (din.available() > 0) {
				String line = din.readLine();
				parseLine(line);

			}

			// Close our input stream
			fin.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Parses one line of text.
	 * @param line string containing the line
	 */
	public void parseLine(String line) {
		String linec[] = line.split("\t");
		if (linec[0].equals("#")) {
			title = linec[1];
			date = linec[2];
			tokenList = new ArrayList<String>();
			tagList = new ArrayList<String>();
		} else {

			if (!linec[0].equals("")) {
				Integer i = Integer.parseInt(linec[0]);
				tokenList.add(linec[1]);
				tagList.add(linec[2]);
			} else {
				// End of Sentence
				if (tokenList != null) {

					String[] tokens = new String[tokenList.size()];
					tokens = tokenList.toArray(tokens);
					String[] tags = new String[tagList.size()];
					tags = tagList.toArray(tags);
					String[] scores = new String[tagList.size()];
					for (int j = 0; j < scores.length; j++) {
						scores[j] = "1";
					}
					processSentence(tokens, tags, scores);
					tokenList = null;
					tagList = null;

				}
			}
		}

	}

	/**
	 * Parses the input string.
	 *
	 * @param cs    character array representing the string
	 * @param start first char
	 * @param end   last char
	 */
	@Override
	public void parseString(char[] cs, int start, int end) {
		String in = new String(cs, start, end - start);
		String[] lines = in.split("\n");
		for (int i = 0; i < lines.length; ++i) {
			parseLine(lines[i]);
		}
	}

	/**
	 * Processes one sentence.
	 * @param tokens tokens
	 * @param tags tags
	 * @param scores scores for the tags
	 */
	void processSentence(String[] tokens, String[] tags, String[] scores) {
		Log.o2(" in:: " + tokens.toString());

		int[] tokenStarts = new int[tokens.length];
		int[] tokenEnds = new int[tokens.length];
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < tokens.length; ++i) {
			tokenStarts[i] = sb.length();
			sb.append(tokens[i]);
			tokenEnds[i] = sb.length();
			if (i + 1 < tokens.length)
				sb.append(' '); // single space separator except at end
		}
		StringTagging tagging = new StringTagging(Arrays.asList(tokens), Arrays.asList(tags), sb.toString(),
				tokenStarts, tokenEnds);

		ChunkingImpl chunking = new ChunkingImpl(tagging.characters());
		for (int n = 0; n < tagging.size(); ++n) {
			String tag = tagging.tag(n);
			if (mOutTag.equals(tag))
				continue; // no match tag is B_O or I_O, should correct String tagging, compare with type
			// see below
			if (!tag.startsWith(mBeginTagPrefix)) {
				if (n == 0) {
					String msg = "First tag must be out or begin." + " Found tagging.tag(0)=" + tagging.tag(0);
					throw new IllegalArgumentException(msg);
				}
				String msg = "Illegal tag sequence." + " tagging.tag(" + (n - 1) + ")=" + tagging.tag(n - 1)
						+ " tagging.tag(" + n + ")=" + tagging.tag(n);
				throw new IllegalArgumentException(msg);
			}
			String type = tag.substring(2);
			int start = tagging.tokenStart(n);
			String inTag = mInTagPrefix + type;
			while ((n + 1) < tagging.size() && inTag.equals(tagging.tag(n + 1)))
				++n;
			int end = tagging.tokenEnd(n);
			if (mOutTag.equals(type))
				continue; // comparison with types
			Chunk chunk = ChunkFactory.createChunk(start, end, type);
			chunking.add(chunk);
			Log.o2("Added Chunk with " + tag);
		}

		getHandler().handle(chunking);
	}

	/**
	 * Calculates average score.
	 * 
	 * @param weigths weights of the tokens
	 * @return average score
	 */
	Double scoreWeigths(HashMap<Integer, String> weigths) {
		int cnt = weigths.size();
		double agg = 0;
		for (String weigth : weigths.values()) {
			double d;
			try {
				d = Double.valueOf(weigth.trim()).doubleValue();
			} catch (NumberFormatException e) {
				System.out.println("NumberFormatException: " + e.getMessage());
				d = 1.0;
			}
			agg += d;
		}

		return (agg / cnt);
	}

}
