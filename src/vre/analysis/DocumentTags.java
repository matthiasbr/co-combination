/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

/**
 * Contains the tags of the document.
 *
 */
public class DocumentTags {
	/**
	 * document ID
	 */
	Integer ID;
	/**
	 * document text
	 */
	String text;
	/**
	 * tagging (with score)
	 */
	DocumentTagsForWordWithWeights d;

	/**
	 * getter
	 * @return ID
	 */
	public Integer getID() {
		return ID;
	}

	/**
	 * setter
	 * @param id
	 */
	public void setID(Integer id) {
		ID = id;
	}

	/**
	 * getter
	 * @return text
	 */
	public String getText() {
		return text;
	}

	/**
	 * setter
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * getter
	 * @return d
	 */
	public DocumentTagsForWordWithWeights getD() {
		return d;
	}

	/**
	 * setter
	 * @param d
	 */
	public void setD(DocumentTagsForWordWithWeights d) {
		this.d = d;
	}

	/**
	 * constructor
	 * 
	 * @param id
	 * @param text
	 * @param d
	 */
	public DocumentTags(Integer id, String text, DocumentTagsForWordWithWeights d) {
		super();
		ID = id;
		this.text = text;
		this.d = d;
	}

}
