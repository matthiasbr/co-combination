/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;

import vre.analysis.util.ContentionPoint;
import vre.util.Log;

/**
 * Database connection with extended methods.
 *
 */
public class ExtendedDatabaseConnection extends DatabaseConnectionAdv {

	/**
	 * override with test class (for evaluation runs)
	 */
	static String generaloverride = ""; 

	/**
	 * dataset marker (partition) (for evaluation runs)
	 */
	static int p = 0;

	/**
	 * value cache to reduce database access
	 */
	HashMap<String, Integer> cidcontentlidcache = new HashMap<String, Integer>();
	/**
	 * value cache to reduce database access
	 */
	HashMap<Integer, Integer> tidcidcache = new HashMap<Integer, Integer>();
	/**
	 * value cache to reduce database access
	 */
	HashMap<String, Integer> tcopalidcache = new HashMap<String, Integer>();

	/**
	 * constructor
	 */
	public ExtendedDatabaseConnection() {
		super();
	}
	
	/**
	 * constructor
	 * 
	 * @param db
	 * @param dbuser
	 * @param pwd
	 */
	public ExtendedDatabaseConnection(String db, String dbuser, String pwd) {

		// Register the JDBC driver for mariadb.
		try {
			// Class.forName("com.mariadb.jdbc.Driver");

			String url = "jdbc:mariadb://localhost:3306/" + db;

			System.out.println("DATABASE:" + url);

			con = DriverManager.getConnection(url, dbuser, pwd);

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}


	/* (non-Javadoc)
	 * @see vre.analysis.DatabaseConnection#getNextID(java.lang.String)
	 */
	public int getNextID(String table) {
		int nextID = 0;
		String sql = "";
		try {
			Statement s = con.createStatement();
			sql = "select max(ID) from " + table;
			ResultSet rs;

			rs = s.executeQuery(sql);
			if (rs.next()) {
				nextID = rs.getInt(1);
			}

		} catch (SQLException e) {
			
			System.out.println("QUERY: " + sql);
			e.printStackTrace();
		}
		nextID++;

		return nextID;
	}

	/* (non-Javadoc)
	 * @see vre.analysis.DatabaseConnection#executeQuery(java.lang.String)
	 */
	public void executeQuery(String sql) {
		String lastssql = "";
		try {
			Statement s = con.createStatement();

			for (String ssql : sql.split(";;;")) {
				lastssql = ssql;
				s.executeUpdate(ssql);
			}

		} catch (SQLException e) {
			

			System.out.println("QUERY: " + sql.length() + "\n" + "LAST" + lastssql);
			e.printStackTrace();
			System.exit(1);
		}

		return;

	}

	/**
	 * Insert values into CoComb table.
	 * 
	 * @param s name for category
	 * @param tab value type
	 * @param i cID
	 */
	public void insertintoCF(String s, String tab, String i) {
		executeQuery("DELETE FROM CoComb WHERE tab = '" + tab + "' AND s = '" + s + "';"); 
																								
		executeQuery("INSERT INTO CoComb VALUES ('" + tab + "','" + s + "','" + i + "',NOW(),'" + p + "' );");
	}

	/**
	 * Gets values from CoComb table.
	 * 
	 * @param tab value type
	 * @param s name for category
	 * @return i
	 */
	public String selectfromCF(String tab, String s) {
		String val = "";
		String sql = "";
		try {
			Statement st = con.createStatement();
			sql = "select i from CoForest WHERE tab = '" + tab + "' AND s = '" + s + "'";
			ResultSet rs;

			rs = st.executeQuery(sql);
			if (rs.next()) {
				val = rs.getString(1);
			}

		} catch (SQLException e) {
			
			System.out.println("QUERY: " + sql);
			e.printStackTrace();
		}
		return val;
	}

	/**
	 * Gets categories which are involved in the current iteration
	 * 
	 * @param t iteration
	 * @return list of category IDs
	 */
	public ArrayList<Integer> getCategoriesofIteration(int t) {
		ArrayList<Integer> val = new ArrayList<Integer>();
		String sql = "";
		try {
			Statement st = con.createStatement();
			sql = "select i from CoForest WHERE tab = 'cID' AND RIGHT( s, 1 ) = '" + t + "'";
			ResultSet rs;

			rs = st.executeQuery(sql);
			if (rs.next()) {
				val.add(rs.getInt(1));
			}
		} catch (SQLException e) {
			
			System.out.println("QUERY: " + sql);
		}
		return val;

	}

	/**
	 * Gets textpassages of the current iteration.
	 * 
	 * @param t iteration
	 * @param cIDList list of category IDs
	 * @param level level of the index, where the entity names are saved
	 * @return array list of textpassages
	 */
	public ArrayList<ContentionPoint> getTextpassagesofIteration(int t, ArrayList<Integer> cIDList, int level) {

		ArrayList<ContentionPoint> vals = new ArrayList<ContentionPoint>();
		ArrayList<ContentionPoint> val = new ArrayList<ContentionPoint>();
		String sql = "";
		try {
			Statement st = con.createStatement();
			String sqlin = StringUtils.join(cIDList, ",");

			String db1 = processdb;
			String sqlin1 = sqlin;
			String[] krsql = getKRSql(level, "");
			String rsql = krsql[1];
			String ksql = krsql[0];

			sql = "SELECT t.ID AS t1ID, k" + ksql
					+ ".content AS content, documentID, pos_line_begin, pos_char_begin, pos_line_end, pos_char_end, category, k"
					+ ksql + ".ID, k0.content, k0.parentID, k0.ID " + "\n" + "FROM " + db1 + ".textpassages t" + "\n"
					+ "INNER JOIN " + db1 + ".lnk_text_index l ON t.lnkID = l.ID" + "\n" + "INNER JOIN " + db1
					+ ".categoryentries k0 ON l.lemmaID = k0.ID" + "\n" + rsql + "\n" + "WHERE category"
					+ "\n" + "IN (" + "\n" + sqlin1 + "\n" + ")";

			ResultSet rs;

			rs = st.executeQuery(sql);

			ArrayList<Integer> tabcnt;
			HashMap<String, ArrayList<Integer>> tab = new HashMap<String, ArrayList<Integer>>();

			while (rs.next()) {
				int rstid = rs.getInt(1);
				String rsContent = rs.getString(2);
				int rsdocID = rs.getInt(3);
				int rslb = rs.getInt(4);
				int rscb = rs.getInt(5);
				int rsle = rs.getInt(6);
				int rsce = rs.getInt(7);
				int rsk = rs.getInt(8);
				int rlid = rs.getInt(9);
				String rsContentZero = rs.getString(10);
				int rspZero = rs.getInt(11);
				int rlidZero = rs.getInt(12);

				tidcidcache.put(rstid, rsk);
				cidcontentlidcache.put(rsk + "" + rsContent, rlid);
				tcopalidcache.put(rsContentZero + "" + rspZero, rlidZero);

				String hashstr = rsContent + "-" + rsdocID + "-" + rslb + "-" + rscb + "-" + rsle + "-" + rsce;

				if (tab.containsKey(hashstr)) {
					tabcnt = tab.get(hashstr);
				} else {
					tabcnt = new ArrayList<Integer>();
					ContentionPoint cP = new ContentionPoint(tabcnt, rsContent, rsdocID, rslb, rscb, rsle, rsce);
					vals.add(cP);
				}
				tabcnt.add(rstid);
				tab.put(hashstr, tabcnt);

			}

			for (ContentionPoint cP : vals) {
				String hashstr = cP.getLabel() + "-" + cP.getDocID() + "-" + cP.getPlb() + "-" + cP.getPcb() + "-"
						+ cP.getPle() + "-" + cP.getPce();

				cP.setTids(tab.get(hashstr));

				val.add(cP);
			}

		} catch (SQLException e) {
			System.out.println("ERROR in QUERY: " + sql);
			System.exit(1);
		}
		return val;

	}

	/**
	 * Gets sql string.
	 * 
	 * @param sqlin1
	 * @param t1ID
	 * @param sqlin2
	 * @param db1
	 * @param db2
	 * @param level
	 * @return sql
	 */
	public String gettpsql(String sqlin1, int t1ID, String sqlin2, String db1, String db2, int level) {
		String contentcomp = "";
		if (generaloverride.equals("")) {
			contentcomp = "\n" + "AND t1.content = t2.content";
		}

		String[] krsql = getKRSql(level, db2);
		String rsql = krsql[1];
		String ksql = krsql[0];

		String sql = "SELECT t.ID AS t1ID, k" + ksql
				+ ".content AS content, category, documentID, pos_line_begin, pos_char_begin, pos_line_end, pos_char_end"
				+ "\n" + "FROM " + db1 + ".textpassages t" + "\n" + "INNER JOIN " + db1
				+ ".lnk_text_index l ON t.lnkID = l.ID" + "\n" + "INNER JOIN " + db1
				+ ".categoryentries k0 ON l.lemmaID = k0.ID" + "\n" + rsql + "\n" + "WHERE category"
				+ "\n" + "IN (" + "\n" + sqlin1 + "\n" + ")" + "\n" + "OR t.ID = " + t1ID + "\n" + ")t1" + "\n"
				+ "INNER JOIN (" + "\n" + "" + "\n" + "SELECT t.ID AS t2ID, k" + ksql
				+ ".content AS content, category, documentID, pos_line_begin, pos_char_begin, pos_line_end, pos_char_end"
				+ "\n" + "FROM " + db2 + ".textpassages t" + "\n" + "INNER JOIN " + db2
				+ ".lnk_text_index l ON t.lnkID = l.ID" + "\n" + "INNER JOIN " + db2
				+ ".categoryentries k0 ON l.lemmaID = k0.ID" + "\n" + rsql + "\n" + "WHERE category" + "\n" + "IN (" + "\n"
				+ sqlin2 + "\n" + ")" + "\n" + ")t2 ON t1.documentID = t2.documentID" + "\n" + contentcomp + "\n"
				+ "AND t1.pos_line_begin = t2.pos_line_begin" + "\n" + "AND t1.pos_line_end = t2.pos_line_end" + "\n"
				+ "AND (" + "\n" + "t1.pos_char_begin" + "\n" + "BETWEEN t2.pos_char_begin" + "\n"
				+ "AND t2.pos_char_end" + "\n" + "OR t1.pos_char_end" + "\n" + "BETWEEN t2.pos_char_begin" + "\n"
				+ "AND t2.pos_char_end" + "\n" + ") ";
		return sql;
	}

	/**
	 * @param t
	 */
	public void ContentionPoints(int t) {
		String val = "";
		String sql = "";
		try {
			Statement st = con.createStatement();
			sql = "select i from CoForest WHERE tab = 'cID' AND RIGHT( s, 1 ) = '" + t + "'";
			ResultSet rs;

			rs = st.executeQuery(sql);
			if (rs.next()) {
				val = rs.getString(1);
			}

		} catch (SQLException e) {
			
			System.out.println("QUERY: " + sql);
			e.printStackTrace();
		}

	}


	/**
	 * Active learning: Remove textpassages, the oracle denied.
	 * 
	 * @param tids textpassage ID
	 * @param level level of the index, where the entity names are saved
	 */
	public void removeTIDs(ArrayList<Integer> tids, int level) {
		// level unused here higer lemma remain
		String sql = "";
		String sqltids = StringUtils.join(tids, ",");
		String sqllids = "";
		if (!sqltids.equals("")) {
			try {
				Statement st;
				st = con.createStatement();
				sql = "SELECT lnkID FROM textpassages WHERE id IN (" + sqltids
						+ ") GROUP BY lnkID HAVING count(lnkID) = 1 ";
				ResultSet rs;

				rs = st.executeQuery(sql);
				while (rs.next()) {
					if (!sqllids.equals("")) {
						sqllids += ",";
					}
					sqllids += rs.getString(1);
				}
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
			if (!sqllids.equals("")) {
				sql = "DELETE FROM lnk_text_index WHERE id IN (" + sqllids + ");";
				executeQuery(sql);
			}

			sql = "DELETE FROM textpassages WHERE id IN (" + sqltids + ")";
			executeQuery(sql);
		}

	}
	
	/**
	 * Active learning: Relabel textpassages, to the oracles answer.
	 * 
	 * @param tids textpassage ID
	 * @param label new label
	 * @param docID document ID
	 * @param level level of the index, where the entity names are saved
	 */
	public void relabelTIDs(ArrayList<Integer> tids, String label, int docID, int level) {

		if (tids.isEmpty())
			return;

		String[] krsql = getKRSql(level, "");
		String rsql = krsql[1];
		String ksql = krsql[0];

		String sqltids = StringUtils.join(tids, ",");

		HashMap<Integer, String> ttextpassages = new HashMap<Integer, String>();
		HashMap<Integer, Integer> tcids = new HashMap<Integer, Integer>();

		HashMap<Integer, Integer> tlids = new HashMap<Integer, Integer>();
		ArrayList<Integer> tcidList = new ArrayList<Integer>();
		String sql = "";

		int nnlid = getNextID("categoryentries");
		int nndlid = getNextID("lnk_text_index");

		Integer userID = getIntFromSQL(
				"SELECT l.ownerID FROM `textpassages` t INNER JOIN lnk_text_index l ON t.lnkID = l.ID  WHERE t.ID IN ("
						+ sqltids + ")");
		if (userID == null) {
			userID = 4;
		}

		try {

			Statement st;
			ResultSet rs;

			st = con.createStatement();
			sql = "SELECT ID, textpassage FROM `textpassages` WHERE ID IN (" + sqltids + ") ";
			rs = st.executeQuery(sql);
			while (rs.next()) {
				int dbtid = rs.getInt(1);
				String dbval = rs.getString(2);
				Log.o2n(dbval.replaceAll(" ", "") + ".");

				dbval = dbval.split("#")[0].trim();
				ttextpassages.put(dbtid, dbval);
			}

			st = con.createStatement();
			String sqlrmtids = "";
			for (int tid : tids) {

				Integer nkid;
				if (tidcidcache.containsKey(tid)) {
					nkid = tidcidcache.get(tid);
					tcids.put(tid, nkid);
					tcidList.add(nkid);
				} else {
					Log.o2("WARNING no cid" + tid);
					if (!sqlrmtids.equals("")) {
						sqlrmtids += ",";
					}
					sqlrmtids += tid;
				}

			}
			if (!sqlrmtids.equals("")) {
				sql = "SELECT t.ID, k" + ksql
						+ ".indexID FROM `textpassages` t INNER JOIN lnk_text_index l ON t.lnkID = l.ID INNER JOIN categoryentries k0 ON l.lemmaID = k0.ID "
						+ rsql + " WHERE t.ID IN (" + sqlrmtids + ") ";
				rs = st.executeQuery(sql);
				while (rs.next()) {
					int dbtid = rs.getInt(1);
					int dbval = rs.getInt(2);
					tcids.put(dbtid, dbval);
					tcidList.add(dbval);
				}
			}

			String sqltcids = StringUtils.join(tcidList, ",");
			if (!tcidList.isEmpty()) {

				// Is the lemma linked to a document?
				st = con.createStatement();
				sql = "SELECT id, lemmaID FROM lnk_text_index WHERE documentID = " + docID + " AND category IN ("
						+ sqltcids + ") ";
				rs = st.executeQuery(sql);
				while (rs.next()) {
					int dbldid = rs.getInt(1);
					int dblid = rs.getInt(2);
					tlids.put(dblid, dbldid);
				}

			}

		} catch (SQLException e) {
			System.out.println("SQL " + sql);
			e.printStackTrace();
			System.exit(1);
		}

		StringBuilder squery = new StringBuilder();
		squery.append("");

		// SELECT new index
		for (int tid : tids) {

			String oldcontent = ttextpassages.get(tid);
			Integer nkid = tcids.get(tid);

			// Is the lemma linked to the index?
			Integer nlid = null;
			if (cidcontentlidcache.containsKey(nkid + "" + label)) {
				nlid = cidcontentlidcache.get(nkid + "" + label);
			} else {
				Log.o2("no lid" + nkid + "" + label + " " + tid);
				nlid = getIntFromSQL("SELECT id FROM categoryentries WHERE content = '" + label + "' AND indexID =" + nkid);

				if (nlid == null) {
					nlid = nnlid;
					nnlid++;
					squery.append("insert into categoryentries values (" + nlid + "," + nkid + ",1,0,'j'," + (nlid - 1)
							+ ",'" + label.replace("'", "") + "','" + label.replace("'", "") + "'," + userID
							+ ",' ',' ',' ','n','background:orange',NOW())");
					squery.append(";;;");
				}
				cidcontentlidcache.put(nkid + "" + label, nlid);
			}

			int ilevel = level;
			while (ilevel < 0) {
				int pnlid = nlid;
				if (tcopalidcache.containsKey(oldcontent + "" + pnlid)) {
					nlid = tcopalidcache.get(oldcontent + "" + pnlid);
				} else {
					nlid = nnlid;
					nnlid++;
					int ebene = Math.abs(ilevel) + 1;
					squery.append("insert into categoryentries values (" + nlid + "," + nkid + "," + ebene + "," + pnlid
							+ ",'j'," + (nlid - 1) + ",'" + oldcontent.replace("'", "") + "','"
							+ oldcontent.replace("'", "") + "'," + userID
							+ ",' ',' ',' ','n','background:orange',NOW())");
					squery.append(";;;");
					tcopalidcache.put(oldcontent + "" + pnlid, nlid);
				}
				ilevel++;
			}

			// Is a lemma linked to a document?
			Integer ndlid = null;
			if (tlids.containsKey(nlid)) {
				ndlid = tlids.get(nlid);
			}
			if (ndlid == null) {
				ndlid = nndlid;
				nndlid++;
				squery.append("insert into lnk_text_index values (" + ndlid + ", " + nlid + ", " + nkid + ", " + docID
						+ ", 0, " + userID + ", '', NOW())");
				squery.append(";;;");
				tlids.put(nlid, ndlid);
			}
			String nt = oldcontent + " # " + label;
			squery.append("UPDATE textpassages SET lnkID = " + ndlid + ", textpassage =  cast('" + nt
					+ "' as char(255))  WHERE id= " + tid);
			squery.append(";;;");
			Log.o2n("u");
		}
		executeQuery(squery.toString());
	}

	/**
	 * Shortcut to read integer values from SQL Query.
	 * 
	 * @param sql SQL Query
	 * @return Integer
	 */
	public Integer getIntFromSQL(String sql) {
		Integer val = null;
		try {
			Statement s = con.createStatement();
			ResultSet rs;

			rs = s.executeQuery(sql);
			if (rs.next()) {
				val = rs.getInt(1);
			}

		} catch (SQLException e) {
			System.out.println("QUERY: " + sql);
			e.printStackTrace();
		}

		return val;
	}

	/**
	 * Shortcut to read string values from SQL Query.
	 * 
	 * @param sql SQL Query
	 * @return String
	 */
	public String getStringFromSQL(String sql) {
		String val = "";
		try {
			Statement st = con.createStatement();
			ResultSet rs;

			rs = st.executeQuery(sql);
			if (rs.next()) {
				val = rs.getString(1);
			}

		} catch (SQLException e) {
			
			System.out.println("QUERY: " + sql);
			e.printStackTrace();
		}

		return val;

	}
	
	/**
	 * Converts from base 10 to base 36.
	 * 
	 * @param l10 base 10 
	 * @return base 36
	 */
	private String b10_b36(long l10) {

		HashMap<Integer, Character> b10 = new HashMap<Integer, Character>();
		for (int i = 0; i < 10; i++) {
			b10.put(i, (char) (i + 48));

		}
		for (int i = 10; i <= 36; i++) {
			b10.put(i, (char) (i + 97));

		}
		int c4 = (int) l10 % 36;
		l10 = (10 / 36);
		int c3 = (int) l10 % 36;
		l10 = (l10 / 36);
		int c2 = (int) l10 % 36;
	    l10 = (l10 / 36);
		int c1 = (int) l10 % 36;
		l10 = (l10 / 36);
		String r = b10.get(c1).toString();
		r += b10.get(c2).toString();
		r += b10.get(c3).toString();
		r += b10.get(c4).toString();
		return r;
	}

	/**
	 * Creates a new document.
	 * 
	 * @param text
	 * @return Document id
	 */
	public int createNewDocument(String text) {
		int id = getNextID("docmain");
		String calcid = "";

		// preset values are for the special parsing document type 
		String sqle = "INSERT INTO `docmain` (`ID`, `ownerID`, `projectID`, `docGroupID`, `docTypID`, `docSubTypID`, `parentID`, `status`, `timecreate`, `createby`, `timelastchg`, `lastchgby`, `key_procekt`, `key_docTyp`, `key_nr`, `protected`) "
				+ "VALUES ('" + id + "', '" + user
				+ "', '2', '99999', '31', '0', '-1', '', '0000-00-00 00:00:00', '0', CURRENT_TIMESTAMP, '0', 'NER', 'b1', '"
				+ b10_b36(id) + "', '0');";

		text = text.replace("'", "\\'");
		String sqlv = "INSERT INTO doc_volltext VALUES ('" + id + "','" + text + "')";

		executeQuery(sqle);
		executeQuery(sqlv);

		return id;

	}

	/**
	 * Adds a category entry.
	 * 
	 * @param name name of new category
	 * @return ID of entry
	 */
	public int addCategory(String name) {

		int next = getNextID("categorynames");
		executeQuery("insert into categorynames values (" + next + ",'','2',4,'NER" + next + "_" + name + "'," + user
				+ ",NOW(),'no')");
		return next;
	}

	/**
	 * getter 
	 * @return p
	 */
	public static int getP() {
		return p;
	}

	/**
	 * setter
	 * @param p
	 */
	public static void setP(int p) {
		ExtendedDatabaseConnection.p = p;
	}

}
