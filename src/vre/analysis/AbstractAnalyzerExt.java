/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.analysis;

import static vre.util.Convert.getPosFromNumber;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.aliasi.chunk.Chunk;
import com.aliasi.chunk.ConfidenceChunker;

import vre.analysis.util.DBTextpassage;
import vre.util.Log;

/**
 * Abstract class to build analyzers.
 *
 */
public abstract class AbstractAnalyzerExt extends AbstractAnalyzer {


	private static final int s = 1024; 
	/**
	 * are tokens separated by whitespace or already array
	 */
	private static boolean tokeninzewhitespace = true;

	/**
	 * split text in word groups, preferable sentences
	 * @param text input text
	 * @param m position
	 * @return string
	 */
	private static int nextWordGroup(String text, int m) {

		int r = text.indexOf(".", m + 2);

		// prevent small sentences
		if ((r - m) < s / 2 - 1) {
			Log.o2("SMALL");
			r = text.indexOf(".", m + s / 2 - 1);
		}
		// prevent too big sentences
		if ((r - m) > s * 2 - 1) {
			Log.o2("BIG");
			r = text.indexOf(" ", m + s * 2 - 1);

		}

		return r;
	}

	private static final int MAX_N_BEST = 20;
	private static final int MAX_N_BEST_CHUNKS = 20;

	// protected Chunker chunker;
	// protected NBestChunker chunker;
	/**
	 * container for chunker
	 */
	protected ConfidenceChunker chunker;
	/**
	 * minimum confidence
	 */
	protected double minConf = 0.25;
	/**
	 * type of classifier "NER" named entity recognition or "POS" part of speech tagging
	 */
	protected String classifiertype = "NER";

	// protected abstract Chunker getChunker();
	// protected abstract NBestChunker getChunker();
	/**
	 * Getter for the chunker.
	 * 
	 * @return chunker
	 */
	protected abstract ConfidenceChunker getChunker();

	/**
	 * Runs one classification.
	 * 
	 * @param docId ID of the document
	 * @param s text string
	 * @return classification result
	 */
	public String classify(int docId, String s) {

		String returnvalue = "";
		HashSet<Integer> currentLemmaIDs = new HashSet<Integer>();
		ArrayList<DBTextpassage> TextPassages = new ArrayList<DBTextpassage>();

		String text = s;
		int wordsnr = s.split(" ").length;

		char[] cs = text.toCharArray();

		int icsstart = 0;
		int icsend = cs.length; 
		for (int m = icsstart; m + 2 < icsend; m = nextWordGroup(text, m) + 1) {
			int csstart = m;
			int csend = nextWordGroup(text, m);

			if (csend == -1)
				csend = icsend;
			Log.o2(m + ": " + csstart + "-" + csend + " of " + cs.length + "is " + text.subSequence(csstart, csend));
			int max_n_best_chunks = MAX_N_BEST_CHUNKS;
			int textsubsequencesize = text.subSequence(csstart, csend).toString().split(" ").length;
			if (classifiertype.equals("POS")) {
				max_n_best_chunks = textsubsequencesize * 4;
			}
			Iterator<Chunk> it = null;
			boolean hasChunks = true;
			try {

				it = chunker.nBestChunks(cs, csstart, csend, max_n_best_chunks);
			} catch (Exception e) {
				Log.o1("warning");
				hasChunks = false;
			}

			int n = 0;

			HashMap<String, DBTextpassage> maxTextpassage = new HashMap<String, DBTextpassage>();
			ArrayList<DBTextpassage> allTextpassage = new ArrayList<DBTextpassage>();

			while (hasChunks) {

				boolean ithasNext = false;
				try {
					ithasNext = it.hasNext();
				} catch (ArrayIndexOutOfBoundsException e) {
					Log.o2("Error, could not get another chunk of the chunk iterator.");
				}

				if (!ithasNext)
					break;

				++n;
				Log.o2("n:" + n);

				Chunk chunk = it.next();
				String type = chunk.type();
				double conf = Math.pow(2.0, chunk.score());
				int start = chunk.start();
				int end = chunk.end();
				String phrase = s.substring(start + csstart, end + csstart);
				DecimalFormat df = new DecimalFormat("0.0000");
				Log.o2(n + " " + df.format(conf) 
						+ "       (" + start + ", " + end + ")       " + type + "         " + phrase);

				chunk = null;

				if (conf < minConf)
					continue; 
				Log.o1(">minconf");

				// token split
				String[] phraseT;
				if (tokeninzewhitespace) {
					phraseT = phrase.split(" ");
				} else {
					phraseT = new String[1];
					phraseT[0] = phrase;
				}
				int mid = 0;
				for (String phraseTs : phraseT) {
					int len = phraseTs.length();

					DBTextpassage textpassage = new DBTextpassage();
					textpassage.setName(phraseTs + " # " + type);
					textpassage.setStart(getPosFromNumber(start + mid + csstart, s));
					textpassage.setEnd(getPosFromNumber(start + mid + len + csstart, s));
					textpassage.setWeight(df.format(conf));
					allTextpassage.add(textpassage);

					if (classifiertype.equals("PTOS")) {

						String posstring = (start + mid + csstart) + "-" + (start + mid + len + csstart);

						if (maxTextpassage.containsKey(posstring)) {
							DBTextpassage maxtp = maxTextpassage.get(posstring);
							double maxconf = Double.parseDouble(maxtp.getWeight());
							if (maxconf < conf) {
								maxTextpassage.remove(posstring);
								maxTextpassage.put(posstring, textpassage);
							}

						} else {

							maxTextpassage.put(posstring, textpassage);

						}

					}

					mid += len;
					// whitespace
					mid++;
				}

			}

			if (classifiertype.equals("PTOS")) {
				allTextpassage.clear();
				allTextpassage.addAll(maxTextpassage.values());
			}

			int allTextpassagesize = allTextpassage.size();
			for (DBTextpassage textpassage : allTextpassage) {
				TextPassages.add(textpassage);
				String tpname = textpassage.getName();
				int poshashname = tpname.lastIndexOf(" # ");
				String phraseTs = tpname.substring(0, poshashname);
				String type = tpname.substring(poshashname + 3, tpname.length());

				HashSet<String> RegisterTopic = RegisterTopics.get(type);
				if (RegisterTopic == null)
					RegisterTopic = new HashSet<String>();
				RegisterTopic.add(phraseTs);

				RegisterTopics.put(type, RegisterTopic);

				if (LemmaIDs.get(type) == null)
					LemmaIDs.put(type, lemmaIdCounter++);

				String lemmaString = phraseTs + " # " + type;
				int currentLemmaID;
				if (LemmaIDs.get(lemmaString) == null) {
					currentLemmaID = lemmaIdCounter++;
					LemmaIDs.put(lemmaString, currentLemmaID);
				}
				currentLemmaID = LemmaIDs.get(lemmaString);
				currentLemmaIDs.add(currentLemmaID);
			}

			maxTextpassage.clear();
			allTextpassage.clear();

			it = null;
			if (csend == icsend)
				break;

		}

		DocumentLemma.put(docId, currentLemmaIDs);
		DocumentTextPassages.put(docId, TextPassages);
		return returnvalue;

	}

}
