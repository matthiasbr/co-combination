/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.webServer;

import vre.analysis.ExtendedDatabaseConnection;
import vre.util.Configuration;
import vre.util.Log;
import vre.util.ImportCrowdflower;

/**
 * Class to run the import, without starting the whole server.
 *
 */
public class offlineTestImport {

	/**
	 * Main method to start import.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Configuration.setConfigXml("config.xml");
		String url = Configuration.getConfigXMLvalue("database");

		Log.o2(url);

		ImportCrowdflower ic = new ImportCrowdflower();

		ExtendedDatabaseConnection dbcon = new ExtendedDatabaseConnection();

		ic.doimport(dbcon);

		String returnList = ic.createSQL(dbcon, "cf");

		Log.o2(String.valueOf(returnList.length()));

		dbcon.executeQuery(returnList);

	}

}
