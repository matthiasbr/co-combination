/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.webServer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import vre.analysis.CoForestOutputArgMaxNER;
import vre.analysis.ExtendedDatabaseConnection;
import vre.analysis.util.DBCatParser;

/**
 * Class holds different steps of the Co-Combination Algorithm.
 *
 */
public class CoCombSteps {

	/**
	 * Routine to prepare the views according to the type of training.
	 * 
	 * @param mycon Database connection
	 * @param cID ID of the category 
	 * @param trainingtype 1: standard, for the data of the VRE 2: training with the short message data set 
	 * @param usertype type of user (for evaluation runs)
	 * @param groupsize number of users per crowdsourcing task
	 * @return views generated for the Co-Training type of algorithms
	 */
	public static HashSet<Integer> prepareViews(ExtendedDatabaseConnection mycon, String cID, int trainingtype,
			String usertype, int groupsize) {

		HashSet<Integer> views = new HashSet<Integer>();
		if (trainingtype == 1) {
			if (usertype.equals("wug") || usertype.equals("wugr")) {
				DBCatParser.groupsize = groupsize;
				DBCatParser.usergs.clear();
				for (int gc = 1; gc <= groupsize; gc++) {
					views.add(gc);
					DBCatParser.usergs.put(gc, new ArrayList<Integer>());
				}

			} else {
				views = mycon.getUsersOfCategory(Integer.valueOf(cID), usertype);
			}
		}
		if (trainingtype == 2) {
			views.add(1);
			views.add(2);
			views.add(3);
			views.add(4);
			views.add(5);

		}

		return views;

	}

	/**
	 * Initial run of the Co-Combination algorithm.
	 * 
	 * @param mID folder ID in the VRE
	 * @param cID ID of the category
	 * @param add additional information
	 * @param mycon database connection
	 * @param learningiteration iteration of the learning curve ()
	 * @param partition partition of the data (for evaluation runs)
	 * @param ts size of the training set (for evaluation runs)
	 * @param level level of the index, where the entity names are saved
	 * @param setname training set, development or test (for evaluation runs)
	 * @param usertype type of user (for evaluation runs)
	 * @param uID user ID 
	 * @param trainingtype 1: standard, for the data of the VRE 2: training with the short message data set 
	 * @param doCoTesting execute the Co-Testing algorithm
	 * @param doCoForest execute the Co-Forest algorithm
	 * @param debugtraindone (for debug only, skips training runs)
	 * @param classifiertype string that identifies the classification task 
	 * @param minConf minimum Confidence used for the confidence threshold
	 * @param userqueries amount of queries asked to the users
	 * @param views IDs of the single views
	 * @return statements for the database
	 */
	public static String start(int mID, String cID, String add, ExtendedDatabaseConnection mycon, int learningiteration,
			int partition, double ts, int level, String setname, String usertype, long uID,
			int trainingtype, boolean doCoTesting, boolean doCoForest, int debugtraindone, String classifiertype,
			double minConf, int userqueries, HashSet<Integer> views) {

		String developmentortest = setname;
		HashMap<Integer, String> myDocs = mycon.getDocumentsbymIDcID(Integer.valueOf(mID), Integer.valueOf(cID),
				partition, ts, setname, "cu", -1);
		// HashMap<Integer, String> docs =
		// mycon.getDocumentsbymIDcID(0,0,partition,ts,false,useDev,useTest,"cu",uID);

		int endcID;
		boolean onlyArgMax = false;
		if (!onlyArgMax) {
			CoCombStepsI CCSI = new CoCombStepsI(mycon, trainingtype, doCoTesting, doCoForest);
			CCSI.go(views, myDocs, cID, partition, ts, level, usertype, setname, debugtraindone, classifiertype, minConf,
					userqueries);

			// resulting labels only in temporary database, train for transfer to main database
			endcID = mycon.getNextID("categorynames") - 1;
		} else {
			// only for development
			System.out.println("SKIPPING");
			endcID = mycon.getNextID("categorynames") - 1;
		}

		CoForestOutputArgMaxNER myCoForestOutputArgMaxNER = new CoForestOutputArgMaxNER(endcID, views, -1, mycon,
				partition, ts, developmentortest, level, "cu", minConf);
		myCoForestOutputArgMaxNER.prepare(myDocs);

		Iterator<Integer> myDocsIterator = myDocs.keySet().iterator();
		while (myDocsIterator.hasNext()) {
			Integer docid = myDocsIterator.next();
			myCoForestOutputArgMaxNER.classify(docid, myDocs.get(docid));
		}

		String returnList = myCoForestOutputArgMaxNER.createSQL(mycon,
				"Amultiend__L" + learningiteration + "_P" + partition);

		return returnList;
	}

}
