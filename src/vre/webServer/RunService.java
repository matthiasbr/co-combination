/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.webServer;

import javax.xml.ws.Endpoint;

/**
 * Runs the service.
 *
 */
public class RunService {

    
 /**
 * Runs the service under the address given below.
 * @param args
 */
public static void main(String[] args) {
  String adress = "http://0.0.0.0:9110/wsServerVRE";	 
  System.out.println("VRE Web Service started. " + adress);
  Endpoint.publish(adress, new WebServiceMain());

 }
}
