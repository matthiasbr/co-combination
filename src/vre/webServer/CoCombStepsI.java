/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.webServer;

import static vre.util.Convert.getDoubleFromString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import vre.analysis.CoForestIterationStatNER;
import vre.analysis.DocumentTagsForWordWithWeights;
import vre.analysis.ExtendedDatabaseConnection;
import vre.analysis.StatNER;
import vre.analysis.WeightEntry;
import vre.analysis.util.ContentionPoint;
import vre.analysis.util.DBErrorEstimator;
import vre.util.Log;

/**
 * Class for the Co-Combination steps in the iteration of the algorithm.
 *
 */
public class CoCombStepsI {
	/**
	 * BIO scheme begin tag
	 */
	protected final String mBeginTagPrefix = "B_";
	/**
	 * BIO scheme inner tag
	 */
	protected final String mInTagPrefix = "I_";
	/**
	 * BIO scheme out tag
	 */
	protected final String mOutTag = "O";
	/**
	 * maximum iterations
	 */
	protected final int mi = (int) Math.pow(2, 4);
	/**
	 * Hash map for category IDs
	 */
	private HashMap<String, Integer> cIDMap;
	/**
	 * Hash map for Co-Forest error estimation values
	 */
	private HashMap<String, Double> e;
	/**
	 * Hash map for Co-Forest predictive confidence values
	 */
	private HashMap<String, Double> W;
	/**
	 * Extended database connection
	 */
	private ExtendedDatabaseConnection econ;
	/**
	 * Type of training 0 = none (debug-only) , 1 = initialization, 2 = iteration
	 */
	private int trainingtype;
	/**
	 * Whether to execute the Co-Testing part of the algorithm
	 */
	private boolean doCoTesting = true;
	/**
	 * Whether to execute the Co-Forest part of the algorithm
	 */
	private boolean doCoForest = true;
	/**
	 * Disables checks in Co-Forest (debug-only, standard is false)
	 */
	private boolean overridetest1 = false;

	/**
	 * constructor
	 * 
	 * @param econ
	 * @param trainingtype
	 * @param doCoTesting
	 * @param doCoForest
	 */
	public CoCombStepsI(ExtendedDatabaseConnection econ, int trainingtype, boolean doCoTesting,
			boolean doCoForest) {
		cIDMap = new HashMap<String, Integer>();
		e = new HashMap<String, Double>();
		W = new HashMap<String, Double>();
		this.econ = econ;
		this.trainingtype = trainingtype;
		this.doCoTesting = doCoTesting;
		this.doCoForest = doCoForest;

		// temporary table for Co-Comb execution
		econ.executeQuery("Delete FROM CoComb");

	}

	/**
	 * Run one Iteration of the Co-Combination
	 * 
	 * @param views IDs of the single views
	 * @param myDocs documents, which are to be indexed
	 * @param cID IDs of the categories
	 * @param partition partition of the data (for evaluation runs)
	 * @param ts size of the training set (for evaluation runs)
	 * @param level level of the index, where the entity names are saved
	 * @param usertype type of user (for evaluation runs)
	 * @param developmentortest is it a development or test run (for evaluation runs)
	 * @param debugtraindone (for debug only, skips training runs)
	 * @param classifiertype string that identifies the classification task 
	 * @param minConf minimum Confidence used for the confidence threshold
	 * @param userqueries amount of queries asked to the users
	 */
	public void go(HashSet<Integer> views, HashMap<Integer, String> myDocs, String cID, int partition, double ts,
			int level, String usertype, String developmentortest, int debugtraindone, String classifiertype,
			double minConf, int userqueries) {
		ExtendedDatabaseConnection mycon = econ;

		// Double confidenceThreshold = 0.25;
		Double confidenceThreshold = minConf;

		HashMap<int[], Double> e = new HashMap<int[], Double>();
		HashMap<int[], Double> W = new HashMap<int[], Double>();

		HashMap<Integer, Integer> highestcIDMapt = new HashMap<Integer, Integer>();

		int t = 0;

		int viewcount = views.size();

		// for debug reasons one can skip the training rounds here
		if (debugtraindone < 0) {
			if (trainingtype == 1) {
				for (int user : views) {
					e.put(new int[] { user, 0 }, 0.5);
					W.put(new int[] { user, 0 }, 0.0);

				}
				for (int user : views) {
					eput(user + "_" + 0, 0.5);
					Wput(user + "_" + 0, 0.0);

				}
				for (int user : views) {
					Log.o1("### T : " + t + " User: " + user + "###");
					StatNER myStatNER = new StatNER(Integer.valueOf(cID), user, mycon, t + "", partition, ts,
							developmentortest, level, usertype, classifiertype, minConf);

					Iterator<Integer> myDocsIterator = myDocs.keySet().iterator();
					while (myDocsIterator.hasNext()) {
						Integer docid = myDocsIterator.next();
						myStatNER.classify(docid, myDocs.get(docid));
					}

					// save ID of temp Cat
					int newID = mycon.getNextID("categorynames");
					cIDMapput(user + "_" + t, newID);

					String createsql = myStatNER.createSQL(mycon, "_" + t + "_" + user);
					mycon.executeQuery(createsql);

				}
			}
			if (trainingtype == 2) {
				for (int chunkerid : views) {
					e.put(new int[] { chunkerid, 0 }, 0.5);
					W.put(new int[] { chunkerid, 0 }, 0.0);

				}
				for (int chunkerid : views) {
					eput(chunkerid + "_" + 0, 0.5);
					Wput(chunkerid + "_" + 0, 0.0);

				}
				for (int chunkerid : views) {
					Log.o1("### T : " + t + " Chunker: " + chunkerid + "###");
					StatNER myStatNER = new StatNER(chunkerid, mycon, t + "", -1, ts, developmentortest, level, "cu",
							classifiertype, minConf);

					Iterator<Integer> myDocsIterator = myDocs.keySet().iterator();
					while (myDocsIterator.hasNext()) {
						Integer docid = myDocsIterator.next();
						myStatNER.classify(docid, myDocs.get(docid));
					}

					// save ID of temp Cat
					int newID = mycon.getNextID("categorynames");
					cIDMapput(chunkerid + "_" + t, newID);
					String createsql = myStatNER.createSQL(mycon, "_" + t + "_" + chunkerid);
					mycon.executeQuery(createsql);

				}

			}

		}
		viewcount = views.size();
		// CoTesting Step 0

		if (debugtraindone < 1) {
			CoTesting(0, cID, views, level, classifiertype, userqueries);
		}

		if (!doCoForest) {
			return;
		}

		HashMap<Integer, CoForestIterationStatNER> LastLearnersforRandomTree = new HashMap<Integer, CoForestIterationStatNER>();

		HashMap<Integer, CoForestIterationStatNER> LearnersforRandomTree;

		boolean changedTree = false;

		do {
			t++;

			if (t < 2) {
				overridetest1 = true;
			} else {
				overridetest1 = false;
			}

			LearnersforRandomTree = new HashMap<Integer, CoForestIterationStatNER>();

			// 1..n
			HashMap<Integer, Integer> cIDMapt = new HashMap<Integer, Integer>();
			for (int view : views) {
				int m = cIDMapget(view + "_" + (t - 1));
				if (m == -1)
					continue;
				cIDMapt.put(view, m);
				highestcIDMapt.put(view, m);
			}
			if (t > debugtraindone) {
				int viewcounter = 0;
				for (int view : views) {
					viewcounter++;
					Log.o1("### T : " + t + " View: " + view + "###" + viewcounter);
					// perhaps later combine with originating index then column in db to
					// represent single tree
					int newcID = cIDMapget(view + "_" + (t - 1));

					DBErrorEstimator eeor = new DBErrorEstimator(view, mycon, views, cIDMapt, partition, level);
					// of new tree see above and imcorporate information from Integer.valueOf(cID)
					// then set user
					double estimateError = eeor.parseCategory(newcID, ts, developmentortest);

					eput(view + "_" + t, estimateError);
					// eeor.dispose
					// L empty done later by running new classifier specified for subsampling

					double eSubit = eget(view + "_" + t);
					double eSubitPrev = eget(view + "_" + (t - 1));
					double WSubitPrev = Wget(view + "_" + (t - 1));

					if (eSubit < eSubitPrev || overridetest1) {
						// subsampling = special statistical parser

						// double parameter = (eSubitPrev * WSubitPrev) / eSubit;
						boolean subsamplingAndConfidence = true && !overridetest1;
						// subsampling and confidence decision combined in one parser
						CoForestIterationStatNER myCoForestIterationStatNER = new CoForestIterationStatNER(view,
								newcID, mycon, highestcIDMapt, partition, ts, developmentortest, level, usertype,
								eSubit, eSubitPrev, WSubitPrev, confidenceThreshold, viewcount, Integer.valueOf(cID),
								classifiertype, minConf, subsamplingAndConfidence);
						LearnersforRandomTree.put(view, myCoForestIterationStatNER);

						double currentW = myCoForestIterationStatNER.getW(); // confidence over all other labels
						Wput(view + "_" + t, currentW);

					} else {
						Wput(view + "_" + t, WSubitPrev);
					}

				}
				// 1..n
				changedTree = false;

				for (int view : views) {
					double eSubit = eget(view + "_" + t);
					double eSubitPrev = eget(view + "_" + (t - 1));
					double WSubitPrev;

					try {
						WSubitPrev = Wget(view + "_" + (t - 1));
					} catch (Exception e2) {
						continue;
					}
					if (!WcontainsKey(view + "_" + t))
						continue; // see above non existent if: if ( eSubit < eSubitPrev) {not met
					double WSubit = Wget(view + "_" + t);

					CoForestIterationStatNER myCoForestIterationStatNER = LearnersforRandomTree.get(view);

					if (overridetest1
							|| eSubit * WSubit >= eSubitPrev * WSubitPrev && myCoForestIterationStatNER != null) {

						Iterator<Integer> myDocsIterator = myDocs.keySet().iterator();
						while (myDocsIterator.hasNext()) {
							Integer docid = myDocsIterator.next();
							if (myDocs.containsKey(docid)) {
								String doctext = myDocs.get(docid);
								myCoForestIterationStatNER.classify(docid, doctext);
							}
						}

						// save ID of temporary category
						int newID = mycon.getNextID("categorynames");
						cIDMapput(view + "_" + t, newID);
						highestcIDMapt.put(view, newID);
						String createsql = myCoForestIterationStatNER.createSQL(mycon, "_" + t + "_" + view);
						mycon.executeQuery(createsql);

						changedTree = true;

					} else {
						cIDMapput(view + "_" + t, highestcIDMapt.get(view));
					}

				}
			}
			CoTesting(t, cID, views, level, classifiertype, userqueries);
			 
		// Repeat until none of the trees in Random Forest changes 
		} while ((changedTree || (overridetest1 && t < 5)) && t < mi);

	}

	/**
	 * Saves cID of current iteration.
	 * 
	 * @param s name for category
	 * @param i cID
	 */
	public void cIDMapput(String s, int i) {
		cIDMap.put(s, i);
		econ.insertintoCF(s, "cID", String.valueOf(i));

	}

	/**
	 * Saves error estimation of current iteration.
	 * 
	 * @param s name for category
	 * @param i cID
	 */
	public void eput(String s, double i) {
		e.put(s, i);
		econ.insertintoCF(s, "e", String.valueOf(i));

	}

	/**
	 * Saves predictive confidence of current iteration.
	 * 
	 * @param s name for category
	 * @param i cID
	 */
	public void Wput(String s, double i) {
		W.put(s, i);
		econ.insertintoCF(s, "W", String.valueOf(i));

	}

	/**
	 * get cID
	 * 
	 * @param s name for category
	 * @return cID
	 */
	public int cIDMapget(String s) {
		String r = econ.selectfromCF("cID", s);
		if (r.equals(""))
			return -1;
		int i = Integer.parseInt(r);
		cIDMap.put(s, i);
		return i;

	}

	/**
	 * get error estimation
	 * 
	 * @param s name for category
	 * @return cID
	 */
	public double eget(String s) {
		double i = getDoubleFromString(econ.selectfromCF("e", s));
		e.put(s, i);
		return i;

	}

	/**
	 * check if predictive confidence exists
	 * 
	 * @param s name for category
	 * @return cID
	 */
	public boolean WcontainsKey(String s) {
		String val = econ.selectfromCF("W", s);
		if (val.equals("")) {
			return false;
		}
		return true;
	}

	/**
	 * get predictive confidence
	 * 
	 * @param s name for category
	 * @return cID
	 */
	public double Wget(String s) {
		double i = getDoubleFromString(econ.selectfromCF("W", s));
		W.put(s, i);
		return i;

	}

	/**
	 * Co-Testing (Muslea, Ion, Minton, Steven und Knoblock, Craig A. (2000): Selective
	 * Sampling with Redundant Views. In: Proceedings of the Seventeenth
	 * National Conference on Artificial Intelligence and Twelfth Conference
	 * on on Innovative Applications of Artificial Intelligence, July 30
	 * - August 3, 2000, Austin, Texas, USA. Hrsg. von Kautz, Henry A.
	 * und Porter, Bruce W. AAAI Press / The MIT Press, S. 621–626.
	 * isbn: 0-262-51112-6. url: http://www.aaai.org/Library/AAAI/
	 * 2000/aaai00-095.php) part of the Co-Combination
	 * 
	 * @param t iteration
	 * @param cid ID of the category
	 * @param views IDs of the single views
	 * @param level level of the index, where the entity names are saved
	 * @param classifiertype string that identifies the classification task 
	 * @param userqueries amount of queries asked to the users
	 */
	private void CoTesting(int t, String cid, HashSet<Integer> views, int level, String classifiertype,
			int userqueries) {
		if (doCoTesting) {
			ArrayList<ContentionPoint> contentionPoints = ContentionPoints(t, views, level);
			if (contentionPoints != null && contentionPoints.size() > 0) {
				Log.o2("Found ContentionPoints size " + contentionPoints.size());
				SelectQuery(contentionPoints, views.size(), cid, level, classifiertype, userqueries);
			}
		}
	}

	/**
	 * Sub algorithm of Co-Testing, which gets the contention points
	 * 
	 * @param t iteration
	 * @param views IDs of the single views
	 * @param level level of the index, where the entity names are saved
	 * @return contention points
	 */
	private ArrayList<ContentionPoint> ContentionPoints(int t, HashSet<Integer> views, int level) {
		// Important this is naive selection change db query and or
		Log.o2("Contention Points of " + t);
		ArrayList<Integer> cIDList = new ArrayList<Integer>();
		for (int view : views) {
			int m = cIDMapget(view + "_" + t);
			if (m == -1)
				continue;
			cIDList.add(m);
		}
		if (cIDList.isEmpty()) {
			return null;
		} else {
			ArrayList<ContentionPoint> textpassages = econ.getTextpassagesofIteration(t, cIDList, level);
			return textpassages;
		}
	}

	/**
	 * 
	 * Sub algorithm of Co-Testing, which selects user queries out of the contention points
	 * 
	 * @param textpassages the potential contention points to query
	 * @param viewcount number of views
	 * @param cid ID of the category
	 * @param level evel of the index, where the entity names are saved
	 * @param classifiertype string that identifies the classification task 
	 * @param userqueries amount of queries asked to the users
	 */
	private void SelectQuery(ArrayList<ContentionPoint> textpassages, int viewcount, String cid, int level,
			String classifiertype, int userqueries) {

		HashSet<Integer> docs = new HashSet<Integer>();

		for (ContentionPoint cp : textpassages) {
			docs.add(cp.getDocID());
		}

		ArrayList<Integer> dIDList = new ArrayList<Integer>();
		for (Integer k : docs) {
			dIDList.add(k);
		}
		String golddb = "";
		if (!econ.golddb.isEmpty()) {
			golddb = econ.golddb;
		}
		HashMap<Integer, DocumentTagsForWordWithWeights> dList = econ
				.getDocumentTagsForWordWithWeights(Integer.parseInt(cid), dIDList, -1, level, "cu", golddb);

		int numcp = textpassages.size();
		int numc = 0;

		for (ContentionPoint cp : textpassages) {
			numc++;
			if (numc > userqueries)
				continue;
			ArrayList<Integer> tids = cp.getTids();
			Integer value = tids.size();
			String oldlabel = cp.getLabel();
			int docID = cp.getDocID();

			Log.o2("CP" + value + " " + oldlabel + " " + docID);
			if (value < viewcount) {

				DocumentTagsForWordWithWeights d = dList.get(docID);
				HashSet<Integer> dusers = dList.get(docID).getDUsers();
				if (dusers == null)
					continue;
				
				int maxuser = dusers.size();
				ArrayList<WeightEntry> weList = d.getWeightEntryKeys(cp.getPlb(), cp.getPcb(), cp.getPle(),
						cp.getPce());
				String tag = mOutTag;
				Double score = 1.0;
				int scorecount = 0;
				HashMap<String, Double> scoreweights = new HashMap<String, Double>();

				for (WeightEntry we : weList) {
					String ctag = we.getName();
					String cweight = we.getWeight();
					int cuser = we.getUserw();
					double cscore = getDoubleFromString(cweight);
					Log.o2("ASK cTAG: " + ctag + ":" + cscore);

					if (scoreweights.containsKey(ctag)) {
						double ascore = scoreweights.get(ctag);
						cscore += ascore;
					}
					scoreweights.put(ctag, cscore);
					if (cscore > score || tag.equals(mOutTag)) {
						tag = ctag;
						score = cscore;
					}
					scorecount++;

				}

				// no weight information found
				if (scorecount == 0) {
					score = 1.0;
				} else {
					score = score / (double) maxuser;
				}

				String label = tag;
				if (!oldlabel.equals(label)) {
					Log.o2("Replace " + oldlabel + "with" + label);
					if (label != mOutTag) {
						econ.relabelTIDs(tids, label, docID, level);
					} else {
						econ.removeTIDs(tids, level);
					}
				}

			}

		}

	}

}
