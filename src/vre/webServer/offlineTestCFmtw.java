/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.webServer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import vre.analysis.ExtendedDatabaseConnection;
import vre.analysis.util.DBCatParser;
import vre.util.Configuration;
import vre.util.Log;

/**
 * Class for an offline test run of the Co-Combination.
 *
 */
public class offlineTestCFmtw {

	/**
	 * Method to start the offline run.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Configuration.setConfigXml("config.xml");

		int logdetail = Integer.parseInt(Configuration.getConfigXMLvalue("logdetail")); // 4
		Log.setDetail(logdetail);
		int debugtraindone = -1;
		int startpartition = 0;
		int endpartition = 9;

		String cID = "9";
		int mID = 0;
		int algorithmtype = 1;
		int trainingtype = 1;
		boolean doCoTesting = true;
		boolean doCoForest = true;
		int level = -1;
		int partition = 0;

		int minli = 0;
		int maxli = 1;

		minli = 1;
		maxli = 39;

		minli = 0;
		maxli = 40;

		String usertype = "wug";

		String classifiertype = "NER";
		int userqueries = 65;
		int groupsize = 10;
		double minConf = 0.25;
		if (classifiertype == "POS") {
			userqueries = 30;
			groupsize = 5;
			minConf = 0.1;
		}

		double ts = 0.1;
		// String developmentortest = "development";
		String developmentortest = "test";

		// String developmentortest = "compare";
		if (developmentortest.equals("test")) {
			partition = -1;
			startpartition = -1;
			endpartition = -1;
			// -1 important => no training set partition
		}

		if (developmentortest.equals("compare")) {
			mID = 3;
			partition = -1;
			startpartition = -1;
			endpartition = -1;
			// -1 important => no training set partition
		}

		ExtendedDatabaseConnection mycon = new ExtendedDatabaseConnection();

		if (debugtraindone == -1) {
			mycon.executeQuery("DELETE FROM CoComb WHERE 1;");
		}
		// generaize replace users views
		HashSet<Integer> views = CoCombSteps.prepareViews(mycon, cID, trainingtype, usertype, groupsize);

		boolean useDev = developmentortest.equals("development");
		boolean useTest = developmentortest.equals("test");

		for (int learningiteration = minli; learningiteration <= maxli; learningiteration++) {
			System.out.println("LI: " + learningiteration);
			userqueries *= 100 + (learningiteration - 20) * 100;
			for (partition = startpartition; partition <= endpartition; partition++) {
				System.out.println("PARTITION " + partition);
				ExtendedDatabaseConnection.setP(partition);

				String returnList = CoCombSteps.start(mID, cID, "", mycon, learningiteration, partition, ts, level,
						developmentortest, usertype, -1, trainingtype, doCoTesting, doCoForest, debugtraindone,
						classifiertype, minConf, userqueries, views);

				mycon.executeQuery(returnList);

			}

		}

		for (Integer k : DBCatParser.usergs.keySet()) {
			System.out.print(k + ":");
			ArrayList<Integer> v = DBCatParser.usergs.get(k);
			Iterator<Integer> it = v.iterator();
			while (it.hasNext()) {
				Integer in = it.next();
				System.out.print(in);
				System.out.print(",");

			}
			System.out.println();
		}

	}

}
