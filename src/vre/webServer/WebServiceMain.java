/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package vre.webServer;

import static vre.util.Convert.aL;
import static vre.util.Convert.l;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.jws.WebService;

import vre.analysis.DatabaseConnection;
import vre.analysis.DatabaseConnectionAdv;
import vre.analysis.DicNER;
import vre.analysis.ExtendedDatabaseConnection;
import vre.analysis.StatNER;
import vre.util.Configuration;
import vre.util.Log;

/**
 * Main class to start the webservice.
 *
 */
@WebService

public class WebServiceMain {

	/**
	 * Test routine for the webservice.
	 * @param argument
	 * @return argument
	 */
	public String test(String argument) {
		return "This is the Webservice i got" + argument;
	}

	/**
	 * Execute a dictionary-based NER
	 * 
	 * @param mID document folder ID
	 * @param cID category ID
	 * @param add additional information
	 * @return new category
	 */
	public String execDicNER(String mID, String cID, String add) {
		DatabaseConnection mycon = new DatabaseConnection();
		HashMap<String,String> myfind = mycon.getCategoriesbyCID(Integer.valueOf(cID));
		DicNER myDicNER = new DicNER(myfind);

		HashMap<Integer, String> myDocs = mycon.getDocumentsbymIDcID(Integer.valueOf(mID), null, 0, 0, "production",
				"cu", -1);

		Iterator<Integer> myDocsIterator = myDocs.keySet().iterator();
		while (myDocsIterator.hasNext()) {
			Integer docid = myDocsIterator.next();
			myDicNER.classify(docid, myDocs.get(docid));
		}

		String returnList = myDicNER.createSQL(null, null);

		return returnList;
	}

	/**
	 * Execute a generic statistical NER
	 * 
	 * @param mID document folder ID
	 * @param cID category ID
	 * @param add additional information
	 * @return new category
	 */
	public String execStatNER(String mID, String cID, String add) {

		DatabaseConnectionAdv mycon = new DatabaseConnectionAdv();

		StatNER myStatNER = new StatNER(Integer.valueOf(cID), 0, mycon, "", 0, 0, "production", 0, "cu", "NER", 0.1);

		HashMap<Integer, String> myDocs = mycon.getDocumentsbymIDcID(Integer.valueOf(mID), null, 0, 0, "production",
				"cu", -1);

		Iterator<Integer> myDocsIterator = myDocs.keySet().iterator();
		while (myDocsIterator.hasNext()) {
			Integer docid = myDocsIterator.next();
			Log.o2(docid.toString());
			myStatNER.classify(docid, mycon.getDocumentTextbydocID(docid));
		}

		String returnList = myStatNER.createSQL(null, null);

		return returnList;
	}

	/**
	 * Execute the Co-Combination NER
	 * 
	 * @param mID document folder ID
	 * @param cID category ID
	 * @param add additional information
	 * @return new category
	 */
	public String execCoCombStatNER(String mID, String cID, String add) {

		ExtendedDatabaseConnection mycon = new ExtendedDatabaseConnection();

		HashSet<Integer> views = CoCombSteps.prepareViews(mycon, cID, 1, "cu", 10);

		String returnList = CoCombSteps.start(Integer.valueOf(mID), cID, add, mycon, 0, 0, 0, 0, "production", "cu",
				0, 1, true, true, 1, "NER", 0.1, 1000, views);

		return returnList;

	}

	/**
	 * Communicator routine with the Virtual Research Environment (VRE). 
	 * The VRE can trigger different routines available in this webservice.
	 * 
	 * @param VREcommand Command the VRE wants to execute
	 * @param VREargument Argument from the VRE
	 * @param VREcallback Callback routine of the VRE client
	 * @param VREcid Socket ID of the VRE client session
	 * @param VREexeccallback Callback routine in the VRE server process
	 * @return answer of the service
	 */
	public String communicatorVRE(String VREcommand, String VREargument, String VREcallback, String VREcid,
			String VREexeccallback) {
		Log.o2("GOT");
		Log.o2(VREcommand);
		Log.o2(VREargument);
		Log.o2(VREcallback);
		Log.o2(VREcid);
		Log.o2(VREexeccallback);
		Log.o2("-------");

		Configuration.setConfigXml("config.xml");
		String result = "";

		// known commands
		if (VREcommand.compareTo("test") == 0)
			result = test(VREargument);
		if (VREcommand.compareTo("execDicNER") == 0) {
			ArrayList<String> aL = aL(VREargument);
			result = execDicNER(aL.get(0), aL.get(1), aL.get(2));
		}
		if (VREcommand.compareTo("execStatNER") == 0) {
			ArrayList<String> aL = aL(VREargument);
			result = execStatNER(aL.get(0), aL.get(1), aL.get(2));
		}
		if (VREcommand.compareTo("execCoCombStatNER") == 0) {
			ArrayList<String> aL = aL(VREargument);
			result = execCoCombStatNER(aL.get(0), aL.get(1), aL.get(2));
		}

		Log.o2("Result of String:" + result);

		ArrayList<String> answer = new ArrayList<String>();

		answer.add(VREexeccallback);
		answer.add(result);
		answer.add(VREcallback);
		answer.add(VREcid);

		String answerVRE = l(answer);

		Log.o2("Answering:" + answerVRE);

		return answerVRE;
	}
}