/*
 * Copyright 2020 Matthias Bremm
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

$$('.myarea').addEvent('change:relay(input)', function(e, target) {
  var checked = target.checked;
  var value = target.value;
  var valuearray = value.split('_');
  var tokenc = valuearray[0];
  var tag = valuearray[1];
  var seltag = 's' + tag;
  var ce =  target.getParent().getParent().getPrevious('a');
  if (checked) {
     ce.addClass(seltag);
  } else {
     ce.removeClass(seltag);
  }
});

Element.Events.keyupspace = {
    base: 'keyup', 
    condition: function(event){
        return (event.key == 'space');
    }
};

var activatefunction = function(e, target) {
  var cursorx = e.client.x;
  var cursory = e.client.y + 10;
  
  var perRow = e.target;
  perRow.setStyle('clear','left');
  
  var perTypes = ['Person', 'Ort', 'Organisation'];
  for (var k=0; k<perTypes.length; k++) {
    perRow = perRow.getNext();   
    var perLabel = perRow.getChildren()[0];
    var perInput = perLabel.getChildren()[0];
    
    perRow.style.display='block';
    
      if (perLabel.get('text').trim() == "") {     
        perLabel.appendText(perTypes[k]);
      }
  }
  
  if (perRow.getNext().get('tag') != 'i') {
    var ic = new Element("i");
    ic.set("class", "icon-remove-sign");
    ic.setStyle('float','left');
    ic.tabIndex = 0;
    ic.inject(perRow, "after");
    var closeFunction = function(e, target) {
      try {
        e.target.getNext().setStyle('clear',null);
      } catch(ex){ }
      perRow = e.target.getPrevious();
      while (perRow.get('tag') == 'div') {
        perRow.style.display='none';
        perRow = perRow.getPrevious();
      }
      try {
        perRow.setStyle('clear',null);
      } catch(ex){ }   
      var next = e.target.getNext('a');  
      e.target.destroy(); 
      if (e.key == 'space') {
        next.focus();
      }
    };
    ic.addEvent('click', closeFunction);
    ic.addEvent('keyupspace', closeFunction);
    
  }
  
  perRow = perRow.getNext('a');  
    
  perRow.setStyle('clear','left');

};

$$('.myarea a').addEvent('focus', activatefunction);

var as = $$('.myarea a');
for (var i=0; i<as.length; i++) {
  var a = as[i];
  a.tabIndex = 0;
  var innerText = a.get('text');
  if (innerText.trim() == "No data available") {
    a.setStyle('display','none');
  }
}

var names = ['answer'];

for (var k=0; k<names.length; k++) {
  var elements = $$('.'+names[k]+'_gold');
  for (var i=0; i<elements.length; i++) {
    var el = elements[i];
    var ischecked = el.checked;
    var selclass = el.value.split('_')[1];
    if (el.getParent().hasClass('gold_good')) {
      ischecked = true;
    }
    var ce =  el.getParent().getParent().getPrevious('a');
    if (ischecked) {
        ce.addClass('s'+selclass);
        ce.focus();
    } else {
        ce.removeClass('s'+selclass);
    }
    
  }
}

